﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(BoxCollider))]
public class Room : MonoBehaviour
{
    enum Directions
    {
        north = 1,
        south = 2,
        east = 4,
        west = 8,
        fullRoom = 15
    }

    public bool north;
    public bool south;
    public bool east;
    public bool west;
    public bool EntryOrExit;
    public bool ShouldSpawnEnemies = true;
    public bool BossRoom = false; 
    public bool isCorridor;

    public bool hasSpawnedWep = false;
    public int TimeToLightRoom = 3;
    bool ShouldFlicker = false;

    EnemySpawner m_spawner;
    public int m_RoomCompleteionHealthReward;
    [SerializeField]
    bool m_roomCleared;
	bool firstClear;
    public int bitBoolHolder;
	GameObject Player;
    GameObject[] doors;
	private Light[] lightsInRoom;
	private bool lightsOn;
    public float LengthOfFlickerSet = 3;
    public float TimeBetweenFlickerSetMin = 10;
    public float TimeBetweenFlickerSetMax =15;

    private bool inFlag = false;
    // Use this for initialization
    void Awake()
    {
        bitBoolHolder = 0;
        north = (bitBoolHolder & (int)Directions.north) > 0 ? true : false;
        east = (bitBoolHolder & (int)Directions.east) > 0 ? true : false;
        west = (bitBoolHolder & (int)Directions.west) > 0 ? true : false;
        south = (bitBoolHolder & (int)Directions.south) > 0 ? true : false;
        doors = new GameObject[4];
        Transform[] objectList;
        objectList = GetComponentsInChildren<Transform>();
        int doorindex = 0;
        foreach (Transform obj in objectList)
        {
            if (obj.tag == "Door")
            {
                doors[doorindex] =  obj.gameObject;
                doorindex++;
            }
        }
        m_roomCleared = false;
		lightsOn = false;

	}

    void Start()
    {
        Player = GameObject.FindGameObjectWithTag("Player");
		firstClear = true;
        gameObject.GetComponent<BoxCollider>().isTrigger = true;
		lightsInRoom = GetComponentsInChildren<Light>();
		LightsOff();
        for (int i = 0; i < doors.Length; i++)
        {
            if(doors[i])
                doors[i].GetComponent<Door>().Unlock();
        }
    }

    // Update is called once per frame
    void Update()
    {         
        if (m_roomCleared && firstClear && ShouldSpawnEnemies)
        {
            //Player.GetComponent<Health>().DoHealing(m_RoomCompleteionHealthReward * Player.GetComponent<PlayerStats>().m_healingBuffMultiplier);
			//Player.GetComponent<PlayerController>().PlayHealthParticles();
			//firstClear = false;
			foreach (GameObject d in doors)
			{
                if (d != null)
                    d.GetComponent<Door>().Unlock();
            }
        }        
    }
    void OnTriggerStay(Collider other)
    {
        if(other.tag == "Player")
        {
            if(Input.GetKeyDown(KeyCode.Backspace))
            {
                //Debug.Log("safety p pressed");
                m_roomCleared = true;
            }
        }
    }
    //Called by EnemySpawner
    public void Clear()
    {
        m_roomCleared = true;
    }
    //Called by SpawnManager
    public void SetSpawner(EnemySpawner a_spawner)
    {
        m_spawner = a_spawner;
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            if (EntryOrExit)
                ShouldFlicker = false;
            else
                ShouldFlicker = true;

            if (!lightsOn)
            { StartCoroutine(LightsOn()); }
            if (lightsOn && ShouldFlicker)
            {
                StartCoroutine(FlickerLights());
            }
            if (!EntryOrExit)
            {
                //                Debug.Log("Collided");
                if (!m_roomCleared)
                {
                    if (other.tag == "Player" && !inFlag)
                    {
                        inFlag = true;
                        GameObject.FindGameObjectWithTag("SoundManager").GetComponent<SoundManager>().PlaySound(gameObject, 4);
                        if (m_spawner)
                        { m_spawner.ActivateEnemies(); }
                        else
                        {
                            Debug.Log("NO SPAWNER EXISTS");
                            return;
                        }
                        foreach (GameObject d in doors)
                        {
                            if (d != null)
                            {
                                d.GetComponent<Door>().Lock();
                            }
                        }

                    }
                }
            }
        }
    }

    void OnTriggerExit(Collider other)
    {
        if(other.tag=="Player")
        {
            ShouldFlicker = false;
        }
    }
    public int GetBitBool()
    {
        return bitBoolHolder;
    }
    public void SetBitBool(int bitBool)
    {
        bitBoolHolder = bitBool;
    }

    public void LightsOff()
    {
        for (int i = 0; i < lightsInRoom.Length; i++)
        {
            lightsInRoom[i].intensity = 0;
        }
    }

	IEnumerator LightsOn()
	{

        float timer = 0f;
        while (timer < TimeToLightRoom)
        {
            for (int i = 0; i < lightsInRoom.Length; i++)
            {
                lightsInRoom[i].intensity = timer / TimeToLightRoom;
                timer += Time.deltaTime;
            }
            yield return new WaitForEndOfFrame();
        }
        timer = 0f;
        lightsOn = true;
        if(ShouldFlicker)
            StartCoroutine(FlickerLights());
    }

    IEnumerator ExplodeLights()
    {
        float timer = 0f;
        while (timer < 2)
        {
            for (int i = 0; i < lightsInRoom.Length; i++)
            {
                lightsInRoom[i].intensity = (timer / TimeToLightRoom) * 8;
                timer += Time.deltaTime;
            }
            yield return new WaitForEndOfFrame();
        }
            yield return new WaitForSeconds(0.5f);
            for (int i = 0; i < lightsInRoom.Length; i++)
            {
                lightsInRoom[i].intensity = 0;
            }
            yield return new WaitForEndOfFrame();
    }

    IEnumerator FlickerLights()
    {
        bool addLight = true;
        float timer = 0f;
        while (timer < LengthOfFlickerSet)
        {
            float RandomVar = Random.Range( 0, 10);
            RandomVar = RandomVar / 100f;
            for (int i = 0; i < lightsInRoom.Length; i++)
            {
                if (addLight)
                {
                    lightsInRoom[i].intensity = 1;//RandomVar + lightsInRoom[i].intensity > 8 ? 8 : RandomVar + lightsInRoom[i].intensity;
//                    Debug.Log("ADDED");
                }
                else
                {
                    lightsInRoom[i].intensity = 0;//lightsInRoom[i].intensity - RandomVar < 0 ? 0 : lightsInRoom[i].intensity - RandomVar;
//                    Debug.Log("Taken");
                }

                if (lightsInRoom[i].intensity <0)
                {
                    lightsInRoom[i].intensity = 0;
                }
                if(lightsInRoom[i].intensity >8)
                {
                    lightsInRoom[i].intensity = 8;
                }

            }

            addLight = !addLight;
            //yield return new WaitForEndOfFrame();
            float waitTime = Random.Range(0, 30);
            waitTime /= 100f;
            timer += Time.deltaTime + waitTime;
            yield return new WaitForSeconds(waitTime);
        }
        for (int i = 0; i < lightsInRoom.Length; i++)
        {
            lightsInRoom[i].intensity = 1;
        }
        yield return new WaitForSeconds(Random.Range(TimeBetweenFlickerSetMin, TimeBetweenFlickerSetMax));
        if (ShouldFlicker)
        { StartCoroutine(FlickerLights()); }

    }
    public bool DeadEnd()
    {
        GameObject[] doorsOnRoom = new GameObject[4];
        Transform[] allObjectsInRoom;
        allObjectsInRoom = GetComponentsInChildren<Transform>();
        int doorNum = 0;
        foreach (Transform obj in allObjectsInRoom)
        {
            if (obj.tag == "Door")
            {
                doorsOnRoom[doorNum] = obj.gameObject;
                doorsOnRoom[doorNum].GetComponent<Door>().Unlock();
                doorNum++;
            }
        }
        int counter = 0;
        for(int i = 0; i< doorsOnRoom.Length; i++)
            if(doorsOnRoom[i] != null)
            {
                if(doorsOnRoom[i].tag == "Door")
                {
                    counter++;
                }
            }
        if (counter == 1)
        {
            return true;
        }
        else
            return false;
    }
    public void SealSpecificDoor(int DoorToLock)
    {
        switch (DoorToLock)
        {
            case (int)Directions.east:
                {
                    for (int i = 0; i < doors.Length; i++)
                    {
                        if (doors[i])
                        {
                            
                            if (doors[i].transform.position.x - (transform.position.x) > 1)
                            {
                                doors[i].GetComponent<Door>().SealDoor();
                            }
                        }
                    }
                        break;
                }
            case (int)Directions.west:
                {
                    for (int i = 0; i < doors.Length; i++)
                    {
                        if (doors[i])
                        {
                            
                            if (doors[i].transform.position.x - transform.position.x < -1)
                            {                       
                                doors[i].GetComponent<Door>().SealDoor();
                            }
                        }
                    }
                    break;
                }
            case (int)Directions.north:
                {
                    for (int i = 0; i < doors.Length; i++)
                    {
                        if (doors[i])
                        {
                           
                            if (doors[i].transform.position.z - transform.position.z > 1)
                            {
                                doors[i].GetComponent<Door>().SealDoor();
                            }
                        }

                    }
                    break;
                }
            case (int)Directions.south:
                {
                    for (int i = 0; i < doors.Length; i++)
                    {
                        if (doors[i])
                        {
                            if (doors[i].transform.position.z - transform.position.z < -1)
                            {
                                doors[i].GetComponent<Door>().SealDoor();
                            }
                        }
                    }
                    break;
                }
        }
    }
        
}
