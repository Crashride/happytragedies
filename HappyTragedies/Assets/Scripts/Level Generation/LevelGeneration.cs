﻿using UnityEngine;
using System.Collections;

public class LevelGeneration : MonoBehaviour {

	//Prefabs for board pieces
    public GameObject LShape;
    public GameObject StraightCorridor;
    public GameObject FourWayIntersection;
    public GameObject TIntersection;
    public GameObject[] EntryRoomsRecreation;
    public GameObject[] ExitRoomsRecreation;
    public GameObject[] regularRoomsRecreation;
    public GameObject[] healthRoomRecreation;
    public GameObject[] StatUpgradeRoomRecreation;
    public GameObject[] EntryRoomsResearch;
    public GameObject[] ExitRoomsResearch;
    public GameObject[] regularRoomsResearch;
    public GameObject[] healthRoomResearch;
    public GameObject[] StatUpgradeRoomResearch;
    public GameObject m_ExitTrigger;
    public GameObject BossRoom;

    //Arrays to hold a list of pieces on the board and Transforms
    GameObject[] scenePieces;
    ArrayList scenePiecesStillValid;
	Vector3[] PositionsOfPiecesOnBoard;

	//adjustable vars to set minimum and maximum rooms per floor as well as track how many have been placed
    private int minRooms;
    private int maxRooms;
    public int minRoomsResearch;
    public int maxRoomsResearch;
    public int minRoomsRec;
    public int maxRoomsRec;
    [Range(0, 100)]
    public int chanceToSpawnStatRoom;
    public int RecreationSpawnFloor;

    //Private Arrays that holds the currently active array of objects
    private GameObject[] EntryRooms;
    private GameObject[] ExitRooms;
    private GameObject[] regularRooms;
    private GameObject[] healthRoom;
    private GameObject[] StatUpgradeRoom;

    private bool spawnStatRoom;
    private int roomCountTotal;
    private int roomCountCurrent;
	int objectCount;
    int chanceOfRoom;
    bool mustPlaceWTS;

	//stores spawner object
	//TODO: Implement function to place spawner in each room
    public GameObject SpawnerManager;
    GameObject Spawner;

    GameObject RoomHolder;
    GameObject CorridorHolder;

    GameObject player;
    //Enum storing the Bit Values for directions
    enum Directions
    {
        north = 1,
        south = 2,
        east = 4,
        west = 8,
        fullRoom = 15
    }
    int fSafe;

    //Awake Generates a new Vector3 array to store board pieces positons
    //It creates a new list to store the pieces in the current scene
    void Awake()
    {
        minRooms = minRoomsResearch;
        maxRooms = maxRoomsResearch;
        PositionsOfPiecesOnBoard = new Vector3[100];
        scenePiecesStillValid = new ArrayList();
        fSafe = 0;
        EntryRooms = EntryRoomsResearch;
        ExitRooms = ExitRoomsResearch;
        regularRooms = regularRoomsResearch;
        healthRoom = healthRoomResearch;
        StatUpgradeRoom = StatUpgradeRoomResearch;
    }

    //the start function initialies the object count to 0
    //it sets all values for chance to default
    //Gets reference to Spawner Manager
    //It gets a reference to the player
    void Start ()
	{
        chanceOfRoom = 50;
		objectCount = 0;
        roomCountCurrent = 0;
        Spawner = GameObject.FindGameObjectWithTag("SpawnerManager");
        player = GameObject.FindGameObjectWithTag("Player");
        roomCountTotal = Random.Range(minRooms, maxRooms);
        mustPlaceWTS = false;
    }

	//Places the start Room First
	//Then while the room count is less than max rooms for this floor keep placing board pieces
	//Pick a piece on the board than
    public void GenerateLevel(bool a_needWTS)
    {
        if(GameManager.instance.m_currentLevel == RecreationSpawnFloor -25)
        {
            regularRooms = regularRoomsRecreation;
            minRooms = minRoomsRec;
            maxRooms = maxRoomsRec;
        }
        spawnStatRoom = false;
        {
            if(Random.Range(0,100) < chanceToSpawnStatRoom)
            {
                spawnStatRoom = true;
            }
        }
        mustPlaceWTS = a_needWTS;
        scenePieces = new GameObject[100];
        int Safe = 0;
        RoomHolder = new GameObject();
        CorridorHolder = new GameObject();
        RoomHolder.name = ("Rooms");
        CorridorHolder.name = ("Corridors");
        if (!Spawner)
        {
            GameObject instance = Instantiate(SpawnerManager, Vector3.zero, Quaternion.identity) as GameObject;
            instance.name = ("Spawner Manager");
            instance.tag = ("SpawnerManager");
        }
        chanceOfRoom = 50;
        objectCount = 0;
        roomCountCurrent = 0;
        scenePiecesStillValid.Clear();
        for (int i = 0; i < scenePieces.Length; i++)
        {
            scenePieces[i] = null;
        }
        for (int i = 0; i < scenePieces.Length; i++)
        {
            PositionsOfPiecesOnBoard[i] = Vector3.zero;
        }
        roomCountTotal = Random.Range(minRooms, maxRooms);
        

        PlaceStartRoom();
        while (roomCountCurrent <= roomCountTotal)
        {
            GameObject chosenSpotPiece = FindPlacedPiece();
            InstantiatePiece(chosenSpotPiece);
            Safe++;
            if (Safe > 10000)
            {
                Debug.Log("looptrap Rooms");
                break;
            }
        }
        for(int i =0; i < objectCount; i++)
        {
            DoDoorStuff(scenePieces[i].GetComponent<Room>(), scenePieces[i]);
        }
        SpawnerManager spManager = GameObject.FindGameObjectWithTag("SpawnerManager").GetComponent<SpawnerManager>();
        spManager.CreateSpawners(GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>().m_currentLevel + 25, scenePieces); //currently hardcoding the level number
        LShape.transform.rotation = Quaternion.identity;
        TIntersection.transform.rotation = Quaternion.identity;
        StraightCorridor.transform.rotation = Quaternion.identity;
        FinalParse();
    }

    //Places a start room prefab at 0,0
    //Sets the player pos to centre of the room
    void PlaceStartRoom()
    {
        GameObject StartRoom = EntryRooms[Random.Range(0, EntryRooms.Length)];
        GameObject instance = Instantiate(StartRoom, new Vector3(0,0,0), Quaternion.identity) as GameObject;
        scenePieces[objectCount] = instance;
        scenePiecesStillValid.Add(instance);
		PositionsOfPiecesOnBoard[objectCount] = instance.transform.position;
		//roomCountCurrent++; Deliberately not adding to room count
		objectCount++;
        Vector3 Pos = instance.transform.position;
        Pos.y += 1;
        if(!player)
        {
            player = GameObject.FindGameObjectWithTag("Player");
        }
        player.transform.position = Pos;
        player.GetComponent<FallSafety>().startPos = Pos;
    }

    //returns a piece already placed in the scene
    GameObject FindPlacedPiece()
    {
        GameObject chooseAPiece;
		int protection = 0;
		bool validPiece = false;
		do
		{
			int RandomNumberForScenePiece = Random.Range(0, scenePiecesStillValid.Count);
            chooseAPiece = scenePiecesStillValid[RandomNumberForScenePiece] as GameObject;
			validPiece = CheckIfPieceIsValid(chooseAPiece, RandomNumberForScenePiece);
			protection++;
			if (protection > 100)
			{
				Debug.Log("loopTrap");
                return null;
				//break;
			}
		} while (validPiece == false);
		return chooseAPiece;
	}

    //Checks to see if a piece can have any rooms placed adjacent to it
    bool CheckIfPieceIsValid(GameObject a_PlacedPiece, int index)
    {
		Room roomScript = a_PlacedPiece.GetComponent<Room>();
        int validPlaces = roomScript.GetBitBool();
        if(validPlaces == (int)Directions.fullRoom )
        {
            scenePiecesStillValid.RemoveAt(index);
            return false;
        }
        return true;
    }

    //chooses a valid pos adjacent to a slected piece to place a new piece
    Vector3 ChooseSpot(ref GameObject a_PieceOnBoard)
    {
		bool foundSpot = false;
		int xMove = 0;
		int yMove = 0;
		Vector3 newPiecesPos = new Vector3(0,0,0);
		while (foundSpot == false)
		{
			xMove = 0;
			yMove = 0;
			Room roomConst = a_PieceOnBoard.GetComponent<Room>();
			int bitTest = roomConst.GetBitBool();
			int randNum = Random.Range(0, 3);
			bool searching = true;

			int protectSearch = 0;
			while (searching)
			{
				if (bitTest == 15)
				{
                    Debug.Log("ERROR");
					searching = false;
				}
				if ((bitTest & (1 << randNum)) == 0)
				{
					if (randNum < 2)
					{
						yMove = randNum < 1 ? 15 : -15;
						searching = false;
					}
					else
					{
						xMove = randNum < 3 ? 15 : -15;
						searching = false;
					}
				}
				else
				{
					randNum++;

					if (randNum > 3)
					{
						randNum = 0;
					}
				}
				protectSearch++;
				if (protectSearch > 100)
				{
					break;
				}
				if (!searching)
				{ bitTest = bitTest | (1 << randNum); }
			}
			roomConst.north = (bitTest & (int)Directions.north) > 0 ? true : false;
			roomConst.east = (bitTest & (int)Directions.east) > 0 ? true : false;
			roomConst.south = (bitTest & (int)Directions.south) > 0 ? true : false;
			roomConst.west = (bitTest & (int)Directions.west) > 0 ? true : false;
			roomConst.SetBitBool(bitTest);

			newPiecesPos = a_PieceOnBoard.transform.position;
			newPiecesPos = new Vector3(newPiecesPos.x + xMove, 0, newPiecesPos.z + yMove);
			foundSpot = true;
			//when pieces are placed adjacently this checks to see if a piece was placed there already and restarts this function if it was
			for (int i = 0; i < objectCount; i++)
			{
				if (newPiecesPos == scenePieces[i].transform.position)
				{
					foundSpot = false;
					a_PieceOnBoard = FindPlacedPiece();
                    if(a_PieceOnBoard == null)
                    {
                        return new Vector3(111, 111, 111);
                    }
                    Debug.Log("PIECE MATCHED");
					break;
				}
			}
		}
        return newPiecesPos;
	}

    //creates a prefab of a chosen piece type
    void InstantiatePiece(GameObject a_chosenSpotPiece)
    {
        Vector3 newPiecesPos = ChooseSpot(ref a_chosenSpotPiece);
        if(newPiecesPos.x == 111)
        {
            Debug.Log("Loop Trap");
            return;
        }
		Vector3 piecePos = newPiecesPos;
		float xMove = newPiecesPos.x - a_chosenSpotPiece.transform.position.x;
		float zMove = newPiecesPos.z - a_chosenSpotPiece.transform.position.z;
        if (xMove >0 && zMove > 0)
        {
            Debug.Log("INSTANCE Err" + a_chosenSpotPiece.transform.position);
            Debug.Log("ERROR DIAGONAL");
        }
		Vector2 moveAmount = new Vector2(xMove, zMove);

		GameObject PieceToCreate = ChooseBoardPiece(moveAmount, a_chosenSpotPiece);
        GameObject instance = Instantiate(PieceToCreate, piecePos, PieceToCreate.transform.rotation) as GameObject;
        if(roomCountCurrent==roomCountTotal+1)
        {
            Instantiate(m_ExitTrigger, instance.transform.position, Quaternion.identity);
        }
        Room roomConst = instance.GetComponent<Room>();
        bool checkCorr = PieceToCreate.GetComponent<Room>().isCorridor;
        roomConst.isCorridor = checkCorr;
        if(roomConst.isCorridor)
        {
            instance.transform.SetParent(CorridorHolder.transform);
        }
        else { instance.transform.SetParent(RoomHolder.transform); }
        int NewRoomsValidLocations = PieceToCreate.GetComponent<Room>().GetBitBool();
        for (int i = 0; i < objectCount; i++)
        {
            if (newPiecesPos == scenePieces[i].transform.position)
            { 
                    break;
            }
            if (newPiecesPos.x - scenePieces[i].transform.position.x == -15 && newPiecesPos.z == scenePieces[i].transform.position.z)
            {
                NewRoomsValidLocations |= (int)Directions.east;
                int validation = scenePieces[i].GetComponent<Room>().GetBitBool();
                validation |= (int)Directions.west;
                scenePieces[i].GetComponent<Room>().SetBitBool(validation);
                Room roomVar = scenePieces[i].GetComponent<Room>();
                roomVar.north = (validation & (int)Directions.north) > 0 ? true : false;
                roomVar.east = (validation & (int)Directions.east) > 0 ? true : false;
                roomVar.south = (validation & (int)Directions.south) > 0 ? true : false;
                roomVar.west = (validation & (int)Directions.west) > 0 ? true : false;
                roomVar.SetBitBool(validation);
                //Debug.Log("Old P West");
            }

            else if (newPiecesPos.x - scenePieces[i].transform.position.x == 15 && newPiecesPos.z == scenePieces[i].transform.position.z)
            {
                NewRoomsValidLocations |= (int)Directions.west;
                int validation = scenePieces[i].GetComponent<Room>().GetBitBool();
                validation |= (int)Directions.east;
                scenePieces[i].GetComponent<Room>().SetBitBool(validation);
                Room roomVar = scenePieces[i].GetComponent<Room>();
                roomVar.north = (validation & (int)Directions.north) > 0 ? true : false;
                roomVar.east = (validation & (int)Directions.east) > 0 ? true : false;
                roomVar.south = (validation & (int)Directions.south) > 0 ? true : false;
                roomVar.west = (validation & (int)Directions.west) > 0 ? true : false;
                roomVar.SetBitBool(validation);
                //Debug.Log("Old P East");

            }

            else if (newPiecesPos.z - scenePieces[i].transform.position.z == 15 && newPiecesPos.x == scenePieces[i].transform.position.x)
            {
                NewRoomsValidLocations |= (int)Directions.south;
                int validation = scenePieces[i].GetComponent<Room>().GetBitBool();
                validation |= (int)Directions.north;
                scenePieces[i].GetComponent<Room>().SetBitBool(validation);
                Room roomVar = scenePieces[i].GetComponent<Room>();
                roomVar.north = (validation & (int)Directions.north) > 0 ? true : false;
                roomVar.east = (validation & (int)Directions.east) > 0 ? true : false;
                roomVar.south = (validation & (int)Directions.south) > 0 ? true : false;
                roomVar.west = (validation & (int)Directions.west) > 0 ? true : false;
                roomVar.SetBitBool(validation);
                //Debug.Log("P SOUTH");
            }

            else if (newPiecesPos.z - scenePieces[i].transform.position.z == -15 && newPiecesPos.x == scenePieces[i].transform.position.x)
            {
                NewRoomsValidLocations |= (int)Directions.north;
                int validation = scenePieces[i].GetComponent<Room>().GetBitBool();
                validation |= (int)Directions.south;
                scenePieces[i].GetComponent<Room>().SetBitBool(validation);
                Room roomVar = scenePieces[i].GetComponent<Room>();
                roomVar.north = (validation & (int)Directions.north) > 0 ? true : false;
                roomVar.east = (validation & (int)Directions.east) > 0 ? true : false;
                roomVar.south = (validation & (int)Directions.south) > 0 ? true : false;
                roomVar.west = (validation & (int)Directions.west) > 0 ? true : false;
                roomVar.SetBitBool(validation);
                //Debug.Log("P North");
            }
        }

        roomConst.north = (NewRoomsValidLocations & (int)Directions.north) > 0 ? true : false;
        //Debug.Log(roomConst.north);
        roomConst.east = (NewRoomsValidLocations & (int)Directions.east) > 0 ? true : false;
        //Debug.Log(roomConst.east);
        roomConst.south = (NewRoomsValidLocations & (int)Directions.south) > 0 ? true : false;
        //Debug.Log(roomConst.south);
        roomConst.west = (NewRoomsValidLocations & (int)Directions.west) > 0 ? true : false;
        //Debug.Log(roomConst.west);
        roomConst.SetBitBool(NewRoomsValidLocations);
        scenePieces[objectCount] = instance;
        scenePiecesStillValid.Add(instance);
		PositionsOfPiecesOnBoard[objectCount] = instance.transform.position;
		objectCount++;
	}

	//picks the type of prefab to place
    GameObject ChooseBoardPiece(Vector2 Location, GameObject basedOffPiece)
    {
		GameObject ChosenPiece;
        if(chanceOfRoom < 0)
        {
            chanceOfRoom = 0;
        }
        if(chanceOfRoom > 100)
        {
            chanceOfRoom = 100;
        }

        if (mustPlaceWTS)
        {
            if (Random.Range(0, 100) > 75)
            {
                ChosenPiece = healthRoom[Random.Range(0, healthRoom.Length)];
                //roomCountCurrent++; this room currently isnt adding to room count deliberately
                mustPlaceWTS = false;
                return ChosenPiece;
            }
        }
        if(spawnStatRoom)
        {
            if (Random.Range(0, 100) > 75)
            {
                ChosenPiece = StatUpgradeRoom[Random.Range(0, StatUpgradeRoom.Length)];
                //roomCountCurrent++; this room currently isnt adding to room count deliberately
                spawnStatRoom = false;
                return ChosenPiece;
            }
        }
        if (spawnStatRoom && roomCountCurrent == roomCountTotal - 2)
        {
            ChosenPiece = StatUpgradeRoom[Random.Range(0, StatUpgradeRoom.Length)];
            //roomCountCurrent++; this room currently isnt adding to room count deliberately
            spawnStatRoom = false;
            return ChosenPiece;
        }
        if (mustPlaceWTS && roomCountCurrent==roomCountTotal - 2)
        {
            ChosenPiece = healthRoom[Random.Range(0, healthRoom.Length)];
            //roomCountCurrent++; this room currently isnt adding to room count deliberately
            mustPlaceWTS = false;
            return ChosenPiece;
        }
            chanceOfRoom += basedOffPiece.GetComponent<Room>().isCorridor ? 50 : -50;
            int chance = Random.Range(0, 100);
            if (roomCountCurrent == roomCountTotal)
            {
                ChosenPiece = ExitRooms[(Random.Range(0, ExitRooms.Length))];
                roomCountCurrent++;
            }

            else if (chance < chanceOfRoom)
            {
                ChosenPiece = regularRooms[Random.Range(0, regularRooms.Length)];
                roomCountCurrent++;
                //chanceOfRoom -= 8;
            }
            else
            {
                ChosenPiece = CreateCorridor(Location);
                ChosenPiece.GetComponent<Room>().isCorridor = true;
                //chanceOfRoom += 10;
            }
        return ChosenPiece;
    }

    //Creates a corridor Prefab
    GameObject CreateCorridor(Vector2 Location)
    {
		int Chance = Random.Range(0, 100);
        if(Chance < 25)
        {
			GameObject LPrefab = LShape;
			AllignL(Location, ref LPrefab);
            return LPrefab;
        }
        if(Chance <50)
        {
            GameObject StraightCor = StraightCorridor;
            AllignStraight(Location, ref StraightCor);
            return StraightCor;
        }
        if (Chance < 75)
        {
            return FourWayIntersection;
        }
        GameObject Tint = TIntersection;
        AlignT(Location, ref Tint);
        return Tint;
    }

    //aligns the L corridor piece correctly
    void AllignL(Vector2 Location, ref GameObject a_LPiece)
    {
        Quaternion Ninety = new Quaternion(0, 0.7071f, 0, 0.7071f);
        Quaternion OneEighty = new Quaternion(0, 1, 0, 0);
        Quaternion TwoSeventy = new Quaternion(0, -0.7071f, 0, 0.7071f);
        Quaternion[] Rotations = new Quaternion[4];
        Rotations[0] = Quaternion.identity;
        Rotations[1] = Ninety;
        Rotations[2] = OneEighty;
        Rotations[3] = TwoSeventy;
        int xMove = (int)Location.x;
		int yMove = (int)Location.y;
		Room roomConst = a_LPiece.GetComponent<Room>();
		int bitBool = roomConst.GetBitBool();
        bitBool = 0;
        //Debug.Log("BIT PRE " + bitBool);
		int rotationAmount = Random.Range(0, 3);
        a_LPiece.transform.rotation = Quaternion.identity;
		if (xMove != 0)
		{
			bool eastPlacement = xMove > 0 ? true : false;
			if (eastPlacement)
			{
				rotationAmount = rotationAmount < 2 ? 3 : 0;
			}
			else
			{
				rotationAmount = rotationAmount < 2 ? 2 : 1;
			}
		}
		if(yMove!=0)
		{
			bool northPlacement = yMove > 0 ? true : false;
			if(northPlacement)
			{
				rotationAmount = rotationAmount < 2 ? 2 : 3;
			}
			else
			{
				rotationAmount = rotationAmount < 2 ? 0 : 1;
			}
		}
        a_LPiece.transform.rotation = Rotations[rotationAmount];

		switch (rotationAmount)
		{
			case 0:
			{
				bitBool |= (int)Directions.east;
				bitBool |= (int)Directions.south;
				break;
			}
			case 1:
			{
				bitBool |= (int)Directions.south;
				bitBool |= (int)Directions.west;
				break;
			}
			case 2:
				{
					bitBool |= (int)Directions.north;
					bitBool |= (int)Directions.west;
					break;
				}
			case 3:
				{
					bitBool |= (int)Directions.north;
					bitBool |= (int)Directions.east;
					break;
				}
			default:
				{
					break;
				}
		}
		roomConst.SetBitBool(bitBool);
        //Debug.Log(bitBool);
	}

    //aligns the straight corridor piece correctly
    void AllignStraight(Vector2 Location, ref GameObject a_StraightCor)
    {
        a_StraightCor.transform.rotation = Quaternion.Euler(0,0,0);
		Room roomConst = a_StraightCor.GetComponent<Room>();
        int bitHolder = 0;
        bitHolder |= (int)Directions.east;
        bitHolder |= (int)Directions.west;

        if (Location.x != 0)
        {
            a_StraightCor.transform.rotation = new Quaternion(0,0.7f,0,0.7f) ;
            bitHolder = 0;
            bitHolder |= (int)Directions.north;
            bitHolder |= (int)Directions.south;
        }
        roomConst.north = (bitHolder & (int)Directions.north) > 0 ? true : false;
        roomConst.east = (bitHolder & (int)Directions.east) > 0 ? true : false;
        roomConst.south = (bitHolder & (int)Directions.south) > 0 ? true : false;
        roomConst.west = (bitHolder & (int)Directions.west) > 0 ? true : false;
        roomConst.SetBitBool(bitHolder);
    }

    //aligns the T corridor piece correctly
    void AlignT(Vector2 Location, ref GameObject a_TInt)
    {
        Quaternion Ninety = new Quaternion(0, 0.7071f, 0, 0.7071f);
        Quaternion OneEighty = new Quaternion(0, 1, 0, 0);
        Quaternion TwoSeventy = new Quaternion(0, -0.7071f, 0, 0.7071f);
        Quaternion[] Rotations = new Quaternion[4];
        Rotations[0] = Quaternion.identity;
        Rotations[1] = Ninety;
        Rotations[2] = OneEighty;
        Rotations[3] = TwoSeventy;


        a_TInt.transform.rotation = Quaternion.identity;
		int xMove = 0;
        int yMove = 0;
        xMove = (int)Location.x;
        yMove = (int)Location.y;
        Room roomConst = a_TInt.GetComponent<Room>();
        int bitTest = 0;
        int RandRot = Random.Range(0, 3);

        if (xMove == 0)
        {
            bool northPlacement = yMove > 0 ? true : false;
            if (northPlacement)
            {

                if (RandRot == 1)
                {
                    RandRot++;
                }
                a_TInt.transform.rotation = Rotations[RandRot];
                //Debug.Log("north" + RandRot);
            }
            else
            {
                if(RandRot == 3)
                {
                    RandRot++;
                }
                a_TInt.transform.rotation = Rotations[RandRot];
                //Debug.Log("south" + RandRot);
            }
        }
        else
        {
            bool eastPlacement = xMove > 0 ? true : false;
            if (eastPlacement)
            {
                if (RandRot == 2)
                {
                    RandRot++;
                }
                a_TInt.transform.rotation = Rotations[RandRot];
                //Debug.Log("east" + RandRot);
            }
            else
            {
                RandRot = Random.Range(0, 3);
                if (RandRot == 0)
                {
                    RandRot = 1;
                }
                a_TInt.transform.rotation = Rotations[RandRot];
               //Debug.Log("west" + RandRot);
            }

        }
        switch (RandRot)
        {
            case 0:
                {
                    bitTest |= (int)Directions.east;
                    break;
                }
            case 1:
                {
                    bitTest |= (int)Directions.south;
                    break;
                }
            case 2:
                {
                    bitTest |= (int)Directions.west;
                    break;
                }
            case 3:
                {
                    bitTest |= (int)Directions.north;
                    break;
                }
            default:
                { break; }

        }
        roomConst.SetBitBool(bitTest);
    }

    //Locks doors that have no room adjacent to them
    //THIS STILL MISSES DOORS THAT HAVE A NON-CONNECTED CORRIDOR PIECE ADJACENT TO THEM
    void DoDoorStuff(Room roomCons, GameObject instance)
    {
        //Debug.Log(string.Format("North: {0} South {1} East: {2} West {3}", roomCons.north, roomCons.south, roomCons.east, roomCons.west));
        ArrayList Doors = new ArrayList();
        int DoorNum = 0;
        foreach (Transform child in instance.GetComponentsInChildren<Transform>())
        {
            if (child.tag == ("Door"))
            {
                Doors.Add(child.gameObject);
                DoorNum++;
            }
        }

        if (!roomCons.east)
        {
            for (int i = 0; i < Doors.Count; i++)
            {
                GameObject l_door = Doors[i] as GameObject;
                if (l_door.transform.position.x - (l_door.transform.parent.transform.position.x) >1)
                {
                    //Doors[i].GetComponent<BoxCollider>().enabled = false;
                    //Doors[i].SetActive(false)
                    l_door.tag = "Sealed";
                }

            }
        }

        if (!roomCons.west)
        {
            for (int i = 0; i < Doors.Count; i++)
            {
                GameObject l_door = Doors[i] as GameObject;
                if (l_door.transform.position.x - l_door.transform.parent.transform.position.x < -1)
                {
                    //Doors[i].GetComponent<BoxCollider>().enabled = false;
                    //Doors[i].SetActive(false);
                    l_door.tag = "Sealed";
                }

            }
        }

        if (!roomCons.north)
        {
            for (int i = 0; i < Doors.Count; i++)
            {
                GameObject l_door = Doors[i] as GameObject;
                if (l_door.transform.position.z - l_door.transform.parent.transform.position.z > 1)
                {
                    //Doors[i].GetComponent<BoxCollider>().enabled = false;
                    //Doors[i].SetActive(false);
                    l_door.tag = "Sealed";
                }

            }
        }

        if (!roomCons.south)
        {
            for (int i = 0; i < Doors.Count; i++)
            {
                GameObject l_door = Doors[i] as GameObject;
                if (l_door.transform.position.z - l_door.transform.parent.transform.position.z <-1)
                {
                    //Doors[i].GetComponent<BoxCollider>().enabled = false;
                    //Doors[i].SetActive(false);
                    l_door.tag = "Sealed";
                }

            }
        }
    }

    //Generates a boss room prefab
    //called instead of generate level
    public void GenerateBossRoom()
    {
        Instantiate(BossRoom, Vector3.zero, Quaternion.identity);
        if (!player)
        {
            player = GameObject.FindGameObjectWithTag("Player");
        }
        Vector3 Pos;
        player.transform.position = new Vector3(0,1,0);
        Pos = player.transform.position;
        player.GetComponent<FallSafety>().startPos = Pos;
    }

    /*TODO:: Lock Remaining doors
    Delete Corridors with only 1 adjacent room
    */
    void FinalParse()
    {
        LockCorridorDoorsProperly();
        fSafe++;
        bool foundDeadEnd = false;
        foreach (GameObject scenePiece in scenePieces)
        {
            Room tempHolderInsideCase = null;
            if (scenePiece)
            {
                Room tempRoom = scenePiece.GetComponent<Room>();
                if (tempRoom.isCorridor)
                {
                    int dire = 0;
                    if (tempRoom.DeadEnd() && tempRoom.gameObject.activeSelf)
                    {
                        foundDeadEnd = true;
                        int bitTemp = tempRoom.GetBitBool();
                        for (int x = 0; x <= 4; x++)
                        {
                            if ((bitTemp & (1 << x)) == 0)
                            {
                                dire = x;
                            }
                        }
                        switch (dire)
                        {
                            case (int)Directions.north:
                                {
                                    Vector3 tempPos = scenePiece.transform.position;
                                    tempPos.z += 15;
                                    foreach (GameObject piece in scenePieces)
                                    {
                                        if (piece)
                                        {
                                            tempHolderInsideCase = piece.GetComponent<Room>();
                                            if (piece.transform.position == tempPos)
                                            {
                                                tempHolderInsideCase.SealSpecificDoor((int)Directions.south);
                                            }
                                        }
                                    }
                                    break;
                                }
                            case (int)Directions.south:
                                {
                                    Vector3 tempPos = scenePiece.transform.position;
                                    tempPos.z -= 15;
                                    foreach (GameObject piece in scenePieces)
                                    {
                                        if (piece)
                                        {
                                            tempHolderInsideCase = piece.GetComponent<Room>();
                                            if (piece.transform.position == tempPos)
                                            {
                                                tempHolderInsideCase.SealSpecificDoor((int)Directions.north);
                                            }
                                        }
                                    }
                                    break;
                                }
                            case (int)Directions.east:
                                {
                                    Vector3 tempPos = scenePiece.transform.position;
                                    tempPos.x += 15;
                                    foreach (GameObject piece in scenePieces)
                                    {
                                        if (piece)
                                        {
                                            tempHolderInsideCase = piece.GetComponent<Room>();
                                            if (piece.transform.position == tempPos)
                                            {
                                                tempHolderInsideCase.SealSpecificDoor((int)Directions.west);
                                            }
                                        }
                                    }
                                    break;
                                }
                            case (int)Directions.west:
                                {
                                    Vector3 tempPos = scenePiece.transform.position;
                                    tempPos.x -= 15;
                                    foreach (GameObject piece in scenePieces)
                                        if (piece)
                                        {                                            
                                            tempHolderInsideCase = piece.GetComponent<Room>();
                                            if (piece.transform.position == tempPos)
                                            {
                                                tempHolderInsideCase.SealSpecificDoor((int)Directions.east);
                                            }                                            
                                        }
                                    break;
                                }

                        }
                        tempHolderInsideCase.north = (tempRoom.GetBitBool() & (int)Directions.north) < 0 ? true : false;
                        //Debug.Log(roomConst.north);
                        tempHolderInsideCase.east = (tempRoom.GetBitBool() & (int)Directions.east) < 0 ? true : false;
                        //Debug.Log(roomConst.east);
                        tempHolderInsideCase.south = (tempRoom.GetBitBool() & (int)Directions.south) < 0 ? true : false;
                        //Debug.Log(roomConst.south);
                        tempHolderInsideCase.west = (tempRoom.GetBitBool() & (int)Directions.west) < 0 ? true : false;
                        scenePiece.SetActive(false);
                    }
                }
            }
        }
        if(foundDeadEnd && fSafe < 100)
        {
            Debug.Log("Count " + fSafe);
            foundDeadEnd = false;
            FinalParse();
        }
        LockCorridorDoorsProperly();
    }

    void LockCorridorDoorsProperly()
    {
        Door[] allDoors = FindObjectsOfType<Door>();
        for (int i = 0; i < allDoors.Length; i++)
        {
            if (allDoors[i].tag == "Door" || allDoors[i].tag == "Locked")
            {
                int foundDoor = 0;
                bool nearbyDoor = false;
                Collider[] hitColliders = Physics.OverlapSphere(allDoors[i].transform.position, 0.2f);
                int x = 0;
                while (x < hitColliders.Length)
                {
                    if (hitColliders[x].gameObject.tag == ("Door"))
                    {
                        foundDoor++;
                        if (foundDoor > 1)
                        { nearbyDoor = true; }
                    }
                    x++;
                }
                if (nearbyDoor == false)
                {
                    allDoors[i].tag = ("Sealed");
                }
            }
        }
    }

}
