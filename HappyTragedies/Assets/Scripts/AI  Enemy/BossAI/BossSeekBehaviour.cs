﻿using UnityEngine;
using System.Collections;

public class BossSeekBehaviour : AIBehaviour
{

    GameObject      m_Player;
    NavMeshAgent    m_Agent;
    BossAI          m_AI;  

    public override eResult Execute()
    {
        m_Agent.SetDestination(m_Player.transform.position);
        m_AI.SetAnimationState(1); //walking
        return eResult.Success; 
    }

    public override eResult Evaluate()
    {
        if (m_Player == null)
            m_Player = GameObject.FindGameObjectWithTag("Player");
        if (m_Agent == null)
            m_Agent = GetComponent<NavMeshAgent>();
        if (m_AI == null)
            m_AI = GetComponent<BossAI>(); 

        return eResult.Success; 
    }
}
