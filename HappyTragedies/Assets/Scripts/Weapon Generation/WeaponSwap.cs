﻿using UnityEngine;
using System.Collections;

public class WeaponSwap : MonoBehaviour
{
    //===============================
    //Public Vars
    [Tooltip("A reference to the canvas used as a dialog to see if the player wants to change their weapon")]
    public GameObject m_ChangeWeaponDialog;
    [Tooltip("A reference to the canvas used as a dialog to see if the player wants to store their weapon")]
    public GameObject m_StoreWeaponDialog;
    public GameObject m_WeaponStatPopupPrefab;
    //===============================

    //===============================
    //Private Vars
    bool m_LockChanged = false;    //Whether or not this WTS has been used to change weapons
    bool m_LockStored = false;    //Whether or not this WTS has been used to store weapons
    bool m_LockSystem = false;    //Whether or not the system is locked (after both swap and change have been used)
    bool m_playerIsClose = false;    //Whether or not the player is close
    bool m_isFirstLevel = true;     //Whether or not this is the first level
    GameObject m_StoredWeapon;                     //The weapon that will be availiable to the player on level 1
    GameObject m_NewWeapon;                        //The weapon that has been spawned for this WTS
    BoxCollider m_pickUpTrigger;                    //The weapon pickup trigger box
    GameObject m_WeaponStatPopup;
    ParticleSystem m_DropParticles;
    //===============================

    // Use this for initialization
    void Start()
    {
        if (gameObject.tag == "Weapon" || gameObject.tag == "Gun") //We need to make sure that if this script is on a weapon, the player only can pick this weapon up
        {
            m_NewWeapon = gameObject;
            m_LockStored = true;
            m_pickUpTrigger = gameObject.AddComponent<BoxCollider>();
            m_pickUpTrigger.isTrigger = true;

            m_WeaponStatPopupPrefab = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>().m_PickupPopup;
            gameObject.layer = 0;
            m_WeaponStatPopup = Instantiate(m_WeaponStatPopupPrefab);
            m_WeaponStatPopup.transform.position = gameObject.transform.position;
            m_WeaponStatPopup.transform.SetParent(gameObject.transform);
            //m_WeaponStatPopup.transform.localPosition = new Vector3(0, 0, 0);
            m_WeaponStatPopup.SetActive(false);
            foreach(MeshRenderer mesh in gameObject.GetComponentsInChildren<MeshRenderer>())
            {
                mesh.enabled = true;
            }
            m_NewWeapon.GetComponent<WeaponExperience>().enabled = false;
            if (m_NewWeapon.GetComponent<MeleeAttack>())
                m_NewWeapon.GetComponent<MeleeAttack>().enabled = false;
            else if (m_NewWeapon.GetComponent<GunAttack>())
                m_NewWeapon.GetComponent<GunAttack>().enabled = false;

            InitialiseDropParticles(); 
        }
        else if (gameObject.tag == "WTS")
        {
            //This finds our WeaponGenerator and asks it for a random weapon
            m_NewWeapon = GameObject.FindGameObjectWithTag("GameManager").GetComponent<WeaponGenerator>().GenerateWeapon(100);
            m_NewWeapon.name = "RandomWep";
            m_isFirstLevel = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>().m_currentLevel == -25 ? true : false;
            m_NewWeapon.SetActive(false);
            //Load in stored weapon
            m_StoredWeapon = LoadStoredWeapon();
            if (m_StoredWeapon == null)
            {
				SetStoredWeapon(Instantiate(GameObject.FindGameObjectWithTag("GameManager").GetComponent<WeaponGenerator>().m_riflePrefab));
                SaveStoredWeapon();
                m_StoredWeapon = LoadStoredWeapon();
            }
            m_StoredWeapon.name = "WTS Stored Wep";
            m_StoredWeapon.SetActive(false);
            if (!m_WeaponStatPopup)
                m_WeaponStatPopup = GameObject.FindGameObjectWithTag("Stats").GetComponentInChildren<UIToggle>().GetWTSCard();
            //m_WeaponStatPopup.GetComponent<WTSLockUI>().SetStats(GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>().GetWeapon().GetComponent<Weapon>(), m_StoredWeapon.GetComponent<Weapon>());
            //m_WeaponStatPopup.SetActive(false);
            //DontDestroyOnLoad(m_WeaponStatPopup);

        }
    }

    void OnEnable()
    {
        if (m_DropParticles == null)
            InitialiseDropParticles(); 
        m_DropParticles.Play(); 
    }

    //We use our update to check if the player would like to swap their weapons and interact with the WTS
    void Update()
    {

        if (Input.GetButton("Fire1") || GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>().GetAttackState())
            return;
        //First check the system isn't locked
        if (!m_LockSystem && m_playerIsClose)
        {
            if (!m_LockChanged && Input.GetButtonDown("Swap"))
            {
                //Gives the player the new randomly generated weapon, replacing their old one (we will probably put dialogs in as a check) 
                PlayerController Player = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>();
                if (m_isFirstLevel && Player != null && gameObject.tag == "WTS")
                {
                    m_StoredWeapon.SetActive(true);
                    //Give the player their stashed weapon
                    Player.SetWeapon(m_StoredWeapon);

                    //Stash default weapon
					SetStoredWeapon(Instantiate(GameObject.FindGameObjectWithTag("GameManager").GetComponent<WeaponGenerator>().m_riflePrefab));
                    m_StoredWeapon.name = "WTS Stored Wep";
                    m_StoredWeapon.SetActive(false);
                    SaveStoredWeapon();
                    m_LockStored = true;
                    m_LockChanged = true;
                    //m_WeaponStatPopup.GetComponent<WTSLockUI>().SetStats(Player.GetWeapon().GetComponent<Weapon>(), m_StoredWeapon.GetComponent<Weapon>());

                }
                else
                {
                    if(Player.GetComponent<Animator>().GetBool("Reloading") == true)
                    { return; }
                    m_NewWeapon.SetActive(true);
                    Vector3 temp = m_NewWeapon.transform.position;
                    GameObject oldWep = Instantiate(Player.GetWeapon());
                    if ( gameObject.tag == "Weapon" || gameObject.tag == "Gun")
                    {
                        //Give player weapon pickup
                        gameObject.layer = 8;
                        Destroy(m_pickUpTrigger);
                        Destroy(m_WeaponStatPopup);
                        if (m_DropParticles != null)
                            m_DropParticles.Stop();  
                        Player.SetWeapon(m_NewWeapon);
                        m_NewWeapon = null;
                        m_NewWeapon = oldWep;
                        m_NewWeapon.GetComponent<WeaponSwap>().enabled = true;
                        m_NewWeapon.GetComponent<WeaponExperience>().enabled = false;
                        if (m_NewWeapon.GetComponent<MeleeAttack>())
                            m_NewWeapon.GetComponent<MeleeAttack>().enabled = false;
                        else if (m_NewWeapon.GetComponent<GunAttack>())
                            m_NewWeapon.GetComponent<GunAttack>().enabled = false;
                        //Set pickup
                        m_NewWeapon.transform.position = temp;
                        //m_NewWeapon.AddComponent<WeaponSwap>();
                        //Destroy(this);
                        m_LockChanged = false;
                    }
                    else if (gameObject.tag == "WTS")
                    {
                        Player.SetWeapon(m_NewWeapon);
                        m_WeaponStatPopup.GetComponent<WTSLockUI>().SetStats(Player.GetWeapon().GetComponent<Weapon>(), m_StoredWeapon.GetComponent<Weapon>());
                        //m_NewWeapon.SetActive(false);
                        Destroy(oldWep);
                        m_LockChanged = true;
                        m_LockStored = true;
                    }
                }
            }
            //Stash current weapon
            else if (!m_LockStored && Input.GetButtonDown("Stash") && !m_isFirstLevel)
            {
                PlayerController Player = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>();

                if (Player != null)
                {
                    //copy old wep
                    GameObject oldWep = Player.GetWeapon();
                    //m_StoredWeapon.SetActive(true);

                    //give player random wep
                    m_NewWeapon.SetActive(true);
                    Player.SetWeapon(m_NewWeapon);
                    //Stash old wep
                    SetStoredWeapon(oldWep);
                    m_StoredWeapon.name = "new WTS Stored Wep";
                    SaveStoredWeapon();
                    m_StoredWeapon.SetActive(false);
                    m_LockChanged = true;
                    m_LockStored = true;
                    m_WeaponStatPopup.GetComponent<WTSLockUI>().SetStats(Player.GetWeapon().GetComponent<Weapon>(), m_StoredWeapon.GetComponent<Weapon>());

                }
            }
            if (m_LockStored && m_LockChanged)
            {
                m_LockSystem = true;
                if(m_WeaponStatPopup)
                    m_WeaponStatPopup.GetComponent<WTSLockUI>().LockSystem();
            }
        }
    }

    //If the player is close enough to the weapon swap
    void OnTriggerEnter(Collider col)
    {
        if (col.tag == "Player")
        {
            m_playerIsClose = true;
            if (gameObject.tag == "WTS")
                return;
            //m_WeaponStatPopup.GetComponent<WTSLockUI>().SetStats(GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>().GetWeapon().GetComponent<Weapon>(), m_StoredWeapon.GetComponent<Weapon>());
            else
            {
                m_WeaponStatPopup.SetActive(true);
                m_WeaponStatPopup.GetComponent<WeaponDropStatsUI>().SetStats();
            }
            //Debug.DrawLine(m_WeaponStatPopup.transform.position + new Vector3(0, 5, 0), gameObject.transform.position, Color.white, 10.0f);
        }
    }

    //when the player is no longer close to the weapon swap
    void OnTriggerExit(Collider col)
    {
        if (col.tag == "Player")
        {
            m_playerIsClose = false;
            if (gameObject.tag == "WTS")
                return;
            m_WeaponStatPopup.SetActive(false);
        }
    }

    void InitialiseDropParticles()
    {
        if (m_DropParticles == null)
        {
            //Create a particle system to bring attention to the gun, but set it to default automatically
            WeaponGenerator wpGen = GameObject.FindObjectOfType<WeaponGenerator>();
            GameObject PSInstance = GameObject.Instantiate(wpGen.GetWeaponDropParticles(GetComponent<Weapon>().m_Rarity), transform) as GameObject;
            m_DropParticles = PSInstance.GetComponent<ParticleSystem>();
            m_DropParticles.Stop();
        }
    }

    //Returns the weapon currently in storage
    public GameObject GetStoredWeapon()
    {
        return m_StoredWeapon;
    }

    //Sets the weapon in storage
    public void SetStoredWeapon(GameObject a_StoredWeapon)
    {
        //Destroy(m_StoredWeapon);
        m_StoredWeapon = null;
        m_StoredWeapon = a_StoredWeapon;
    }

    //Returns a reference to the weapon created for this WTS
    public GameObject GetNewWeapon()
    {
        return m_NewWeapon;
    }

    //Saves the data of the stored weapon to Unity PlayerPrefs
    public void SaveStoredWeapon()
    {
        Weapon weapon = m_StoredWeapon.GetComponent<Weapon>();
        PlayerPrefs.SetInt("StoredRarity", (int)weapon.m_Rarity);
        PlayerPrefs.SetInt("StoredLevelCap", weapon.m_levelCap);
        PlayerPrefs.SetInt("StoredLevel", weapon.m_weaponLevel);
        PlayerPrefs.SetInt("StoredKnockback", weapon.m_knockBackForce);
        PlayerPrefs.SetFloat("StoredDamage", weapon.m_damage);
        PlayerPrefs.SetFloat("StoredAttackRate", weapon.m_attackRate);
        PlayerPrefs.SetFloat("StoredReloadSpeed", (float)weapon.m_reloadTime);
        PlayerPrefs.SetInt("StoredClipSize", weapon.m_clipSize);

        PlayerPrefs.SetInt("StoredType", (int)weapon.m_WeaponType);
        PlayerPrefs.Save();

    }

    //Loads the data of the stored weapon from Unity PlayerPrefs 
    public GameObject LoadStoredWeapon()
    {
        //Find the type of weapon we are loading from playerprefs, then create a GameObject to store it in
        WEAPON_TYPE weaponType = (WEAPON_TYPE)PlayerPrefs.GetInt("StoredType", 9999);
        GameObject LoadedWeapon = null;

        //If weapon type is equal to the default value we should return null immediately.
        if (weaponType == WEAPON_TYPE.DEFAULT)
        {
            return null;
        } //Otherwise, we need to see what weapon type we want, then ask the weapon generator to give us one of that type
        else if (weaponType == WEAPON_TYPE.WEAPON_RIFLE)
            LoadedWeapon = GameObject.FindGameObjectWithTag("GameManager").GetComponent<WeaponGenerator>().GenerateWeaponDef(WEAPON_TYPE.WEAPON_RIFLE, 100);
        else if (weaponType == WEAPON_TYPE.WEAPON_PISTOL)
            LoadedWeapon = GameObject.FindGameObjectWithTag("GameManager").GetComponent<WeaponGenerator>().GenerateWeaponDef(WEAPON_TYPE.WEAPON_PISTOL, 100);
        else if (weaponType == WEAPON_TYPE.WEAPON_SHOTGUN)
            LoadedWeapon = GameObject.FindGameObjectWithTag("GameManager").GetComponent<WeaponGenerator>().GenerateWeaponDef(WEAPON_TYPE.WEAPON_SHOTGUN, 100);
        else if (weaponType == WEAPON_TYPE.WEAPON_MELEE)
            LoadedWeapon = GameObject.FindGameObjectWithTag("GameManager").GetComponent<WeaponGenerator>().GenerateWeaponDef(WEAPON_TYPE.WEAPON_MELEE, 100);
        if (LoadedWeapon != null) //Now we have our reference we need to load in the values from player prefs 
        {
            Weapon weapon = LoadedWeapon.GetComponent<Weapon>();
            weapon.m_Rarity = (Weapon.Rarity)PlayerPrefs.GetInt("StoredRarity");
            weapon.m_levelCap = PlayerPrefs.GetInt("StoredLevelCap");
            weapon.m_weaponLevel = PlayerPrefs.GetInt("StoredLevel");
            weapon.m_knockBackForce = PlayerPrefs.GetInt("StoredKnockback");
            weapon.m_damage = PlayerPrefs.GetFloat("StoredDamage");
            weapon.m_attackRate = PlayerPrefs.GetFloat("StoredAttackRate");
            weapon.m_reloadTime = PlayerPrefs.GetFloat("StoredReloadSpeed");
            weapon.m_clipSize = PlayerPrefs.GetInt("StoredClipSize");
            weapon.m_currentClip = weapon.m_clipSize;
            weapon.m_WeaponType = weaponType;

            return LoadedWeapon;
        }

        return null; //this will return null if the weapon generator couldn't give us a weapon
    }
}


