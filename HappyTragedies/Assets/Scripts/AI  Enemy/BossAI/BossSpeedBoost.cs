﻿using UnityEngine;
using System.Collections;

public class BossSpeedBoost : AIBehaviour {

    NavMeshAgent m_Agent;
    BossAI m_AI; 
    float m_boost; 

    public override eResult Execute()
    {
        m_Agent.speed *= m_boost;
        m_AI.SetHasDone(2); 
        return eResult.Success; 
    }

    public override eResult Evaluate()
    {
        if (m_Agent == null)
            m_Agent = GetComponent<NavMeshAgent>();
        if (m_AI == null)
            m_AI = GetComponent<BossAI>(); 
        if (m_boost == 0)
            m_boost = GetComponent<BossAI>().GetBoostAmount(); 

        return eResult.Success; 
    }

}
