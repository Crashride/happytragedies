﻿using UnityEngine;
using System.Collections;

public class HurtDefaultBehaviour : AIBehaviour {

    //===============
    //Private Vars 
    EnemyAI m_AI;   //A reference to the AI component on this game object 
    eResult m_State = eResult.Fail; //The current stage of the behaviour | Fail = not started | In Progress = In Progress | Success = Finished 
    //==============

    public override eResult Execute()
    {
        if (m_State == eResult.Fail)
        {
            m_State = eResult.InProgress;
            //Stop the AI from doing anything 
            m_AI.PurgeInProgressActions();
            GetComponent<NavMeshAgent>().enabled = false; 
            //Play the hurt animation
            m_AI.SetAnimationState(4);
            return m_State;
        }

        return m_State; 
    }
    public override eResult Evaluate()
    {
        m_State = eResult.Fail; 
        if (m_AI == null)
            m_AI = GetComponent<EnemyAI>(); 

        return eResult.Success; 
    }

    public void FinishedHurting()
    {
        GetComponent<NavMeshAgent>().enabled = true;
        m_AI.SetAnimationState(0); 
        m_State = eResult.Success;
        GetComponent<Rigidbody>().isKinematic = true; 
    }
}
