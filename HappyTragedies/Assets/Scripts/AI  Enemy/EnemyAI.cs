﻿using UnityEngine;
using System.Collections;
using System.Reflection;

/// <summary>
/// AUTHOR: DOUGLAS LANDERS 
/// DATE: 8/8/16
/// The enemy AI script is the base script added to any enemy
/// object to get them to begin using the AI system. 
/// </summary>
[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(NavMeshAgent))]
[RequireComponent(typeof(Health))]
public class EnemyAI : MonoBehaviour
{
    public enum AI_STATE
    {
        IDLE = 0,
        WALKING = 1,
        ATTACKING = 2,
        DYING = 3,
        DEAD = 4
    }

    //================================
    //AI events
    [Tooltip("The behaviour executed to spawn this AI into the world")]
    public string m_SpawnEvent = "empty";
    [Tooltip("The behaviour to be execute when health is low")]
    public string m_OnLowHealth = "empty";
    [Tooltip("The behaviour to be executed when the player is spotted")]
    public string m_OnSeePlayer = "empty";
    [Tooltip("The behaviour to be executed when the player is lost")]
    public string m_OnLosePlayer = "empty";
    [Tooltip("The behaviour that this AI uses to prepare to attack")]
    public string m_AttackPrepare = "empty";
    [Tooltip("The first option for attacking")]
    public string m_Attack1 = "empty";
    [Tooltip("the second option for attacking")]
    public string m_Attack2 = "empty";
    [Tooltip("What behaviour to be executed when the AI is hurt")]
    public string m_OnHurt = "empty";
    [Tooltip("The behaviour to execute when nothing is happening")]
    public string m_OnIdle = "empty";
    [Tooltip("The behaviour to execute when the AI dies")]
    public string m_OnDeath = "empty";

    AIBehaviour m_SpawnBehaviour;
    AIBehaviour m_LowHealthBehaviour;
    AIBehaviour m_SeePlayerBehaviour;
    AIBehaviour m_LosePlayerBehaviour;
    AIBehaviour m_AttackPrepareBehaviour;
    AIBehaviour m_Attack1Behaviour;
    AIBehaviour m_Attack2Behaviour;
    AIBehaviour m_HurtBehaviour;
    AIBehaviour m_IdleBehaviour;
    AIBehaviour m_DeathBehaviour;
    //================================


    //================================
    //Public Vars
    [Tooltip("The duration of the shake that occurs on death xplosion")]
    public float m_XplodeShakeDuration;
    [Range(0, 1.0f)]
    [Tooltip("The intensity of the shake that occurs on death xplosion")]
    public float m_XplodeShakeIntensity;
    [Tooltip("The % of health where is AI will consider itself low on health")]
    public float m_LowHealthThreshold;
    [Tooltip("The maximum range between the AI and Player for sighting")]
    public float m_ViewDistance;
    [Tooltip("The maximum distance the AI can attack (ranged only)")]
    public float m_AttackRange;
    [Tooltip("The base amount of damage this enemy will apply")]
    public float m_BaseDamage;
    [Tooltip("The factor used to change the speed of projectiles")]
    public float m_SpeedFactor;
    [Tooltip("How long the AI should search for the player")]
    public float m_SearchTime;
    [Tooltip("How much base currency will be rewarded to the player upon death")]
    public float m_KillReward;
    [Tooltip("How much base EXP will be rewarded to player")]
    public int m_EXPReward;
    [Tooltip("How long the enemy should wait between attacks")]
    public float m_AttackCooldown;
    [Tooltip("A reference to a prefab of the projectile object")]
    public GameObject m_ProjectilePrefab;
    [Tooltip("A reference to the prefab of this enemy's spawner")]
    public GameObject m_SpawnPointPrefab;
    [Tooltip("A particle system used as a bleeding effect")]
    public ParticleSystem m_BloodEffect;
    [Tooltip("A particle system used to signal preparing for a ranged attack")]
    public ParticleSystem m_RangedEffect;
    [Tooltip("Death Particle Animation")]
    public ParticleSystem m_deathXplode;
    [Tooltip("The amount of different groups that will take turns at updating less critical systems HIGHER NUMBER = FASTER SPEED / LOWER ACCURACY")]
    public int m_UpdateGroups;
    [Tooltip("Whether or not this class will print debug logs. Used only for debugging, and should be removed later in beta.")]
    public bool m_DoDebug;
    [Tooltip("Whether or not the AI is currently active")]
    public bool m_IsActive = false;
    //================================


    //================================
    //Private Vars 
    [SerializeField]
    bool m_PlayerSighted = false;       //Whether or not the AI has spotted the player 
    [SerializeField]
    bool m_PlayerClose = false;       //Whether or not the AI is close to the player, used for melee attacks. 
    bool m_IsDead = false;       //Whether or not the AI is 'alive'
    bool m_IsLowHealth = false;       //whether or not the AI is low on health
    bool m_AreBehavioursSetUp = false;                //Whether or not the behaviours for this AI have been initialised
    float m_SincePlayerSeen;                //The amount of time since the player has been spotted
    Health m_Health;                         //A reference to the AIs health component
    GameObject m_Player;                         //A reference to the Player object 
    Vector3 m_LastKnownPlayerPosition;        //A Vec3 containing the last known player position
    Animator m_Animator;                       //A reference to the AIs animator component
    AIBehaviour m_InProgressAction;               //A list of inprogress actions 
    float m_SinceLastAttack = 10.0f;        //A cpunter since the last attack was dealt
    AI_STATE m_CurrentState;                   //The current state the AI is in, used for animating
    Room m_Room;                           //A reference to the room this enemy was placed in 
    int m_LayerMask;                      //The layermask used to ignore certain layers in raycasts
    int m_UpdateGroup;                    //The update group this AI Belongs too 
    int m_FrameCounter;                   //Used to count the frames and is used for finding update groups 
                                          //================================

    // Get our references and set default values
    void Start()
    {
        m_Health = GetComponent<Health>();
        m_Player = GameObject.FindGameObjectWithTag("Player");
        m_Animator = GetComponent<Animator>();

        //Setup our layermask for raycasting
        m_LayerMask = (1 << 10) | (1 << 12);
        m_LayerMask = ~m_LayerMask;

        //Test the blood effect
        if (m_BloodEffect)
        {
            m_BloodEffect.Clear();
            m_BloodEffect.Stop();
        }
        //Test the range effect
        if (m_RangedEffect)
        {
            m_RangedEffect.Clear();
            m_RangedEffect.Stop();
        }

        //figure out our update group 
        m_UpdateGroup = Random.Range(0, m_UpdateGroups - 1);
        m_FrameCounter = 1;

        m_LastKnownPlayerPosition = Vector3.zero;
        SetActionState(1);

        if (!m_AreBehavioursSetUp)
            SetUpBehaviours();

    }

    public void SetUpBehaviours()
    {
        //Initialise our behaviours 
        m_SpawnBehaviour = InitialiseBehaviour(m_SpawnEvent);
        m_LowHealthBehaviour = InitialiseBehaviour(m_OnLowHealth);
        m_SeePlayerBehaviour = InitialiseBehaviour(m_OnSeePlayer);
        m_LosePlayerBehaviour = InitialiseBehaviour(m_OnLosePlayer);
        m_AttackPrepareBehaviour = InitialiseBehaviour(m_AttackPrepare);
        m_Attack1Behaviour = InitialiseBehaviour(m_Attack1);
        m_Attack2Behaviour = InitialiseBehaviour(m_Attack2);
        m_HurtBehaviour = InitialiseBehaviour(m_OnHurt);
        m_IdleBehaviour = InitialiseBehaviour(m_OnIdle);
        m_DeathBehaviour = InitialiseBehaviour(m_OnDeath);

        m_AreBehavioursSetUp = true;
    }

    //Initialisation Helper function, determines if a behaviour should be empty or 
    //if it should add a component of a specific type
    AIBehaviour InitialiseBehaviour(string a_BehaviourName)
    {
        if (a_BehaviourName == "empty")
        {
            return gameObject.AddComponent<AIBehaviour>();
        }
        else
        {
            return CreateBehaviourFromName(a_BehaviourName) as AIBehaviour;
        }
    }

    //Generation function used to add components of a given type, prodivded by Matt
    private AIBehaviour CreateBehaviourFromName(string strBehaviourName)
    {
        System.Type t = Assembly.GetAssembly(this.GetType()).GetType(strBehaviourName);

        if (GetComponent(strBehaviourName))
        {
            //return GetComponent(strBehaviourName) as AIBehaviour; 
        }
        AIBehaviour newBehaviour = gameObject.AddComponent(t) as AIBehaviour;


        return newBehaviour;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (!m_IsDead)
        {
            m_SinceLastAttack += Time.deltaTime; //quickly and quitely increase our timer
            if (m_IsActive)
            {
                //center the AI's y on 0
                Vector3 temp = transform.position;
                temp.y = 0;
                transform.position = temp;
            }

            //figure out if its our
            bool updatethisframe = false;

            if (m_FrameCounter % m_UpdateGroups == m_UpdateGroup)
                updatethisframe = true;
            m_FrameCounter++;

            //check we have health
            float CurHealth = m_Health.GetHealth();
            if (CurHealth <= 0)
            {
                m_IsDead = true;
                if (m_DeathBehaviour.Evaluate() == AIBehaviour.eResult.Success)
                    m_DeathBehaviour.Execute();
                return;
            }
            //Second, we see if the AI was in the middle of something last frame
            if (m_InProgressAction != null)
            {
                AIBehaviour.eResult result = m_InProgressAction.Execute();
                if (result == AIBehaviour.eResult.Success || result == AIBehaviour.eResult.Fail)
                {
                    m_CurrentState = AI_STATE.WALKING;
                    SetAnimationState(0);
                    m_InProgressAction = null;
                    return;
                }
                else return;
            }
            //Now, we check if our health is low
            if (m_IsLowHealth == false && CurHealth <= m_Health.GetMaxHealth() * (m_LowHealthThreshold / 100))
            {
                m_IsLowHealth = true;
                if (m_LowHealthBehaviour.Evaluate() == AIBehaviour.eResult.Success)
                {
                    m_LowHealthBehaviour.Execute();
                    return;  //if our health is now low we should do our onlowhealth action and leave the function
                }
            }
            //now we check if we can see the player
            RaycastHit rayHit;


            if (updatethisframe && Physics.Raycast(transform.position + new Vector3(0, 0.5f, 0), Vector3.Normalize(m_Player.transform.position - transform.position), out rayHit, m_ViewDistance, m_LayerMask))
            {
                //thisisadebugthing = rayHit.collider.tag;
                if (rayHit.collider.tag == "Player" && rayHit.distance <= m_ViewDistance && !m_PlayerSighted) //if we see the player for this first time, active OnPlayerSighted
                {
                    m_PlayerSighted = true;
                    m_LastKnownPlayerPosition = m_Player.transform.position;
                    if (m_SeePlayerBehaviour.Evaluate() == AIBehaviour.eResult.Success)
                    {
                        m_SeePlayerBehaviour.Execute();
                        return;
                    }
                }
                else if ((rayHit.collider.tag != "Player" && m_PlayerSighted) || (rayHit.distance > m_ViewDistance && m_PlayerSighted)) //if we don't see the player but we used to, active OnPlayerLost
                {
                    m_PlayerSighted = false;
                    m_SincePlayerSeen = 0;
                    if (m_LosePlayerBehaviour.Evaluate() == AIBehaviour.eResult.Success)
                        m_LosePlayerBehaviour.Execute();
                }
            }
            //Update timer if player isn't spotted
            if (!m_PlayerSighted)
            {
                m_SincePlayerSeen += Time.deltaTime;
            }


            //Figure out an attack to try and do, the evaulute() method will check to see if the player is visible so we don't have to first
            if (m_CurrentState != AI_STATE.ATTACKING)
            {
                {
                    AIBehaviour.eResult result = m_Attack1Behaviour.Evaluate(); //see if we can attack 
                    if (result == AIBehaviour.eResult.Success)
                    {
                        if (m_Attack1Behaviour.Execute() == AIBehaviour.eResult.InProgress) //if we can, we do, if it is in progress, store it as our InProgressBehaviour and change our AI_STATE
                        {
                            if (m_Attack1Behaviour.GetType() == typeof(VomitAttack))//Play different sound depending on grunt type
                                PlaySound(3);
                            else
                                PlaySound(0);

                            m_InProgressAction = m_Attack1Behaviour;
                            m_CurrentState = AI_STATE.ATTACKING;
                            return;
                        }
                    }
                    else if (result == AIBehaviour.eResult.Fail && m_AttackPrepareBehaviour.Evaluate() == AIBehaviour.eResult.Success) //otherwise, see if we are able to prepare to attack
                    {
                        m_AttackPrepareBehaviour.Execute();
                        return;
                    }


                }
            }
            //if we have done everything else, we will see if we have lost the player and searched long enough to revert to idle
            if (!m_PlayerSighted && m_SincePlayerSeen >= m_SearchTime && m_CurrentState != AI_STATE.IDLE)
            {
                m_CurrentState = AI_STATE.IDLE;
                m_PlayerSighted = false;
            }

            if (m_CurrentState == AI_STATE.IDLE && m_IdleBehaviour.Evaluate() == AIBehaviour.eResult.Success)
                m_IdleBehaviour.Execute();
        }
        else
        {
            if (!m_deathXplode.IsAlive())
            {
                if (GetComponent<DyingDefaultBehaviour>())
                    GetComponent<DyingDefaultBehaviour>().FinishedDying();
            }
        }
    }

    //Returns whether or not the AI is dead
    public bool IsAIDead()
    {
        return m_IsDead;
    }

    //Help function used to find if an AI is close to the player
    public bool IsPlayerClose()
    {
        return m_PlayerClose;
    }

    //Resets the cool down for the enemies attack
    public void ResetAttackCounter()
    {
        m_SinceLastAttack = 0.0f;
    }

    public int GetLayerMask()
    {
        return m_LayerMask;
    }

    public float GetSpeedFactor()
    {
        return m_SpeedFactor;
    }

    //returns whether or not this AI can see the player
    public bool CanSeePlayer()
    {
        return m_PlayerSighted;
    }

    //Called by different weapons to damage enemy's health
    public void TakeDamage(float a_damage)
    {
        if (!m_IsDead)
        {
            m_Health.DoDamage(a_damage);
            PlayOnHitParticles();
            if (m_HurtBehaviour.Evaluate() == AIBehaviour.eResult.Success)
                if (m_HurtBehaviour.Execute() == AIBehaviour.eResult.InProgress)
                {
                    m_InProgressAction = m_HurtBehaviour;
                }
        }
    }

    // Clears / Cancels the in progress actions 
    public void PurgeInProgressActions()
    {
        m_InProgressAction = null;
    }

    public int GetState()
    {
        return (int)m_CurrentState;
    }

    public void SetRoom(Room a_room)
    {
        m_Room = a_room;
    }


    public Room GetRoom()
    {
        return m_Room;
    }

    //Retruns the Vec3 position where the AI last saw the player
    public Vector3 GetLastKnownPlayerPosition()
    {
        return m_LastKnownPlayerPosition;
    }

    //Used to play the blood effect particles 
    public void PlayOnHitParticles()
    {
        if (m_BloodEffect)
        {
            if (m_BloodEffect.isPlaying)
                return;
            m_BloodEffect.Clear();
            m_BloodEffect.Play();
        }

    }

    public void ScaleDamageFactor(float a_factor)
    {
        m_BaseDamage *= a_factor;
    }

    public void ScaleKillReward(float a_factor)
    {
        m_KillReward *= a_factor;
        m_EXPReward = (int)(m_EXPReward * a_factor);
    }

    public void ScaleEnemyHealth(float a_factor)
    {
        Health health = GetComponent<Health>();
        health.SetMaxHealth(health.GetMaxHealth() * a_factor);
    }

    public float GetKillReward()
    {
        return m_KillReward;
    }

    public int GetExpReward()
    {
        return m_EXPReward;
    }

    //sets the animation state to the specified int
    public void SetAnimationState(int a_StateInt)
    {
        if (m_Animator == null)
            m_Animator = GetComponent<Animator>();
        m_Animator.SetInteger("State", a_StateInt);
    }

    //Sets the specified bool to the specified value
    public void SetAnimationBool(string a_BoolName, bool a_bool)
    {
        m_Animator.SetBool(a_BoolName, a_bool);
    }

    public void SetActionState(int a_state)
    {
        switch (a_state)
        {
            case 0:
                m_CurrentState = AI_STATE.IDLE;
                break;
            case 1:
                m_CurrentState = AI_STATE.WALKING;
                break;
            case 2:
                m_CurrentState = AI_STATE.ATTACKING;
                break;
            case 3:
                m_CurrentState = AI_STATE.DYING;
                break;
            case 4:
                m_CurrentState = AI_STATE.DEAD;
                break;
            default:
                break;
        }
    }

    //Returns a reference to the projectile prefab used by the enemy
    public GameObject GetProjectilePrefab()
    {
        return m_ProjectilePrefab;
    }

    public void Activate()
    {
        this.enabled = true;
        if (m_SpawnBehaviour.Evaluate() == AIBehaviour.eResult.Success)
            if (m_SpawnBehaviour.Execute() == AIBehaviour.eResult.InProgress)
                m_InProgressAction = m_SpawnBehaviour;
    }

    public void DeActivate()
    {
        this.enabled = false;
        m_IsActive = false;
    }

    //Returns a reference to the ranged effect particle system
    public ParticleSystem GetRangedParticles()
    {
        return m_RangedEffect;
    }

    //Returns the maximum attack range of the Enemy
    public float GetAttackRange()
    {
        return m_AttackRange;
    }

    //On trigger function used for seeing if the player is close
    void OnTriggerExit(Collider col)
    {
        if (col.gameObject.tag == "Player")
            m_PlayerClose = false;
    }

    //On trigger function used for seeing if the player is close 
    void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.tag == "Player")
            m_PlayerClose = true;
    }

    //========================================
    //      AUDIO FUNCTIONS 

    //Finds the sound manager and uses it to play this gameobjects sound 
    public void PlaySound(int a_index)
    {
        SoundManager sm = GameObject.FindGameObjectWithTag("SoundManager").GetComponent<SoundManager>();
        if (sm != null)
            sm.PlaySound(gameObject, a_index);
    }

    //Find the sound manager and use it to mute this gameobjects sound
    public void MuteSound()
    {
        SoundManager sm = GameObject.FindGameObjectWithTag("SoundManager").GetComponent<SoundManager>();
        if (sm != null)
            sm.MuteSound(gameObject);
    }
}
