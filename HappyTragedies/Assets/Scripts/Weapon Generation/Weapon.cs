﻿using UnityEngine;
using System.Collections;
using UnityEngine.Serialization;
public class Weapon : MonoBehaviour
{
    [System.Serializable]
    public struct IntVec2
    {
        public int x;
        public int y;
    }
    public enum Rarity
    {
        COMMON = 0,
        UNCOMMON = 1,
        RARE = 2,
        EPIC = 3,
        LEGENDARY = 4,
    }

    public Rarity m_Rarity;
    public int m_LevelUnlock;
    //Real Stats
    [Header("Real Stats")]
    public int m_levelCap;
    public int m_weaponLevel;
    public int m_knockBackForce;
    public float m_damage;
    public float m_attackRate;
    public WEAPON_TYPE m_WeaponType;
    public int m_clipSize;
    public int m_currentClip;
    public double m_reloadTime;

    [HideInInspector]
    public int m_score;
    [HideInInspector]
    public float m_lvlOneDmg = 0;

    //Base Stats
    [Header("Base Stats")]
    public int m_baseLevelCap;
    public int m_baseKnockBackForce;
    public float m_baseDamage;
    public float m_baseAttackRate;
    public int m_baseClipSize;
    public double m_baseReloadTime;


    //Varience Ranges
    [Header("Random Range Limits (X is Min, Y is Max)")]    
    public IntVec2 m_KnockBackVarience;
    [Tooltip("This is multiplied by the base clip size")]
    public IntVec2 m_ClipSizeVarience;
    public Vector2 m_DamageVarience;
    public Vector2 m_AttackRateVarience;
    public Vector2 m_ReloadTimeVarience;

    //TEXTUTES TO BE SWAPPED BASED ON RARITY
    //Put Textures on in Order of Rarity, Most Common first, Least common Last;
    public Texture[] batonTextures;
    public Texture[] swordTextures;
    private Material weaponMat;

    //UIStats Handle
    private UIStats m_UIStats;

    bool dirty = true;
    public WeaponExperience weaponExp
    {
        get;
        set;
    }

    void Awake()
    {
        weaponMat = GetComponent<Renderer>().material;
    }
    
    // Use this for initialization
    void Start()
    {
        weaponExp = GetComponent<WeaponExperience>();
        if(m_lvlOneDmg == 0)
            m_lvlOneDmg = m_damage;
        if (GameObject.FindGameObjectWithTag("GameManager"))
        {
            m_UIStats = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>().GetUIStats();

            if (!m_UIStats)
                dirty = true;
            else
                dirty = false;
        }
        //UpdateUIStats();
    }

    void Update()
    {
        if (GameObject.FindGameObjectWithTag("GameManager"))
        {
            if (dirty)
                m_UIStats = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>().GetUIStats();

            if (!m_UIStats)
                dirty = true;
            else
                dirty = false;
        }
    }

    public void GenerateMe()
    {
        int rarityRan = Random.Range(0, 190);
        if (rarityRan < 190)
        {
            m_Rarity = Rarity.COMMON;
            m_score = 0;
            if (rarityRan < 110)
            {
                m_Rarity = Rarity.UNCOMMON;
                m_score = 1;
                if (rarityRan < 60)
                {
                    m_Rarity = Rarity.RARE;
                    m_score = 2;
                    if(rarityRan < 30)
                    {
                        m_Rarity = Rarity.EPIC;
                        m_score = 3;
                        if(rarityRan < 10)
                        {
                            m_Rarity = Rarity.LEGENDARY;
                            m_score = 4;
                        }
                    }
                }
            }
        }

        m_levelCap = m_baseLevelCap + (int)m_Rarity;
        m_weaponLevel = 1;

        m_knockBackForce = m_baseKnockBackForce + Random.Range(m_KnockBackVarience.x, m_KnockBackVarience.y + 1);
        int dmgVarience = Random.Range((int)m_DamageVarience.x, (int)m_DamageVarience.y + 1);
        float dmgRange = m_DamageVarience.y - m_DamageVarience.x;
        if (dmgVarience < (dmgRange * 0.4f))
            m_score += 0;
        else if (dmgVarience < (dmgRange * 0.8f))
            m_score += 1;
        else
            m_score += 2;
        m_damage = m_baseDamage + dmgVarience;
        m_lvlOneDmg = m_damage;

        if (m_WeaponType != WEAPON_TYPE.WEAPON_PISTOL)
            m_attackRate = m_baseAttackRate + Random.Range(m_AttackRateVarience.x, m_AttackRateVarience.y);
        else
            m_attackRate = m_baseAttackRate = 0;
        if (m_WeaponType >= WEAPON_TYPE.WEAPON_RIFLE)
        {
            m_clipSize = m_baseClipSize + Random.Range(m_ClipSizeVarience.x, m_ClipSizeVarience.y + 1);
            if (m_WeaponType != WEAPON_TYPE.WEAPON_SHOTGUN)
                m_reloadTime = m_baseReloadTime + Random.Range(m_ReloadTimeVarience.x, m_ReloadTimeVarience.y);
            else
                m_reloadTime = m_baseReloadTime;
        }
        else
        {
            m_clipSize = 0;
            m_reloadTime = 0;
        }
        m_currentClip = m_clipSize;

        //todo:  TEXTUTE SWAP SWORD BATON
        if(m_WeaponType == WEAPON_TYPE.WEAPON_MELEE)
        {
            Shader.EnableKeyword("_EMISSION");
            int index = (int)m_Rarity;
            switch (m_Rarity)
            {
                case Rarity.COMMON:
                    {
                        weaponMat.SetTexture("_EmissionMap", batonTextures[index]);
                        break;
                    }
                case Rarity.UNCOMMON:
                    {
                        weaponMat.SetTexture("_EmissionMap", batonTextures[index]);
                        break;
                    }
                case Rarity.RARE:
                    {
                        weaponMat.SetTexture("_EmissionMap", batonTextures[index]);
                        break;
                    }
                case Rarity.EPIC:
                    {
                        weaponMat.SetTexture("_EmissionMap", batonTextures[index]);
                        break;
                    }
                case Rarity.LEGENDARY:
                    {
                        weaponMat.SetTexture("_EmissionMap", batonTextures[index]);
                        break;
                    }
                }
            }

        if (m_WeaponType == WEAPON_TYPE.WEAPON_SWORD)
        {
            int index = (int)m_Rarity;
            switch (m_Rarity)
            {
                case Rarity.COMMON:
                    {
                        weaponMat.SetTexture("_EmissionMap", swordTextures[index]);
                        break;
                    }
                case Rarity.UNCOMMON:
                    {
                        weaponMat.SetTexture("_EmissionMap", swordTextures[index]);
                        break;
                    }
                case Rarity.RARE:
                    {
                        weaponMat.SetTexture("_EmissionMap", swordTextures[index]);
                        break;
                    }
                case Rarity.EPIC:
                    {
                        weaponMat.SetTexture("_EmissionMap", swordTextures[index]);
                        break;
                    }
                case Rarity.LEGENDARY:
                    {
                        weaponMat.SetTexture("_EmissionMap", swordTextures[index]);
                        break;
                    }
            }
        }

    }

    public void UpdateUIStats()
    {
        if (dirty)
        {
            if (GameObject.FindGameObjectWithTag("GameManager"))
                m_UIStats = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>().GetUIStats();
        }
        if (!m_UIStats)
            dirty = true;
        else
            dirty = false;
        if (dirty)
            return;
        if (m_WeaponType == WEAPON_TYPE.WEAPON_SHOTGUN)
        {
            m_UIStats.UpdateDmg(m_damage * 4);
            m_UIStats.UpdateBaseDmg(m_lvlOneDmg * 4);
        }
        else
        {
            m_UIStats.UpdateDmg(m_damage);
            m_UIStats.UpdateBaseDmg(m_lvlOneDmg);
        }
        m_UIStats.UpdateAtkSpd(m_attackRate);
        m_UIStats.UpdateWeaponLevel(m_weaponLevel, m_levelCap);
        m_UIStats.UpdateClipSize(m_clipSize, m_currentClip);
        m_UIStats.UpdateReloadSpeed(m_reloadTime);
        m_UIStats.UpdateWeaponRating(m_score);
    }

}