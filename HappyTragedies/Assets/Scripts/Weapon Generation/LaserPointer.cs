﻿using UnityEngine;
using System.Collections;

public class LaserPointer : MonoBehaviour
{

    LineRenderer m_line;
    Light m_spotLight;

    // Use this for initialization
    void Start()
    {
        m_line = GetComponent<LineRenderer>();
        m_spotLight = GetComponent<Light>();
    }

    // Update is called once per frame
    void Update()
    {
        Ray ray = new Ray(transform.position, transform.forward);
        RaycastHit temp = new RaycastHit();
        if(Physics.Raycast(ray, out temp))
        {
            m_line.SetPosition(1, new Vector3(0, 0, temp.distance));
        }

    }
}
