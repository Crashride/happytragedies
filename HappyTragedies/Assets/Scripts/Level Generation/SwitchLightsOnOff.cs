﻿using UnityEngine;
using System.Collections;

public class SwitchLightsOnOff : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
    public void TurnOffLights(Light[] lights)
    {
        for(int i= 0; i<lights.Length; i++)
        {
            lights[i].intensity = 0;
        }
               
    }
    public void TurnOnLights(Light[] lights)
    {
        for (int i = 0; i < lights.Length; i++)
        { lights[i].intensity = 1; }
    }
}
