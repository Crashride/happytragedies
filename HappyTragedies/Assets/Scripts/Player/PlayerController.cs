﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{

    //===============================
    //Public Vars     (the lack of formatting in these variables is triggering me guys) 

    [Tooltip ("The speed at which the player navigates the world")]
    public float            m_MoveSpeed = 5;                    
    [Tooltip("A reference to the main camera used for veiwing ")]
    public Camera           m_MainCamera;
    [Tooltip("The speed at which the player moves through the world while sprinting")]
    public float            m_sprintSpeed;
    [Tooltip("The total amount of stamina the player has")]
    public float            m_Stamina;
    [Tooltip("The time it takes for stamina to fully recharge")]
    public float            m_StaminaRechargeRate;
    [Tooltip ("The amount of time after being attacked the player is untouchable for")]
    public float            m_UntouchableTime;
    [Tooltip("How much the player slows down while firing weapons")]
    public float            m_speedModifierDenominator;
    [Tooltip("A handle to the Players invincinbility shield thing")]
    public GameObject       m_shield;
    [Tooltip("Time in seconds between the flashing of player when it's hit")]
    public float            m_BlinkTime;
    [Tooltip("A reference to the particle system that indicates when the player is stunned")]
    public ParticleSystem   m_StunnedParticles; 
    //==============================


    //==============================
    //Private Vars 

    //playersWeapon
    Rotate levelParticleCode;
    Collider        m_WeaponCol;                        //A reference to the weapons collider
    GameObject      m_Weapon;                           //A reference to the currently equipped qeapon 
    Weapon          m_WeaponScript;                     //A reference to the weapon script for the currently equpped weapon. 
    Rigidbody       m_RigidBody;                        //A reference to the player's rigidbody component
    Animator        m_PlayerAnim;                       //A reference to the player's animator component
    ParticleSystem[] m_PlayerParticleSystems;           //A list of all particle systems on the player object or childed to the player object
    ParticleSystem  m_HealthParticles;                  //A reference to the particle system that plays when the player regains health
    ParticleSystem  m_SlashParticles;                   //A reference to the particle system that plays when the player attacks
	ParticleSystem 	m_HurtParticles;
    int             m_IsAttacking;                      //Whether or not the player is attacking ( 0 = false 1 = true) 
    RandomizePlayer m_RandomizePlayer;                  //Script used to give the player randomised appearance
    int             m_upgradeCount = 0;                 //the total number of upgrades the player has purchased from shops 
    Image           m_StaminaBar;                       //The Stamina bar in the UI
    double          m_stunCounter;
    double          m_stunPeriod;

    List<MeshRenderer> m_Meshes;                            //An Array of all the mesh renderers for toggling pn and off when the play is hit
    SkinnedMeshRenderer m_meshSkin;

    float           m_Currency;
    float           m_UntouchableCooldown;
    float           realStamina;

    bool            moving;
    bool            m_Untouchable   = false;
    bool            sprinting       = false;
    bool            m_freezTime     = false;
    bool            m_slowWalk      = false;
    bool            m_stunned       = false;
    //==============================

    Vector3 posLastFrame;
    float IdleWaitTime;
    float timeStationary;

    public int m_inputDevice = 0; // 1 = Keyboard Input detected, 2 = Controller Input detected.
    // Use this for initialization
    void Awake()
    {
        posLastFrame = Vector3.zero;
        IdleWaitTime = 0.2f;
        timeStationary = 0;

        m_RandomizePlayer = GetComponentInChildren<RandomizePlayer>();
        foreach (Transform child in this.GetComponentsInChildren<Transform>())
        {
            if (child.tag == ("Weapon") || child.tag == "Gun")
            {
                m_Weapon = child.gameObject;
                m_WeaponCol = child.GetComponent<Collider>();
                break;
            }
        }
        m_StaminaBar = GameObject.FindGameObjectWithTag("StaminaBar").GetComponent<Image>();
        m_WeaponScript = GetComponentInChildren<Weapon>();
        //m_WeaponScript.m_WeaponType = 1;
        m_WeaponScript.UpdateUIStats();
        m_MainCamera = Camera.main;
        realStamina = m_Stamina;
        //m_Meshes
        m_Meshes = new List<MeshRenderer>();
        m_Meshes.AddRange(gameObject.GetComponentsInChildren<MeshRenderer>());
        m_meshSkin = gameObject.GetComponentInChildren<SkinnedMeshRenderer>();
        //m_Meshes += gameObject.GetComponentsInChildren<SkinnedMeshRenderer>();
    }
    void Start()
    {
        levelParticleCode = GetComponentInChildren<Rotate>();
        levelParticleCode.gameObject.SetActive(false);
        moving = false;
        m_PlayerParticleSystems = GetComponentsInChildren<ParticleSystem>();

        //dont destroy the player on scene changes
        DontDestroyOnLoad(gameObject);

        //for the particle systems attached to the player, find the health particle system and the slash particle system
        foreach (ParticleSystem em in m_PlayerParticleSystems)
        {
            if (em.tag == ("HealthParticles"))
            {
                m_HealthParticles = em;
            }
            if (em.tag == "SlashParticles")
            {
                m_SlashParticles = em;
            }
			if (em.tag == "HurtParticles") 
			{
				m_HurtParticles = em;
			}
        }
        //Sshhh
        //reset P.System
        m_HealthParticles.Clear();
        m_HealthParticles.Stop();

        m_RigidBody = GetComponent<Rigidbody>();
        m_MainCamera = Camera.main;
        m_PlayerAnim = GetComponent<Animator>();

        //find the weapon on the player

    }

    void OnLevelWasLoaded()
    {
        m_MainCamera = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera>();
    }

    // Update is called once per frame
    void Update()
    {
        if (m_stunned)
        {
            m_stunCounter += Time.deltaTime;
            if (m_stunCounter >= m_stunPeriod)
            {
                m_StunnedParticles.Stop();
                m_stunCounter = 0.0f;
                m_stunPeriod = 0.0f;
                m_stunned = false;
            }
            return;
        }
        if (FetchKeyDown() != KeyCode.None)
        {
            KeyCode curr = FetchKeyDown();
            if ((int)curr >= 330 && (int)curr < 350)
                m_inputDevice = 2;
            else
                m_inputDevice = 1;
        }
        //Update our invaunerability timer to see if we're invincible or not
        if (m_Untouchable)
        {
            if ((m_UntouchableCooldown += Time.deltaTime) >= m_UntouchableTime)
            {
                m_Untouchable = false;
            }
        }

        if (m_freezTime)
        {
            return;
        }
        //Basic Movement
        if(sprinting == false)
        {
            realStamina += Time.deltaTime * 1 / (m_StaminaRechargeRate + GetComponent<PlayerStats>().GetStaminaRateBuff());
            if (realStamina >= (m_Stamina + GetComponent<PlayerStats>().GetStaminaBuff()))
                realStamina = (m_Stamina + GetComponent<PlayerStats>().GetStaminaBuff());
        }
        if (Input.GetAxisRaw("Sprint") > 0 && m_IsAttacking != 1)
        {
            sprinting = true;
            realStamina -= Time.deltaTime;
            if (realStamina <= 0)
            {
                sprinting = false;
                realStamina = 0;
            }
        }
        else
            sprinting = false;
        m_StaminaBar.fillAmount = realStamina / (m_Stamina + GetComponent<PlayerStats>().GetStaminaBuff());
        float modMoveSpeed;
        if (sprinting)
            modMoveSpeed = m_sprintSpeed;
        else
            modMoveSpeed = m_slowWalk ? m_MoveSpeed / m_speedModifierDenominator : m_MoveSpeed;

        Vector3 velocity = new Vector3(Input.GetAxisRaw("Horizontal"), 0, Input.GetAxisRaw("Vertical"));
        if (velocity.magnitude != 0)
        {
            KeyCode temp = FetchKey();
            if ((int)temp < 330 && temp != KeyCode.None)
                m_inputDevice = 1;
            else
                m_inputDevice = 2;
            moving = true;
            velocity.Normalize();
        }
        else
        {
            moving = false;
        }
        m_RigidBody.MovePosition(m_RigidBody.position + (velocity * modMoveSpeed * Time.deltaTime));

        m_PlayerAnim.SetFloat("WalkSpeed", modMoveSpeed * 0.15f);

        if(m_inputDevice == 1)
        {
            //create a ray and a plane to cast it against
            Ray ray = m_MainCamera.ScreenPointToRay(Input.mousePosition);
            Plane ground = new Plane(Vector3.up, new Vector3(0, m_Weapon.transform.position.y, 0));
            float mouseDistance;
            //1.328097f

            //Do the raycast, if hit, use the returned distance to find the point the mouse is over. 
            if (ground.Raycast(ray, out mouseDistance) && Time.timeScale != 0)
            {
                //Detect mouse moving
                Vector3 point = ray.GetPoint(mouseDistance);
                transform.LookAt(new Vector3(point.x, transform.position.y, point.z));
            }
        }
        else
        {
            Vector2 rightStick = new Vector2(Input.GetAxis("AimX"), Input.GetAxis("AimY"));
            transform.LookAt(new Vector3(transform.position.x + rightStick.x, transform.position.y, transform.position.z + rightStick.y));
        }



        Vector3 forwardVec = transform.forward;
        float directionTravelling = Vector3.Angle(forwardVec, velocity.normalized);

        //if the player isnt in the middle of attacking and is using the melee weapon, start playing the animation
        //if (m_IsAttacking == 0)
        //{
        //    //m_SlashParticles.Stop();
        if (Input.GetAxisRaw("Fire1") > 0)
        {
            m_IsAttacking = 1;
            //        //Debug.Log("Fire! B1");
            //        // Debug.Log(m_WeaponScript.m_WeaponType);
            //        if (m_WeaponScript.m_WeaponType == WEAPON_TYPE.WEAPON_MELEE || m_WeaponScript.m_WeaponType == WEAPON_TYPE.WEAPON_SWORD)
            //        {
            //            //     Debug.Log("Fire! B2");
            //            //m_IsAttacking = 1;
            //            //m_PlayerAnim.SetBool("DoMelee", true);
        }
        //    }
        //}
        if (Input.GetAxisRaw("Fire1") == 0 && m_Weapon.GetComponent<GunAttack>())
            m_IsAttacking = 0;
        if (Input.GetAxisRaw("Fire1") == 0 && m_Weapon.GetComponent<MeleeAttack>() && !m_PlayerAnim.GetBool("DoMelee"))
            m_IsAttacking = 0;
        //set the directions to an easier to understand rotation
        directionTravelling += 45;
        directionTravelling %= 360;

        //if animations stuff up again, it's probably this if statements fault
        if (timeStationary > IdleWaitTime) 
        {
            m_PlayerAnim.SetInteger("State", 0);
        }

        else
        {
            {
                if (0 < directionTravelling && directionTravelling <= 90)
                {
                    //forwards run
                    m_PlayerAnim.SetInteger("State", 1);
                }
                else if (90 < directionTravelling && directionTravelling <= 180)
                {
                    if(Vector3.Dot(forwardVec, velocity.normalized) < 0)
                    {
                        //right run
                        m_PlayerAnim.SetInteger("State", 3);
                    }
                    else if (Vector3.Dot(forwardVec, velocity.normalized) > 0)
                    {
                        //left run
                        m_PlayerAnim.SetInteger("State", 5);
                    }
                }
                else if (180 < directionTravelling && directionTravelling <= 270)
                {
                    //backwards run
                    m_PlayerAnim.SetInteger("State", 4);
                }
                else
                {
                    m_PlayerAnim.SetInteger("State", 1);
                }
            }
        }

        
            if (velocity == Vector3.zero)
        {
            timeStationary += Time.deltaTime;
        }
            else
        {
            timeStationary = 0;
        }
    }

    public void SlowWalkSpeed(bool a_slow)
    {
        m_slowWalk = a_slow;
    }

    public void SetAttackState(int state)
    {
        m_IsAttacking = 0;
        m_PlayerAnim.SetBool("DoMelee", false);
    }

    public bool GetAttackState()
    {
        if (m_IsAttacking == 0)
            return false;
        else if (m_IsAttacking == 1)
            return true;
        else
            return false;
    }

    public void SetAnimationState(int state)
    {
        m_PlayerAnim.SetInteger("State", state);
    }

    public void RecieveStun(double a_duration)
    {
        m_StunnedParticles.Play(); 
        m_stunPeriod = a_duration;
        m_stunned = true; 
    }

    //When Player is doing hurt anim
    //m_audioSources[1].Play(); 

    //The set weapon function is used to assign a weapon on the player
    //inside setWeapon we will need to set which animator controller to use
    public void SetWeapon(GameObject a_weapon)
    {
        Weapon weaponScript = a_weapon.GetComponent<Weapon>();
        if (weaponScript)
        {
            m_Meshes[m_Meshes.IndexOf(m_Weapon.GetComponentInChildren<MeshRenderer>())].enabled = true;
            m_Meshes.Remove(m_Weapon.GetComponentInChildren<MeshRenderer>());


            if (a_weapon.GetComponent<WeaponSwap>())
            {
                a_weapon.GetComponent<WeaponSwap>().enabled = false;
            }
            a_weapon.GetComponent<WeaponExperience>().enabled = true;
            if (weaponScript.m_WeaponType == WEAPON_TYPE.WEAPON_MELEE || weaponScript.m_WeaponType == WEAPON_TYPE.WEAPON_SWORD)
            {
                //this section needs new numbers 
                //need to get designers to find the hard code values
                m_PlayerAnim.SetInteger("WeaponType", 1);
                GameObject weaponHand = GameObject.FindGameObjectWithTag("PlayerRightHand"); //Find the hand to parent the weapons transform to. 
                a_weapon.transform.SetParent(weaponHand.transform);
                Vector3 transformPosition = new Vector3(-0.058f, -0.041f, -0.034f); //Hardcoded local position for the weapon
                Quaternion transformRotation;
                if (weaponScript.m_WeaponType == WEAPON_TYPE.WEAPON_SWORD)
                {
                    transformRotation = Quaternion.Euler(new Vector3(278.239f, -39.682f, -179.682f)); //Hardcoded local rotation for the weapon
                    if (m_WeaponScript.m_WeaponType == WEAPON_TYPE.WEAPON_MELEE)
                        m_SlashParticles.gameObject.transform.localPosition += new Vector3(-0.2f,0, 0.5f);
                }
                else
                {
                    transformRotation = Quaternion.Euler(new Vector3(176.661f, 42.81799f, -183.974f)); //Hardcoded local rotation for the weapon 
                    if (m_WeaponScript.m_WeaponType == WEAPON_TYPE.WEAPON_SWORD)
                        m_SlashParticles.gameObject.transform.localPosition -= new Vector3(-0.2f, 0, 0.5f);
                }
                a_weapon.transform.localPosition = transformPosition;
                a_weapon.transform.localRotation = transformRotation;
                a_weapon.GetComponent<MeleeAttack>().enabled = true;
                //a_weapon.
                RemoveWeapon();
                m_Weapon = a_weapon;
                m_WeaponScript = m_Weapon.GetComponent<Weapon>();
                m_WeaponCol = m_Weapon.GetComponent<BoxCollider>();
            }
            else if (weaponScript.m_WeaponType == WEAPON_TYPE.WEAPON_RIFLE)
            {
                m_PlayerAnim.SetInteger("WeaponType", 3);
                GameObject weaponHand = GameObject.FindGameObjectWithTag("PlayerRightHand");
                a_weapon.transform.SetParent(weaponHand.transform);
                Vector3 transformPosition = new Vector3(-0.2258333f, 0.09711285f, -0.1330295f);
                Quaternion transformRotation = Quaternion.Euler(new Vector3(-1.407f, -129.509f, -3.848f));
                a_weapon.transform.localPosition = transformPosition;
                a_weapon.transform.localRotation = transformRotation;
                a_weapon.GetComponent<GunAttack>().enabled = true;

                RemoveWeapon();
                m_Weapon = a_weapon;
                m_WeaponScript = m_Weapon.GetComponent<Weapon>();
            }
            else if (weaponScript.m_WeaponType == WEAPON_TYPE.WEAPON_PISTOL)
            {
                m_PlayerAnim.SetInteger("WeaponType", 2);
                GameObject weaponHand = GameObject.FindGameObjectWithTag("PlayerRightHand");
                a_weapon.transform.SetParent(weaponHand.transform);
                Vector3 transformPosition = new Vector3(-0.1112f, -0.0585f, -0.0248f);
                Quaternion transformRotation = Quaternion.Euler(new Vector3(-7.661f, -101.403f, -0.979f));
                a_weapon.transform.localPosition = transformPosition;
                a_weapon.transform.localRotation = transformRotation;
                a_weapon.GetComponent<GunAttack>().enabled = true;

                RemoveWeapon();
                m_Weapon = a_weapon;
                m_WeaponScript = m_Weapon.GetComponent<Weapon>();
            }
            else if (weaponScript.m_WeaponType == WEAPON_TYPE.WEAPON_SHOTGUN)
            {
                m_PlayerAnim.SetInteger("WeaponType", 3);
                GameObject weaponHand = GameObject.FindGameObjectWithTag("PlayerRightHand");
                a_weapon.transform.SetParent(weaponHand.transform);
                Vector3 transformPosition = new Vector3(-0.03984392f, -0.03894692f, 0.0007825028f);
                Quaternion transformRotation = Quaternion.Euler(new Vector3(-1.407f, -129.509f, -3.848f));
                a_weapon.transform.localPosition = transformPosition;
                a_weapon.transform.localRotation = transformRotation;
                a_weapon.GetComponent<GunAttack>().enabled = true;

                RemoveWeapon();
                m_Weapon = a_weapon;
                m_WeaponScript = m_Weapon.GetComponent<Weapon>();
            }
            m_WeaponScript.UpdateUIStats();
            m_Meshes.Add(m_Weapon.GetComponentInChildren<MeshRenderer>());

        }
    }

    //Removes the players equiped weapon and gives them the default batton, and returns the inital weapon
    public GameObject RetrieveWeapon()
    {
        GameObject weapon = m_Weapon;
        m_Weapon = null; //we will make this a default batton
        return weapon;
    }

    public int GetUpgradeCount()
    {
        return m_upgradeCount; 
    }

    public int GiveUpgrade()
    {
        m_upgradeCount++;

        int num = Random.Range(0, 4);

        bool choosing = true;
        PlayerStats stats = GetComponent<PlayerStats>(); 
        while (choosing)
        {
            num = num % 4; 
            switch (num)
            {
                case 0:
                    if (!stats.IsStatMaxed(PlayerStats.STATS.SPEEDMULTIPLIER))
                    {
                        stats.UpgradeStat(PlayerStats.STATS.SPEEDMULTIPLIER, 1);
                        choosing = false;
                        return num;
                    }    
                    else
                        num++; 
                    break;
                case 1:
                    if (!stats.IsStatMaxed(PlayerStats.STATS.MAXHEALTHMULTIPLIER))
                    {
                        stats.UpgradeStat(PlayerStats.STATS.MAXHEALTHMULTIPLIER, 1);
                        gameObject.GetComponent<Health>().DoHealing(stats.m_maxHealthPerLvlBoost);
                        GameObject.FindGameObjectWithTag("Stats").GetComponentInChildren<UIStats>().UpdatePlayerHealth(GetComponent<Health>().GetHealth(), GetComponent<Health>().GetMaxHealth());
                        choosing = false;
                        return num;
                    }
                    else num++;
                    break;
                case 2:
                    if (!stats.IsStatMaxed(PlayerStats.STATS.REGENMULTIPLIER))
                    {
                        stats.UpgradeStat(PlayerStats.STATS.REGENMULTIPLIER, 1); 
                        choosing = false;
                        return num;
                    }
                    else num++;
                    break;
                case 3:
                    if (!stats.IsStatMaxed(PlayerStats.STATS.XPMULTIPLIER))
                    {
                        stats.UpgradeStat(PlayerStats.STATS.XPMULTIPLIER, 1); 
                        choosing = false;
                        return num;
                    }
                    else num++;
                    break;
                default:
                    return -1;
            }
        }
        return -1;
    }

    //Destroys the currently equipped weapon
    public void RemoveWeapon()
    {
        Destroy(m_Weapon);
        m_Weapon = null;
    }


    //Returns the players equipped weapon
    public GameObject GetWeapon()
    {
        return m_Weapon;
    }

    public bool GetIsStunned()
    {
        return m_stunned; 
    }

    public void ColliderOn()
    {
        m_SlashParticles.Play();

        m_WeaponCol.enabled = true;
    }

    public void ColliderOff()
    {
        m_SlashParticles.Stop();

        m_WeaponCol.enabled = false;
    }

    //Sound Manager Getter

    public void PlaySound(int a_index)
    {
        SoundManager sm = GameObject.FindGameObjectWithTag("SoundManager").GetComponent<SoundManager>();

        if (a_index == 2 || a_index == 3 || a_index == 13 || a_index == 14)
            sm.PlaySound(gameObject.GetComponentInChildren<MeleeAttack>().gameObject, a_index);
        else if (a_index > 4 && a_index < 9)
            sm.PlaySound(gameObject.GetComponentInChildren<GunAttack>().gameObject, a_index);
        else
            sm.PlaySound(gameObject, a_index);

    }

    public void MuteSound()
    {
        SoundManager sm = GameObject.FindGameObjectWithTag("SoundManager").GetComponent<SoundManager>();
        sm.MuteSound(gameObject);
    }

    public void Randomize()
    {
        //m_RandomizePlayer.RandomiseHair();
    }

    public void PlayHealthParticles()
    {
        PlaySound(11);
        m_HealthParticles.Clear();
        m_HealthParticles.Play();
        GameObject.FindGameObjectWithTag("Stats").GetComponentInChildren<UIStats>().UpdatePlayerHealth(GetComponent<Health>().GetHealth(), GetComponent<Health>().GetMaxHealth());

    }

    //used to give the player money
    public void AddBank(float a_amount)
    {
        m_Currency += a_amount; 
    }

    //used to take the players money, we could do this by just passing a negative number into the first function but come, who's gonna do that. 
    public void TakeBank(float a_amount)
    {
        m_Currency -= a_amount; 
    }

    //returns the total amount of money the player has
    public float GetBank()
    {
        return m_Currency; 
    }
    //used to gain XP
    public void GainExp(int a_experience)
    {
        if (m_Weapon)
            m_WeaponScript.weaponExp.GainEXP((int)(a_experience * GetComponent<PlayerStats>().GetXPBuff()));
    }
    public void LevelUpParticlesPlay()
    {
        levelParticleCode.gameObject.SetActive(false);
        levelParticleCode.gameObject.SetActive(true);
    }
    public void SetTransition()
    {
        m_PlayerAnim.SetInteger("WeaponType", 0);
    }

    public void PauseMovement(bool a_dur)
    {
        if (GameObject.FindGameObjectWithTag("Player") != gameObject)
            GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>().PauseMovement(a_dur);
        else
            m_freezTime = a_dur;
    }

    //Self contained DoDamage function which calls the health component to do damage then activates an invaunerable period. 
    public void DoDamage(float a_dmg)
    {
        if (!m_Untouchable)
        {
            GetComponent<Health>().DoDamage(a_dmg);
            GameObject.FindGameObjectWithTag("Stats").GetComponentInChildren<UIStats>().UpdatePlayerHealth(GetComponent<Health>().GetHealth(), GetComponent<Health>().GetMaxHealth());
            //m_shield.SetActive(true);
            m_Untouchable = true;
            m_UntouchableCooldown = 0.0f;
			m_HurtParticles.Play ();
            StartCoroutine(FlashPlayer());
        }
    }
    //fuck sand

    KeyCode FetchKeyDown()
    {
        int e = 509;
        for (int i = 0; i < e; i++)
        {
            if (Input.GetKeyDown((KeyCode)i))
            {
                return (KeyCode)i;
            }
        }

        return KeyCode.None;
    }

    KeyCode FetchKey()
    {
        int e = 509;
        for (int i = 0; i < e; i++)
        {
            if (Input.GetKey((KeyCode)i))
            {
                return (KeyCode)i;
            }
        }

        return KeyCode.None;
    }

    public void SetReloadOff()
    {
        GetComponent<Animator>().SetBool("Reloading", false);
    }

    IEnumerator FlashPlayer()
    {
        foreach (MeshRenderer mesh in m_Meshes)
        {
            mesh.enabled = false;
            m_meshSkin.enabled = false;
        }
        while (m_Untouchable)
        {
            foreach (MeshRenderer mesh in m_Meshes)
            {
                mesh.enabled = !mesh.enabled;
            }
            m_meshSkin.enabled = !m_meshSkin.enabled;
            yield return new WaitForSecondsRealtime(m_BlinkTime);
        }
        foreach (MeshRenderer mesh in m_Meshes)
        {
            mesh.enabled = true;
            m_meshSkin.enabled = true;
        }
    }

}
