﻿using UnityEngine;
using System.Collections;
/// <summary>
/// AUTHOR: DOUGLAS LANDERS  
/// DATE: 17th July 2016 
/// CURRENT STATE: Extra functionality planned
/// PURPOSE: 
/// This is the universal health class, intended to be possibly to attach to any killable entity in the game. 
/// it includes a range of helper functions to allow external influence on the objects member variables 
/// 
/// TODO :: 
///  -  Add a DoT Effect for healing
/// </summary>
public class Health : MonoBehaviour
{

    //===================================================
    //Public Vars
    [Tooltip ("The maximum health of this entity")]
    public float m_MaxHealth;
    [Tooltip("The maximum number of Damage over time effects that can effect this entity")]
    public int m_MaxDoTs = 10; 
    //===================================================


    //===================================================
    //Private Vars

    //The current health of this entity
    [SerializeField]
    float m_fCurrentHealth;
    //A list of the DoT effects active on this entity 
    DoTEffect[] m_DoTEffects; 
    //===================================================

    //A DoTEffect is a Damage Over Time effect, it has a timer and damage amount, each frame it will be updated to cause some of that damage 
    struct DoTEffect
    {
        public bool active; 
        public float damage;
        public double timer;
    }


    // Initialise the health as full when created
    void Start ()
    {
        m_fCurrentHealth = m_MaxHealth;
        m_DoTEffects = new DoTEffect[m_MaxDoTs]; 
        for (int i = 0; i < m_MaxDoTs; i++)
        {
            m_DoTEffects[i].active = false; 
        }

        
	}


    void Update()
    {
        //go through our active DOT effects 
        if (m_MaxDoTs > 0)
        {
            for (int i = 0; i < m_MaxDoTs; i++)
            {
                if (m_DoTEffects == null)
                {
                    if (m_DoTEffects[i].active)
                    {
                        //see if its finished, if it is, discard it. 
                        if (m_DoTEffects[i].timer <= 0.0f)
                        {
                            m_DoTEffects[i].active = false;
                            break;
                        }
                        else //if its not, do the damage, take off time
                        {
                            DoDamage(Time.deltaTime * m_DoTEffects[i].damage);
                            m_DoTEffects[i].timer -= Time.deltaTime;
                        }
                    }
                }
            }
        }
    }

    //===================================================
    //      Public Helper Functions 


    //Call this function to directly set the entity's health
    public void SetHealth(float a_health)
    {
        m_fCurrentHealth = a_health; 
    }

    //This function will return the current health value 
    public float GetHealth()
    {
        return m_fCurrentHealth; 
    }

    //This function will set the entity's max possible health to the arguements value
    public void SetMaxHealth(float a_MaxHealth)
    {
        m_MaxHealth = a_MaxHealth; 
    }

    //This function will return the entity's maximum possible health
    public float GetMaxHealth()
    {
        if(gameObject.tag == "Player")
        {
            return m_MaxHealth + gameObject.GetComponent<PlayerStats>().GetMaxHealthBuff();
        }
        else
            return m_MaxHealth; 
    }

    //This function will instantly inflict the damage given to health 
    public void DoDamage(float a_damage)
    {
        m_fCurrentHealth -= a_damage;
		if(gameObject.tag == "Enemy")
		{
            gameObject.GetComponentInChildren<Canvas>().enabled = true;
			gameObject.GetComponent<EnemyAI>().PlayOnHitParticles();
            if(m_fCurrentHealth <= 0)
                GetComponentInParent<EnemyAI>().PlaySound(1);
            else
                GetComponentInParent<EnemyAI>().PlaySound(2);
        }
        else if(gameObject.tag == "Boss")
        {
            if (m_fCurrentHealth <= 0)
                GetComponentInParent<BossAI>().PlaySound(1);
            else
                GetComponentInParent<BossAI>().PlaySound(2);
        }
        else if(gameObject.tag == "Player")
        {
            GetComponentInParent<PlayerController>().PlaySound(4);
        }
    }

    //This fumction will inflict the specified amount of damage over the time specified
    public void DoDamage(float a_damage, double a_duration)
    {
        for (int i = 0; i < m_MaxDoTs; i++)
        {
            if (!m_DoTEffects[i].active)
            {
                DoTEffect newDOT = new DoTEffect();
                newDOT.active = true;
                newDOT.damage = a_damage;
                newDOT.timer = a_duration;

                m_DoTEffects[i] = newDOT; 
            }
        }
    }

    //This function will instantly increase the entity's health by the specified amount
    public void DoHealing(float a_healing)
    {
		if (m_fCurrentHealth + a_healing < GetMaxHealth())
		{
			m_fCurrentHealth += a_healing;
		}
		else
		{ m_fCurrentHealth = GetMaxHealth(); }
    }
}
