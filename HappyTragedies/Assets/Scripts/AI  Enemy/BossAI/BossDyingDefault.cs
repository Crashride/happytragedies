﻿using UnityEngine;
using System.Collections;

public class BossDyingDefault : AIBehaviour {

    public override eResult Execute()
    {
        //Start The Death Animation
        GetComponent<BossAI>().SetAnimationState(4);
        //Turn off the navmeshagent 
        GetComponent<NavMeshAgent>().enabled = false;
        GetComponent<CapsuleCollider>().enabled = false; 
        Rigidbody rb = GetComponent<Rigidbody>();
        rb.constraints = RigidbodyConstraints.FreezeAll;
        GetComponent<BossAI>().PlaySound(4);
        return eResult.Success; 
    }

    public override eResult Evaluate()
    {
        return eResult.Success; 
    }

    public void FinishedDying()
    {
        Debug.Log("FinishedDyingCalled"); 
        //TODO: Give player XP 
        GetComponent<Animator>().Stop();
        GetComponent<AlphaSpawnExit>().CreateExit();
        PlayerController pc = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>();
        pc.AddBank(GetComponent<BossAI>().GetKillReward());
        pc.GainExp(GetComponent<BossAI>().GetExpReward());
    }
}
