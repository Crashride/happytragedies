﻿using UnityEngine;
using System.Collections;

public class VomitAttack : AIBehaviour {

    //==========================
    // Private Vars 
    GameObject      m_ProjectilePrefab;
    ParticleSystem  m_PrepParticles; 
    EnemyAI         m_AI;
    GameObject      m_Player;
    float m_PrepTime = 3.0f;
    float m_timeCounter = .0f;
    float m_speedFactor = 0.0f; 
    eResult m_CurrentState = eResult.Fail; 
    //===================


	public override eResult Execute ()
    {
        if (m_CurrentState == eResult.Fail)
        {
            m_CurrentState = eResult.InProgress;
            if (m_PrepParticles != null)
                m_PrepParticles.Play();
            m_AI.SetAnimationState(5);
            transform.LookAt(m_Player.transform.position);
            gameObject.GetComponent<NavMeshAgent>().enabled = false;
        }
        else if (m_CurrentState == eResult.InProgress)
        {
            transform.LookAt(m_Player.transform.position);
        }
        return m_CurrentState;
	}
	

    //Used to determine if an attack is possible 
	public override eResult Evaluate ()
    {
        if (m_Player == null)
            m_Player = GameObject.FindGameObjectWithTag("Player");
        if (m_AI == null)
            m_AI = GetComponent<EnemyAI>();
        if (m_ProjectilePrefab == null)
            m_ProjectilePrefab = m_AI.GetProjectilePrefab();
        if (m_PrepParticles == null)
            m_PrepParticles = m_AI.GetRangedParticles();
        if (m_speedFactor == 0.0f)
            m_speedFactor = m_AI.GetSpeedFactor(); 

        m_CurrentState = eResult.Fail; 
            //figure out if the AI is able to attack
            if (m_ProjectilePrefab != null && m_Player != null) //if we have a player to hit and a bullet to hit with
            {
                RaycastHit rayHit;
                if (Physics.Raycast(transform.position, m_Player.transform.position - transform.position, out rayHit))
                {
                    if (rayHit.collider.tag == "Player" && rayHit.distance <= m_AI.GetAttackRange()) //if we can see the player && they're in range
                    {
                        //we can attack
                        return eResult.Success; 
                    }
                }
            }
        return eResult.Fail;        
	}

    //Will be used once the ranged attack is animated
    public void FinishedRangeAttack()
    {
        GetComponent<NavMeshAgent>().enabled = true;
        m_AI.SetAnimationState(1);
        m_CurrentState = eResult.Success; 
    }

    public void SpawnProjectile()
    {
        Vector3 direction = transform.position - m_Player.transform.position;
        GameObject projectile = Instantiate(m_ProjectilePrefab, transform.position + transform.forward + new Vector3(0, 0.5f, 0), transform.rotation) as GameObject;
        //calculate the time we want the projectile to take
        float distance = Vector3.Distance(m_Player.transform.position, transform.position + transform.forward) / 10.0f;
        projectile.GetComponent<Rigidbody>().AddForce(GetProjectileSpeed(distance * m_speedFactor), ForceMode.VelocityChange);
        projectile.GetComponent<EnemySpitProjectile>().setSource(gameObject);
        if (m_PrepParticles != null)
            m_PrepParticles.Stop();
    }

    //calculates the force to apply to the projectile's rigidbody to make it travel in a parabolic arc
    //Code is sourced from unity user Tomer-Barkan's reply in this thread: http://answers.unity3d.com/questions/248788/calculating-ball-trajectory-in-full-3d-world.html
    private Vector3 GetProjectileSpeed(float time)
    {
        Vector3 TargetRoute = m_Player.transform.position - (transform.position + transform.forward + new Vector3(0, 0.5f, 0));
        Vector3 TargetRouteXY = TargetRoute;
        TargetRouteXY.y = 0;

        float y = TargetRoute.y;
        float xz = TargetRouteXY.magnitude;

        float v0y = y / time + 0.5f * Physics.gravity.magnitude * time;
        float v0xz = xz / time;

        Vector3 ProjectileSpeed = TargetRouteXY.normalized;
        ProjectileSpeed *= v0xz;
        ProjectileSpeed.y = v0y;

        return ProjectileSpeed; 
    }
}
