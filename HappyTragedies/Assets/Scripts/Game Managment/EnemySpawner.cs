﻿using UnityEngine;
using System.Collections;
/// <summary>
/// AUTHOR: MATT CALLAHAN && DOUGLAS LANDERS 
/// DATE: 18th July 2016 (created) | 19th July 2016 (last editted)
/// PURPOSE: 
/// The EnemySpawner is th object that is reponsible for spawning enemies within the bounds of a room. 
/// and then managing the enemies. Managment of enemies includes keeping track of how many have died, and activating/deactivating the enemy controllers
/// </summary>
/// 

public class EnemySpawner : MonoBehaviour
{
    //=======================================
    //Public Vars 
    [Tooltip("Enemy Prefabs here, must be same size as the SpawnChance Array")]
    public GameObject[] m_EnemyTypes;
    [Tooltip("Percentage chance of the relative enemy prefab spawning, try make all values add to 100")]
    public int[] m_spawnChance;
    [Tooltip("Indicates whether or not this is a spawner placed in a boss room")]
    public bool m_IsBossRoom = false; 

    //=======================================


    //=======================================
    //Private Vars
    int                 m_MaxEnemies;           //The maximum number of enemies that this spawner can create 
    float               m_EnemyDamageFactor;    //The amount of damage each enemy will deal 
    float               m_EnemyHealthFactor;    //The factor used to scale enemy health
    float               m_EnemyRewardFactor;    //The factor used to scale enemy kill reward 
    Vector3             m_RoomMinBounds;        //The minimum bounds of the room
    Vector3             m_RoomMaxBounds;        //The maximum bounds of the room 
    Room                m_Room;                 // A reference to the room an instance of the spawner is in
    EnemyAI[]           m_Enemies;              // A list of enemies in the room 
    bool                m_Cleared = false;      //Whether or not the room has been cleaer
    bool                m_EnemiesActive;        //Whether or not the spawner's enemies are activated
    SpawnerManager      m_SpawnMan;             //A reference to the SpawnerManager that created this spawn.
    //=======================================

    // Use this for initialization
    void Awake()
    {
        m_SpawnMan =GameObject.FindWithTag("SpawnerManager").GetComponent<SpawnerManager>();
    }

    // Use the Fixed update method to check if our enemies are dead.
    void FixedUpdate()
    {
        //Only bother checking when our enemies are activated
        if (m_EnemiesActive)
        {
            //Debug.Log("Active");
            int enemiesDead = 0; //The count of enemies that are dead
            //See if any enemies need to be killed.
            for (int i = 0; i < m_Enemies.Length; i++)
            {
                GameObject enemy = m_Enemies[i].gameObject;
                Health enemyHP = enemy.GetComponent<Health>();
				if (enemyHP)
                {
                    //if the enemy's HP is 0 or below, destroy it. 
                    if (enemyHP.GetHealth() <= 0.0f)
                        enemiesDead++;
                }
                //Debug.Log(enemiesDead + " out of " + m_MaxEnemies);
            }

            if (enemiesDead == m_MaxEnemies) //if all enemies are dead
            {
                Door[] doorsInRoom = new Door[4];
                doorsInRoom = transform.parent.GetComponentsInChildren<Door>();
                for(int i = 0; i < 4; i++)
                {
                    if(doorsInRoom[i])
                    {
                        if (doorsInRoom[i].tag != "Sealed")
                            doorsInRoom[i].Unlock();
                    }
                }
                m_Cleared = true;
                if (m_Room)
                    m_Room.Clear();
            }
        }
    }

    //This function will spawn the required number of enemies of various enemy types 
    public void SpawnEnemies()
    {
        if(m_Room && !m_Room.ShouldSpawnEnemies && !m_Room.BossRoom)
        {
            transform.parent.GetComponent<Room>().Clear();
            m_Cleared = true;
            Destroy(gameObject);
        }
        if ((m_Room && m_Room.ShouldSpawnEnemies) || m_IsBossRoom) //only do this if the room should spawn enemies
        {
            m_Enemies = new EnemyAI[m_MaxEnemies];

            //Begin by checking the spawn chances and ensuring they add to 100%
            int total = 0;
            foreach (int i in m_spawnChance)
            {
                total += i;
            }
            if (total > 100)
            {
                for (int i = 0; i < m_spawnChance.GetLength(1); ++i)
                {
                    m_spawnChance[i] *= (100 / total);
                }
            }
            else if (total < 100)
            {
                for (int i = 0; i < m_spawnChance.GetLength(1); ++i)
                {
                    m_spawnChance[i] *= (100 / total);
                }
            }

            //loop through and spawn m_MaxEnemies amount of enemies
            for (int enemyI = 0; enemyI < m_MaxEnemies; enemyI++)
            {
                //Get a random number between 1 and 100 and check which range within spawn chance it fits into.
                int rand = Random.Range(1, 100);
                total = 0;
                for (int i = 0; i < m_spawnChance.Length; ++i)
                {
                    int min = total, max = total + m_spawnChance[i];
                    if (rand >= min && rand < max)
                    {
                        //if we have found the right enemy to spawn Instanciate that enemy variety and parent its transform to this

                        //Create a small variance in its spawn position

                        float varianceX, varianceZ;
                        if (m_IsBossRoom)
                        {
                            varianceX = Random.Range(15 ,-15);
                            varianceZ = Random.Range(15, -15);
                        }
                        else
                        {
                          varianceX = Random.Range(-m_SpawnMan.RangeOfSpawn, m_SpawnMan.RangeOfSpawn);
                          varianceZ = Random.Range(-m_SpawnMan.RangeOfSpawn, m_SpawnMan.RangeOfSpawn);
                        }
                        Vector3 SetPos = new Vector3();
                        SetPos = gameObject.transform.position;
                        SetPos.x += varianceX;
                        SetPos.z += varianceZ;
                        
                        GameObject instance = Instantiate(m_EnemyTypes[i], SetPos, Quaternion.identity) as GameObject;

                        m_Enemies[enemyI] = instance.GetComponent<EnemyAI>(); //add it to our enemy list
                        m_Enemies[enemyI].ScaleDamageFactor(m_EnemyDamageFactor); //adjust its damage
                        m_Enemies[enemyI].ScaleEnemyHealth(m_EnemyHealthFactor);
                        m_Enemies[enemyI].ScaleKillReward(m_EnemyRewardFactor); 
                        if (m_Room != null) m_Enemies[enemyI].SetRoom(m_Room);
                        m_Enemies[enemyI].SetUpBehaviours();
                        m_Enemies[enemyI].GetComponentInChildren<SkinnedMeshRenderer>().enabled = false; 
						instance.transform.SetParent(this.gameObject.transform);
                        break;
                    }
                    else total = max;
                }
            }
            DeactivateEnemies(); //Set the enemies to non-active by default
        }
    }

    //This function will automatically iterate through and destroy each of the enemies managed by this spawner. 
    void PurgeEnemies()
    {
        foreach (EnemyAI enemy in m_Enemies)
        {
            Destroy(enemy.gameObject);
        }
    }

    //This function is used to enable all of the enemy controlller scripts in the enemies 
    public void ActivateEnemies()
    {
        if (m_Enemies != null)
        {
            foreach (EnemyAI enemy in m_Enemies)
            {
                enemy.Activate(); 
            }
            m_EnemiesActive = true;
        }
    }

    //This function is used to disable all of the enemy controller scripts in the enemies 
    public void DeactivateEnemies()
    {
        foreach (EnemyAI enemy in m_Enemies)
        {
            enemy.DeActivate(); 
        }
        m_EnemiesActive = false;

    }

    //Sets the amount of enemies this spawner will create
    public void SetMaxEnemies(int a_amount)
    {
        m_MaxEnemies = a_amount;
    }

    //Returns the amount of enemies this spawner will create
    public int GetMaxEnemies()
    {
        return m_MaxEnemies;
    }

    //Returns whether or not the enemies for this spawner are active
    public bool IsActive()
    {
        return m_EnemiesActive;
    }

    //Returns whether or not the room has been cleared
    public bool IsCleared()
    {
        return m_Cleared;
    }

    //Sets the list of spawn chases for each spawn type in EnemyTypes
    public void SetSpawnChance(int[] a_spawnChance)
    {
        m_spawnChance = a_spawnChance;
    }

    //Sets the list of enemy types that the spawner can create
    public void SetEnemyTypes(GameObject[] a_enemyTypes)
    {
        m_EnemyTypes = a_enemyTypes;
    }


    public void SetRoom(Room a_room)
    {
        m_Room = a_room;
    }
    
    public void SetDamageFactor(float a_damage)
    {
        m_EnemyDamageFactor = a_damage; 
    }

    public void SetHealthFactor(float a_factor)
    {
        m_EnemyHealthFactor = a_factor; 
    }

    public void SetRewardFactor(float a_factor)
    {
        m_EnemyRewardFactor = a_factor; 
    }
}

