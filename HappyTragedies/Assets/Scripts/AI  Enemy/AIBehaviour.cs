﻿using UnityEngine;
using System.Collections;
/// <summary>
/// AUTHOR: DOUGLAS LANDERS
/// DATE: 8/8/16
/// The AIBehaviour script is the parent script for all AI behaviours.
/// </summary>
public class AIBehaviour : MonoBehaviour
{
    public enum eResult
    {
        Success     = 0, 
        Fail        = 1, 
        InProgress  = 2
    }

    void Start()
    {

    }

    void Update()
    {

    }
    public virtual eResult Execute()
    {
        return eResult.Fail; 
    }

    public virtual eResult Evaluate()
    {
        return eResult.Fail; 
    } 

}
