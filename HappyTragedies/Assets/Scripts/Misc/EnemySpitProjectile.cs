﻿using UnityEngine;
using System.Collections;

[RequireComponent (typeof(Rigidbody))]
public class EnemySpitProjectile : MonoBehaviour {

    //Private Vars 
    GameObject m_source; 


	// Use this for initialization
	void Start () {
	}
	
    void OnTriggerEnter(Collider col)
    {
        if (col.tag == "Player")
        {
            col.GetComponent<PlayerController>().DoDamage(m_source.GetComponent<EnemyAI>().m_BaseDamage);
            Destroy(this.gameObject);
        }
        else if (col.tag != "Enemy")
        {
            Destroy(this.gameObject); 
        }
    }

    public void setSource (GameObject a_source)
    {
        m_source = a_source; 
    }
}
