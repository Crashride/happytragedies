﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UIToggle : MonoBehaviour
{

    public GameObject PlayerStatCard; // Assign in inspector ( the player Stat card on the canvas)
    public GameObject WeaponStatCard; //Assign in inspector (the tooltip telling the player how to activate the hud) 
    //public Image TabToggleButton;

    //public GameObject WTSStatCard;
    public GameObject BossHealthBar;

    public Sprite KeyBoardButton;
    public Sprite ControllerButton;
    //public GameObject TabButton;

    public GameObject HackUI;

    int m_inputDevice = 0;
    void Start()
    {
        if(GameObject.FindGameObjectWithTag("Player"))
        {
            m_inputDevice = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>().m_inputDevice;
        }
        PlayerStatCard.SetActive(false);
        WeaponStatCard.SetActive(false);
        //WTSStatCard.SetActive(false);
        //TabToggleButton.enabled = false;
    }

    void Update()
    {
        if (m_inputDevice == 0)
        {
            if (GameObject.FindGameObjectWithTag("Player"))
            {
                m_inputDevice = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>().m_inputDevice;
            }
        }
        else if(m_inputDevice != GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>().m_inputDevice)
            m_inputDevice = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>().m_inputDevice;
        // YOU ARE HERE

        //if (m_inputDevice == 1)
            //TabButton.GetComponent<Image>().sprite = KeyBoardButton;
        //else if (m_inputDevice == 2)
            //TabButton.GetComponent<Image>().sprite = ControllerButton;
        if (Input.GetButtonDown("Stats"))
        {
            //TabToggleButton.enabled = !TabToggleButton.enabled;
            PlayerStatCard.SetActive(!PlayerStatCard.activeSelf);
            WeaponStatCard.SetActive(!WeaponStatCard.activeSelf);
        }
        if (GameObject.FindGameObjectWithTag("Boss") && !BossHealthBar.activeInHierarchy)
            BossHealthBar.SetActive(true);
        else if(!GameObject.FindGameObjectWithTag("Boss"))
            BossHealthBar.SetActive(false);

    }

    public GameObject GetWTSCard()
    {
        return null;
        //return WTSStatCard;
    }

    public GameObject GetStatsCard()
    {
        return PlayerStatCard;
    }
}
