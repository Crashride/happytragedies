﻿using UnityEngine;
using System.Collections;
public enum WEAPON_TYPE
{
    WEAPON_MELEE,
    WEAPON_RIFLE,
    WEAPON_PISTOL,
    WEAPON_SHOTGUN,
    WEAPON_SWORD,
    DEFAULT = 9999,
}
public class WeaponGenerator : MonoBehaviour
{
    GameManager m_gm;

    public GameObject m_meleePrefab;
    public GameObject m_riflePrefab;
    public GameObject m_pistolPrefab;
    public GameObject m_shotgunPrefab;
    public GameObject m_swordPrefab;
    public GameObject m_DropWeaponParticlesCommon;      //A reference to the particle system that indicates when a weapon stops. One for each rarity
    public GameObject m_DropWeaponParticlesUncommon;
    public GameObject m_DropWeaponParticlesRare;
    public GameObject m_DropWeaponParticlesEpic;
    public GameObject m_DropWeaponParticlesLegendary;


    public int m_batonChance;
    public int m_pistolChance;
    public int m_rifleChance;
    public int m_swordChance;
    public int m_shotgunChance;

    public int m_dropDecreaseFactor;
    int currLevel;

    WEAPON_TYPE m_lastGen;
    void Awake()
    {
        m_lastGen = WEAPON_TYPE.DEFAULT;
    }
    // Use this for initialization
    void Start()
    {
        m_gm = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>();
        currLevel = m_gm.m_currentLevel + 26;
    }

    // Update is called once per frame
    void Update()
    {
        currLevel = m_gm.m_currentLevel + 26;
    }

    public GameObject GenerateWeapon(int a_spanwChance)
    {
        int rand = Random.Range(0, 100);
        if (rand <= a_spanwChance)
        {
            switch(m_lastGen)
            {
                case WEAPON_TYPE.WEAPON_MELEE:
                    m_batonChance /= m_dropDecreaseFactor;
                    break;
                case WEAPON_TYPE.WEAPON_PISTOL:
                    m_pistolChance /= m_dropDecreaseFactor;
                    break;
                case WEAPON_TYPE.WEAPON_RIFLE:
                    m_rifleChance /= m_dropDecreaseFactor;
                    break;
                case WEAPON_TYPE.WEAPON_SWORD:
                    m_swordChance /= m_dropDecreaseFactor;
                    break;
                case WEAPON_TYPE.WEAPON_SHOTGUN:
                    m_shotgunChance /= m_dropDecreaseFactor;
                    break;
                default:
                    break;
            }
                
                

            int tempRc = (currLevel >= m_riflePrefab.GetComponent<Weapon>().m_LevelUnlock) ? m_rifleChance : 0;
            int tempSwC = (currLevel >= m_swordPrefab.GetComponent<Weapon>().m_LevelUnlock) ? m_swordChance : 0;
            int tempShC = (currLevel >= m_shotgunPrefab.GetComponent<Weapon>().m_LevelUnlock) ? m_shotgunChance : 0;
            float total = m_batonChance
                        + m_pistolChance
                        + tempRc
                        + tempSwC
                        + tempShC;

            GameObject generatedWep;
            int wepNum = -1;
            float ranC = Random.Range(0.0f, 1.0f);
            if (ranC < (m_batonChance / total))
                wepNum = 0;
            else if (ranC <= (m_batonChance / total) + (m_pistolChance / total))
                wepNum = 1;
            else if (ranC <= (m_batonChance / total) + (m_pistolChance / total) + (tempRc / total))
                wepNum = 2;
            else if (ranC <= (m_batonChance / total) + (m_pistolChance / total) + (tempRc / total) + (tempSwC / total))
                wepNum = 3;
            else if (ranC <= (m_batonChance / total) + (m_pistolChance / total) + (tempRc / total) + (tempSwC / total) + (tempShC / total))
                wepNum = 4;

            switch (m_lastGen)
            {
                case WEAPON_TYPE.WEAPON_MELEE:
                    m_batonChance *= m_dropDecreaseFactor;
                    break;
                case WEAPON_TYPE.WEAPON_PISTOL:
                    m_pistolChance *= m_dropDecreaseFactor;
                    break;
                case WEAPON_TYPE.WEAPON_RIFLE:
                    m_rifleChance *= m_dropDecreaseFactor;
                    break;
                case WEAPON_TYPE.WEAPON_SWORD:
                    m_swordChance *= m_dropDecreaseFactor;
                    break;
                case WEAPON_TYPE.WEAPON_SHOTGUN:
                    m_shotgunChance *= m_dropDecreaseFactor;
                    break;
                default:
                    break;
            }

            switch (wepNum)
            {
                case 0:
                    generatedWep = GenerateMelee();
                    generatedWep.GetComponent<Weapon>().GenerateMe();
                    m_lastGen = WEAPON_TYPE.WEAPON_MELEE;
                    break;
                case 1:
                    generatedWep = GeneratePistol();
                    generatedWep.GetComponent<Weapon>().GenerateMe();
                    m_lastGen = WEAPON_TYPE.WEAPON_PISTOL;
                    break;
                case 2:
                    generatedWep = GenerateRifle();
                    generatedWep.GetComponent<Weapon>().GenerateMe();
                    m_lastGen = WEAPON_TYPE.WEAPON_RIFLE;
                    break;
                case 3:
                    generatedWep = GenerateSword();
                    generatedWep.GetComponent<Weapon>().GenerateMe();
                    m_lastGen = WEAPON_TYPE.WEAPON_SWORD;
                    break;
                case 4:
                    generatedWep = GenerateShotgun();
                    generatedWep.GetComponent<Weapon>().GenerateMe();
                    m_lastGen = WEAPON_TYPE.WEAPON_SHOTGUN;
                    break;
                default:
                    generatedWep = new GameObject();
                    m_lastGen = WEAPON_TYPE.DEFAULT;
                    break;
            }
            return generatedWep;
        }
        else
            return null;
    }

    public GameObject GenerateWeapon(WEAPON_TYPE a_type, int a_spanwChance)
    {
        int rand = Random.Range(0, 100);
        if (rand <= a_spanwChance)
        {
            GameObject generatedWep;
            switch (a_type)
            {
                case WEAPON_TYPE.WEAPON_MELEE:
                    generatedWep = GenerateMelee();
                    generatedWep.GetComponent<Weapon>().GenerateMe();
                    break;
                case WEAPON_TYPE.WEAPON_RIFLE:
                    if (currLevel >= m_riflePrefab.GetComponent<Weapon>().m_LevelUnlock)
                    {
                        generatedWep = GenerateRifle();
                        generatedWep.GetComponent<Weapon>().GenerateMe();
                        break;
                    }
                    goto case WEAPON_TYPE.WEAPON_PISTOL;
                case WEAPON_TYPE.WEAPON_PISTOL:
                    if (currLevel >= m_pistolPrefab.GetComponent<Weapon>().m_LevelUnlock)
                    {
                        generatedWep = GeneratePistol();
                        generatedWep.GetComponent<Weapon>().GenerateMe();
                        break;
                    }
                    goto case WEAPON_TYPE.WEAPON_MELEE;
                case WEAPON_TYPE.WEAPON_SHOTGUN:
                    if (currLevel >= m_shotgunPrefab.GetComponent<Weapon>().m_LevelUnlock)
                    {
                        generatedWep = GenerateShotgun();
                        generatedWep.GetComponent<Weapon>().GenerateMe();
                        break;
                    }
                    goto case WEAPON_TYPE.WEAPON_RIFLE;
                case WEAPON_TYPE.WEAPON_SWORD:
                    if (currLevel >= m_swordPrefab.GetComponent<Weapon>().m_LevelUnlock)
                    {
                        generatedWep = GenerateSword();
                        generatedWep.GetComponent<Weapon>().GenerateMe();
                        break;
                    }
                    goto case WEAPON_TYPE.WEAPON_MELEE;
                default:
                    generatedWep = new GameObject();
                    break;
            }
            return generatedWep;
        }
        else
            return null;
    }

    public GameObject GenerateWeaponDef(WEAPON_TYPE a_type, int a_spanwChance)
    {
        int rand = Random.Range(0, 100);
        if (rand <= a_spanwChance)
        {
            GameObject generatedWep;
            switch (a_type)
            {
                case WEAPON_TYPE.WEAPON_MELEE:
                    generatedWep = GenerateMelee();
                    generatedWep.GetComponent<Weapon>().GenerateMe();
                    break;
                case WEAPON_TYPE.WEAPON_RIFLE:
                    generatedWep = GenerateRifle();
                    generatedWep.GetComponent<Weapon>().GenerateMe();
                    break;
                case WEAPON_TYPE.WEAPON_PISTOL:
                    generatedWep = GeneratePistol();
                    generatedWep.GetComponent<Weapon>().GenerateMe();
                    break;
                case WEAPON_TYPE.WEAPON_SHOTGUN:
                    generatedWep = GenerateShotgun();
                    generatedWep.GetComponent<Weapon>().GenerateMe();
                    break;
                case WEAPON_TYPE.WEAPON_SWORD:
                    generatedWep = GenerateSword();
                    generatedWep.GetComponent<Weapon>().GenerateMe();
                    break;
                default:
                    generatedWep = new GameObject();
                    break;
            }
            return generatedWep;
        }
        else
            return null;
    }

    GameObject GenerateRifle()
    {
        return Instantiate<GameObject>(m_riflePrefab);
    }
    GameObject GeneratePistol()
    {
        return Instantiate<GameObject>(m_pistolPrefab);
    }
    GameObject GenerateShotgun()
    {
        return Instantiate<GameObject>(m_shotgunPrefab);
    }
    GameObject GenerateMelee()
    {
        return Instantiate<GameObject>(m_meleePrefab);
    }
    GameObject GenerateSword()
    {
        return Instantiate<GameObject>(m_swordPrefab);
    }

    public GameObject GetWeaponDropParticles(Weapon.Rarity a_rarity)
    {
        switch(a_rarity)
        {
            case Weapon.Rarity.COMMON:
                return m_DropWeaponParticlesCommon;
            case Weapon.Rarity.UNCOMMON:
                return m_DropWeaponParticlesUncommon;
            case Weapon.Rarity.RARE:
                return m_DropWeaponParticlesRare;
            case Weapon.Rarity.EPIC:
                return m_DropWeaponParticlesEpic;
            case Weapon.Rarity.LEGENDARY:
                return m_DropWeaponParticlesLegendary;
            default:
                return m_DropWeaponParticlesCommon; 
        }

    }
}
