﻿using UnityEngine;
using System.Collections;

public class ShopInteraction : MonoBehaviour
{

    //=======================
    //Public Vars 
    [Tooltip("The base cost for an upgrade (will be scaled)")]
    public int m_BaseCost = 20;
    [Tooltip("A scalar amount to increase cost for each previously obtained upgrade")]
    public float m_IncreaseFactor = 1.5f;
    [Tooltip("A reference to the instruction canvas ")]
    public Canvas m_Canvas;
    [Tooltip("Array of the different displays for each upgrade type")]
    public GameObject[] m_UpgradeDiplay;
    //=======================

    //=======================
    //Private Vars 
    bool m_Used = false;
    bool m_IsPlayerClose = false;
    //int m_CurrentLevel;
    int m_UpgradeCount;
    [SerializeField]
    int m_CurrentCost;
    PlayerController m_Player;
    InteractionShopUI interactionUI;
    //=======================


    // Use this for initialization
    void Start()
    {
        //m_CurrentLevel = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>().m_currentLevel + 25;
        m_UpgradeCount = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>().GetUpgradeCount();
        interactionUI = GetComponentInChildren<InteractionShopUI>();

        //calculate the cost for this upgrade 
       float temp = (Mathf.Pow(m_IncreaseFactor, m_UpgradeCount) * m_BaseCost);
        m_CurrentCost = (int)temp;

        //Get a reference to the player 
        m_Player = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>();
    }

    // Update is called once per frame
    void Update()
    {
        if (!m_Used && m_IsPlayerClose && Input.GetButtonDown("Swap") && m_Player.GetBank() - m_CurrentCost >= 0)
        {
            
            m_Player.TakeBank(m_CurrentCost);
            m_UpgradeDiplay[m_Player.GiveUpgrade()].SetActive(true);            
            m_Used = true;
            interactionUI.gameObject.SetActive(false);
        }
    }


    //Trigger Functions used for determining when the player is close enough to use the shop 
    void OnTriggerEnter(Collider col)
    {
        if (col.tag == "Player")
        {
            m_IsPlayerClose = true;
            if (m_Canvas != null)
                m_Canvas.enabled = true;
        }
    }

    void OnTriggerExit(Collider col)
    {
        if (col.tag == "Player")
        {
            m_IsPlayerClose = false;
            if (m_Canvas != null)
                m_Canvas.enabled = false;
        }
    }

    public int GetUpgradeCost()
    {
        return m_CurrentCost;
    }
}
