﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class InteractionShopUI : MonoBehaviour {

    // Use this for initialization
    int shopCost;
    Text gameText;
    ShopInteraction shop;
	void Start () {
        shop = GetComponentInParent<ShopInteraction>();
        shopCost = shop.GetUpgradeCost();
        gameText = GetComponent<Text>();
        gameText.text = shopCost.ToString() + " \nTo Upgrade";
    }
	
	// Update is called once per frame
	void Update () {
	
	}
}
