﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class LoadLevel : MonoBehaviour
{
    PlayerController player;
    float startTime;
    float journeyLength;
    Vector3 startMarker;
    Vector3 endMarker;
    public float PipeHeight;
    public float TravelSpeed;
    public float AccelPerSecond;
    bool CoroutineFlag = false; 
    // Use this for initialization
    void Start()
    {
        startMarker = new Vector3(gameObject.transform.position.x, transform.position.y, gameObject.transform.position.z);
        endMarker = new Vector3(gameObject.transform.position.x, PipeHeight, gameObject.transform.position.z);
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>();
        journeyLength = Vector3.Distance(startMarker, endMarker);

    }

    void OnTriggerEnter(Collider col)
    {
        if (col.tag == "Player")
        {
            if (!CoroutineFlag) 
                StartCoroutine(RunPipeAnim());
        }
    }

    IEnumerator RunPipeAnim()
    {
        CoroutineFlag = true;
        if(NiceSceneTransition.instance)
            NiceSceneTransition.instance.LoadScene("SceneBuilding");
        GameManager gm = GameObject.FindObjectOfType(typeof(GameManager)) as GameManager;
        gm.PlaySuccSound();
        player.PauseMovement(true);
        player.SetAnimationState(0);
        startTime = Time.time;
        if(GameObject.FindGameObjectWithTag("Roof"))
        {
            GameObject.FindGameObjectWithTag("Roof").SetActive(false);
        }
        if (NiceSceneTransition.instance)
        {
            while (!NiceSceneTransition.instance.endDone)
            {
                float distCovered = (Time.time - startTime) * TravelSpeed;
                float fracJourney = distCovered / journeyLength;
                transform.position = Vector3.Lerp(startMarker, endMarker, fracJourney);
                TravelSpeed += Time.deltaTime * AccelPerSecond;
                yield return new WaitForEndOfFrame();
            }
        }
        player.PauseMovement(false);
        gm.NextLevel();
        CoroutineFlag = false; 
        if (!NiceSceneTransition.instance)
            SceneManager.LoadScene("SceneBuilding");
    }


}
