﻿using UnityEngine;
using System.Collections;

public class SeekLastKnownPosition : AIBehaviour
{
    //=============
    //Private Vars 
    EnemyAI m_AI;
    NavMeshAgent m_Agent; 
    

    public override eResult Execute()
    {
        //Get any references we need
        if (m_AI == null)
            m_AI = GetComponent<EnemyAI>();
        if (m_Agent == null)
            m_Agent = GetComponent<NavMeshAgent>();

        //Get the Last Known Position (lkp) and set it as the destination
        Vector3 lkp = m_AI.GetLastKnownPlayerPosition();
        if (lkp != Vector3.zero) //if the LKP is a valid position seek to it and return success
        {
            m_Agent.SetDestination(lkp);
            m_AI.SetActionState(1); 
            m_AI.SetAnimationState(1); 
            return eResult.Success;
        }
        else return eResult.Fail;
        }

    public override eResult Evaluate()
    {
        //success by default, this behaviour has no requirements
        return eResult.Success; 
    }

}
