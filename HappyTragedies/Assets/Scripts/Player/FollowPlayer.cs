﻿using UnityEngine;
using System.Collections;

public class FollowPlayer : MonoBehaviour
{

    private Vector3 m_PlayerPos;
    Vector3 realPos;
    bool m_shaking = false;
    public float m_ShakeTime;
    float m_shakeDuration;
    [Range(0, 1.0f)]
    public float m_ShakeIntensity;
    // Use this for initialization
    void Start()
    {
        m_shakeDuration = 0;
        Camera.main.transform.position = GameObject.FindGameObjectWithTag("Player").transform.position + new Vector3(0, 10, -5);
        m_PlayerPos = GameObject.FindGameObjectWithTag("Player").transform.position;
        Vector3 diff = GameObject.FindGameObjectWithTag("Player").transform.position - m_PlayerPos;
        Camera.main.transform.position += diff;
        Camera.main.transform.LookAt(GameObject.FindGameObjectWithTag("Player").transform);
        realPos = Camera.main.transform.position;
        m_PlayerPos = GameObject.FindGameObjectWithTag("Player").transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        if (m_PlayerPos != GameObject.FindGameObjectWithTag("Player").transform.position)
        {
            Vector3 diff = GameObject.FindGameObjectWithTag("Player").transform.position - m_PlayerPos;
            Camera.main.transform.position += diff;
            realPos += diff;
            Camera.main.transform.LookAt(GameObject.FindGameObjectWithTag("Player").transform);
            m_PlayerPos = GameObject.FindGameObjectWithTag("Player").transform.position;
        }
        if (m_shaking)
        {
            Vector3 shakeOffset = Random.insideUnitSphere * m_ShakeIntensity;
            Camera.main.transform.LookAt(GameObject.FindGameObjectWithTag("Player").transform);

            Camera.main.transform.position = realPos + shakeOffset;

            m_shakeDuration += Time.deltaTime;
            if (m_shakeDuration >= m_ShakeTime)
                StopShake();
        }

        //if (Input.GetKeyDown(KeyCode.Space))
        //{
        //    if (m_shaking)
        //        StopShake();
        //    else
        //        Shake();
        //}

    }

    public void Shake()
    {
        m_shaking = true;
        realPos = Camera.main.transform.position;
    }

    public void Shake(float a_dur, float a_intens)
    {
        m_shakeDuration = a_dur;
        m_ShakeIntensity = a_intens;
        m_shaking = true;
        realPos = Camera.main.transform.position;
    }

    public void StopShake()
    {
        m_shaking = false;
        Camera.main.transform.position = realPos;
        Camera.main.transform.LookAt(GameObject.FindGameObjectWithTag("Player").transform);
        m_shakeDuration = 0;
    }
    
    public bool Shaking() { return m_shaking; }
}
