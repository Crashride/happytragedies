﻿using UnityEngine;
using System.Collections;

public class BossRangedAttack1 : AIBehaviour
{

    //Private VArs 
    BossAI          m_AI;
    NavMeshAgent    m_Agent;
    Rigidbody       m_RigidBody;
    Health          m_PlayerHealth;
    eResult         m_CurrentState;

    public override eResult Execute()
    {
        if (m_CurrentState == eResult.Fail)
        {
            m_Agent.enabled = false;
            m_AI.SetAnimationState(2);
            m_RigidBody.constraints = RigidbodyConstraints.FreezeAll;
            m_CurrentState = eResult.InProgress; 
        }
        return m_CurrentState;
    }

    public override eResult Evaluate()
    {
        //Get references 
        if (m_AI == null)
            m_AI = GetComponent<BossAI>();
        if (m_Agent == null)
            m_Agent = GetComponent<NavMeshAgent>();
        if (m_RigidBody == null)
            m_RigidBody = GetComponent<Rigidbody>();
        if (m_PlayerHealth == null)
            m_PlayerHealth = GameObject.FindGameObjectWithTag("Player").GetComponent<Health>();

        //default our curremt state
        m_CurrentState = eResult.Fail;

        //If the player is close we can attack (return Success) otherwise return Fail. 
        if (m_AI.IsPlayerClose("Ranged"))
        {
            return eResult.Success;
        }
        else return eResult.Fail;
    }

    public void StartParticles()
    {
        bool dougmode = true;
        while (dougmode)
        {
            dougmode = true;
        }
    }

    public void DealDamageRanged1()
    {
        if (m_AI.IsPlayerClose("Ranged") && m_PlayerHealth != null)
        {
            m_PlayerHealth.DoDamage(m_AI.GetHeavyDamage(1));
        }
    }
}
