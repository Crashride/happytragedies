﻿using UnityEngine;
using System.Collections;

public class MeleeAttackPrepare : AIBehaviour {


    //===============
    //Private Vars 
    GameObject m_Player;    //A reference to the players gameobject
    NavMeshAgent m_Agent;   //A reference to the AI's navmeshagent
    EnemyAI m_AI;           //A reference to the AI's EnemyAI component
    //===============


    public override eResult Execute()
    {
        //if we have valid references execute our stuff
        if (m_Player && m_Agent && m_AI)
        {
            //Check to see if we're far away from the player and need to seek towards them, or if we just need to rotate to get them in the attack zone 
            //Start by getting the distance between the player and the ai 
            float dist = Vector3.Distance(transform.position, m_Player.transform.position);

            if (dist <= m_Agent.stoppingDistance) //if the player is close we just need to rotate
            {
                Quaternion targetRot = Quaternion.LookRotation(m_Player.transform.position - transform.position);
                transform.rotation = Quaternion.Lerp(transform.rotation, targetRot, 5 * Time.deltaTime); 
            }
            else
            {
                m_Agent.SetDestination(m_Player.transform.position); 
            }
            m_AI.SetActionState(1);
            m_AI.SetAnimationState(1); //used to set the animation for the AI to walking
            return eResult.Success;
        }
        else
            return eResult.Fail;
    }

    public override eResult Evaluate()
    {
        if (m_AI == null)
            m_AI = GetComponent<EnemyAI>();
        if (m_Player == null)
            m_Player = GameObject.FindGameObjectWithTag("Player");
        if (m_Agent == null)
            m_Agent = GetComponent<NavMeshAgent>();

        //only return success if the AI caqn see the player 
        if (m_AI.CanSeePlayer())
            return eResult.Success;
        else return eResult.Fail;
    }

}
