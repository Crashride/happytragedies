﻿using UnityEngine;
using System.Collections;

public class GruntSpawnBehaviour : AIBehaviour {

    //===============
    //Vars 
    GameObject      m_Spawner;      //A reference to the prefab for the mounds the enemies appear from
    CapsuleCollider m_Collider;     //A reference to the objects collider, used to prevent the enemy from being attacked while spawning. 
    EnemyAI         m_AI;           //A reference to the class containing the AI logic and AI public variables. 
    NavMeshAgent    m_Agent;        //A reference to the objects NAvMeshAgent, used to prevent the AI from moving while spawning. 
    eResult m_CurrentState;         //Used to show the current state of the behaviour
    //===============

    //This function is used for finding the references we need to perform the behaviour, and informs the AI whether or not it can execute this behaviour. 
    public override eResult Evaluate()
    {
        m_CurrentState = eResult.Fail; //Set the state to fail by default (this shows the behaviour hasn't been started.
        //Initialise any un-aquired references / variables. 
        if (m_AI == null)
            m_AI = GetComponent<EnemyAI>();
        if (m_Spawner == null)
            m_Spawner = m_AI.m_SpawnPointPrefab; 
        if (m_Agent == null)
            m_Agent = GetComponent<NavMeshAgent>();
        if (m_Collider == null)
            m_Collider = GetComponent<CapsuleCollider>(); 

        //if we successfully obtained our references then we can start the behaviour 
        if (m_Agent && m_AI)
            return eResult.Success;
        else return eResult.Fail; 
    }

    //This function is used for executing the behaviour, is used for disabling un-needed compoenents and triggering animations etc. 
    public override eResult Execute()
    {
        if (m_CurrentState == eResult.Fail) //if the behaviour hasn't been started. 
        { 
            m_Agent.enabled     = false; //disable the navmeshagent so it doesn't reset our AI's position
            m_Collider.enabled  = false; //disable the capsule collider so it cannot take damage while spawning. 

            Instantiate(m_Spawner, transform.position, Quaternion.identity); //Instantiate a mount at this enemies pos
            GetComponentInChildren<SkinnedMeshRenderer>().enabled = true; //Turn on the enemies renderer
            
            m_AI.SetAnimationState(0);  //Set the enemies animation to idle, so it can transition to spawn.
            m_AI.SetAnimationState(10); //Set the enemies animation to spawn.
            
            m_CurrentState = eResult.InProgress; //change our state and return 
            return m_CurrentState;
        }
        return m_CurrentState; 
    }
    
    //Function called at the end of the animation to make the behaviour complete. 
    public void FinishedSpawn()
    {
        if (m_Agent && m_AI)
        {
            //when the spawn is finished, re-enable the AIs components and set the state to finished. 
            m_Agent.enabled     = true; 
            m_Collider.enabled  = true; 
            m_AI.m_IsActive     = true;
            m_CurrentState      = eResult.Success;
        }
    }
}
