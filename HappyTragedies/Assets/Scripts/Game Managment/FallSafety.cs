﻿using UnityEngine;
using System.Collections;

public class FallSafety : MonoBehaviour {

    // Use this for initialization
    public Vector3 startPos;
	void Start () {

	}
	void OnLevelWasLoaded()
    {
        startPos = transform.position;
    }
	// Update is called once per frame
	void Update () {

        if (transform.localPosition.y < 0)
        {
            transform.position = startPos;
        }
	}
}
