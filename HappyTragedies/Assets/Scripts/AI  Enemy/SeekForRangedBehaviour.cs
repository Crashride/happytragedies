﻿using UnityEngine;
using System.Collections;

public class SeekForRangedBehaviour : AIBehaviour {

    GameObject m_Player;
    NavMeshAgent m_Agent; 
    float m_MaxRange = 0f;
    eResult m_CurrentState; 


    public override eResult Execute()
    {
        if (m_CurrentState == eResult.Fail)
        {
            m_CurrentState = eResult.InProgress;
            m_Agent.SetDestination(m_Player.transform.position);

            return m_CurrentState; 
        }
        else
        {
            //check to see if we can see the player yet
            RaycastHit rayHit;
            if (Physics.Raycast(transform.position, Vector3.Normalize(m_Player.transform.position - transform.position), out rayHit, m_MaxRange))
            {
                if (rayHit.collider.tag == "Player")
                    return eResult.Success; 
            }
            return m_CurrentState; 
        }
        
    }

    public override eResult Evaluate()
    {
        m_CurrentState = eResult.Fail; //default our current range.
        //find our variables if we haven't already
        if (m_Player == null)
            m_Player = GameObject.FindGameObjectWithTag("Player");
        if (m_Agent == null)
            m_Agent = GetComponent<NavMeshAgent>();
        if (m_MaxRange == 0)
            m_MaxRange = GetComponent<EnemyAI>().GetAttackRange(); 

        //perform a raycast, if don't see the player we retrun success so we can position the AI better, otherwuse we don't need to get into position
        RaycastHit rayHit; 
        if (Physics.Raycast(transform.position, Vector3.Normalize(m_Player.transform.position - transform.position), out rayHit, m_MaxRange) && GetComponent<EnemyAI>().CanSeePlayer())
        {
            if (rayHit.collider.tag != "Player")
            {
                return eResult.Success;
            }
        }
        return eResult.Fail;


    }
}
