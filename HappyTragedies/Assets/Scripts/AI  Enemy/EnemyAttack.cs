﻿using UnityEngine;
using System.Collections;

public class EnemyAttack : MonoBehaviour
{


    public float m_AttackRadius;
    public float m_AttackDamage;
    public float m_attackCooldown;
    float m_maxCooldown;
    bool attackOnCooldown = false;


    private Animator anim;
    // Use this for initialization
    void Start()
    {
        anim = gameObject.GetComponent<Animator>();
        m_maxCooldown = m_attackCooldown;
    }

    // Update is called once per frame
    void Update()
    {
        if (!attackOnCooldown)
        {
            Ray tRay = new Ray(GetComponent<Transform>().position, (GameObject.FindGameObjectWithTag("Player").transform.position - GetComponent<Transform>().position));
            RaycastHit data;
            if (Physics.Raycast(tRay, out data))
            {
                if (data.collider.tag == "Player" && data.distance <= m_AttackRadius)
                {
                    //anim.SetInteger("State", 2);
                    GetComponent<Rigidbody>().freezeRotation = true;
                    GetComponent<Rigidbody>().AddExplosionForce(20, data.normal + gameObject.transform.position, 2, 0, ForceMode.Impulse);
                    attackOnCooldown = true;
                }
            }
        }
        if(attackOnCooldown)
        {
            m_attackCooldown -= Time.deltaTime;
            if(m_attackCooldown <= 0)
            {
                attackOnCooldown = false;
                m_attackCooldown = m_maxCooldown;
                GetComponent<Rigidbody>().freezeRotation = false;
            }
        }
    }
}
