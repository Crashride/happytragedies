﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CurrencyCounter : MonoBehaviour {
    private Text textField;
    private PlayerController player;
	// Use this for initialization
	void Start () {
        textField = gameObject.GetComponent<Text>();
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>();
	}
	
	// Update is called once per frame
	void Update () {
        textField.text = player.GetBank().ToString("F0");
	}
}
