﻿using UnityEngine;
using System.Collections;

public class ContactAttack : AIBehaviour {

    //===================
    //Vars 
    EnemyAI          m_AI;           //Reference to the AI component
    PlayerController m_Player;       //Reference to the player gameobject
    NavMeshAgent     m_Agent;        //Reference to the AIs NavMeshAgent
    Rigidbody        m_rigidbody;    //Reference to the AIs rigidbody

    eResult          m_CurrentState = eResult.Fail; //Used to track the current phase of the behaviour
    //====================

    public override eResult Evaluate()
    {
        m_CurrentState = eResult.Fail; 
        if (m_AI == null)
            m_AI = GetComponent<EnemyAI>();
        if (m_Agent == null)
            m_Agent = GetComponent<NavMeshAgent>(); 
        if (m_Player == null)
            m_Player = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>();
        if (m_rigidbody == null)
            m_rigidbody = GetComponent<Rigidbody>(); 

        if (m_AI.IsPlayerClose())
            return eResult.Success;
        else return eResult.Fail; 
    }

    //fewegwrfeghbrewty
    public override eResult Execute()
    {
        if (m_CurrentState == eResult.Fail && m_AI && m_Player)
        {
            m_Player.DoDamage(m_AI.m_BaseDamage); 
            m_AI.SetAnimationState(2);
            m_AI.ResetAttackCounter();
            m_Agent.enabled = false;
            m_rigidbody.constraints = RigidbodyConstraints.FreezeRotation;
            m_CurrentState = eResult.InProgress; 
        }
        return m_CurrentState; 
    }

    public void InflictDamage()
    {
        //empty function, is only here so the animation event has a reciever. 
    }

    public void FinishHeadbuttAttack()
    {
        m_AI.SetAnimationState(0);
        m_AI.SetActionState(0);
        m_Agent.enabled = true;
        m_rigidbody.constraints = RigidbodyConstraints.None;
        m_CurrentState = eResult.Success; 
    }


}
