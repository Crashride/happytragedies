﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
/// <summary>
/// AUTHOR: DOUGLAS LANDERS  
/// DATE: 17th July 2016 
/// CURRENT STATE: Work In Progress 
/// PURPOSE: 
/// This is the basic class for a gun style weapon, it uses a hit scan algorithm to detect if it has hit something. 
/// It uses game object tags to determine if it has hit a target it can do damage to, then obtains that targets health script to do damage to it. 
/// 
/// TODO :: 
///  -  Add Inacuracy to rayCast to simulate weapon spread
///  -  Add Knockback to the hit entity
///  -  Calculate Damage from weapon enchancements and base damage 
///  -  Reload Animation 
///  -  BulletHit Animation
/// </summary>


public class GunAttack : MonoBehaviour {

    //===================================================
    //Public Vars 
    [Tooltip ("How many times the gun fires a second")]
	public float            m_RateOfFire;
    [Tooltip ("How many bullets can be fired before reloading")]
    public int              m_MagzineSize; 
    [Tooltip("What object tag will result in a 'hit'")]
    public string           m_TargetTag;
    [Tooltip("The Maximum number of weapon effects a weapon can have")]
    public int              m_MaxWeaponEffects = 3; 
    [Tooltip("Weapon Effects can change the properties of different weapons")]
    public WeaponEffect []  m_WeaponEffects;
    [Tooltip("Bullet particle system prefab")]
    public GameObject       m_bulletParticle;
    [Tooltip("The amount of time the muzzle flash will be played for (if using the billboard)")]
    public float            m_MuzzleFlashDuration; 

    //===================================================
    //read me please
    // [ok, I'm reading you (: ]

    //===================================================
    //Private Vars
    
    //How long since reload started
    double                  m_timeSinceReloadStarted;
    double                  m_reloadTime;
    //How long since the last bullet was fired 
    double                  m_dSinceLastShot;
    //How many bullets remain in the magazine 
    int                     m_fBulletsRemaining;
    //A reference to a empty child object where the muzzleflash should be played. 
    ParticleSystem          m_MuzzleFlash;
    //A reference to the gameobject that contains the quads with the muzszle flash texture
    GameObject              m_MuzzleFlashQuad;
    //A reference to the player controller 
    PlayerController m_Player; 

    //sparks system
    public GameObject       Sparks;

    //Attached weapon script
    private Weapon          m_weaponScript;
    
    private float           m_Damage;
    private Image[]         m_reloadUI;

    bool                    reloadStarted = false;
    bool                    m_isAxisInUse = false;
    //===================================================

    // Use this for initialization
    void Start ()
    {
        m_Player = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>(); 
        m_weaponScript = GetComponent<Weapon>();
        m_Damage = m_weaponScript.m_damage;
		m_dSinceLastShot = 5;
        m_MagzineSize = m_weaponScript.m_clipSize;
        m_fBulletsRemaining = m_MagzineSize;
        m_RateOfFire = m_weaponScript.m_attackRate;
        m_reloadTime = m_weaponScript.m_reloadTime;
        m_WeaponEffects = new WeaponEffect[m_MaxWeaponEffects];

        //Use this game objects tag to determine m_TargetTag
        if (gameObject.tag == "Player")
        {
            m_TargetTag = "Enemy"; 
        }
        else if (gameObject.tag =="Enemy")
        {
            m_TargetTag = "Player"; 
        }

        //Get a reference to our muzzleflash 
        if (m_weaponScript.m_WeaponType == WEAPON_TYPE.WEAPON_RIFLE || m_weaponScript.m_WeaponType == WEAPON_TYPE.WEAPON_PISTOL)
        {
            m_MuzzleFlashQuad = transform.GetChild(0).gameObject;
            //turn off the GO by default 
            m_MuzzleFlashQuad.SetActive(false); 
        }
        else
            m_MuzzleFlash = GetComponentInChildren<ParticleSystem>();
        

        //UI Stuff
        GameObject[] temp = GameObject.FindGameObjectsWithTag("GunClip");
        m_reloadUI = new Image[2];
        foreach(GameObject g in temp)
        {
            if(g.GetComponent<Image>().type == Image.Type.Filled)
                m_reloadUI[0] = g.GetComponent<Image>();
            else if(g.GetComponent<Image>().type == Image.Type.Simple)
                m_reloadUI[1] = g.GetComponent<Image>();
        }

    }

    // Update is called once per frame
    void Update()
    {        

        //Update the time its been since the gun fired. 
        m_dSinceLastShot += Time.deltaTime;
        
        //If the player is stunned don't update the weapon
        if (m_Player != null && m_Player.GetIsStunned())
            return;

        //Check to see if the weapon needs to be reloaded
        if (m_fBulletsRemaining <= 0)
        {
            if (Input.GetButtonDown("Fire1"))
            {
                GetComponentInParent<PlayerController>().PlaySound(5);
            }
            //Start Reloading 
            ReLoad();
        }  //If we don't need to reload we can see if the weapon is ready to fire again 
        else if (Input.GetButtonDown("Reload") && m_fBulletsRemaining != m_MagzineSize)
        {
            ReLoad();
        }
        else if (m_weaponScript.m_WeaponType == WEAPON_TYPE.WEAPON_PISTOL) // Single shot weapons
        {
            if(Input.GetAxisRaw("Fire1") != 0)
            {
                if (!m_isAxisInUse)
                {
                    m_isAxisInUse = true;
                    GetComponentInParent<PlayerController>().PlaySound(9);
                    --m_fBulletsRemaining;
                    m_reloadUI[0].fillAmount = ((float)m_fBulletsRemaining / (float)m_MagzineSize);

                    //Now we do a hit scan to see if we hit anything in front of the gun 
                    Instantiate(m_bulletParticle, transform.GetChild(0).position, GameObject.FindGameObjectWithTag("Player").transform.rotation);

                    StartCoroutine(PlayMuzzleFlash(m_MuzzleFlashDuration));//Play muzzle flash

                    m_dSinceLastShot = 0;
                    UpdateWeaponStats();
                }
            }
            if (Input.GetAxisRaw("Fire1") == 0)
                m_isAxisInUse = false;
        }
        else if (Input.GetAxis("Fire1") > 0 && m_dSinceLastShot >= 1 / m_RateOfFire)
        {
            switch(m_weaponScript.m_WeaponType)
            {
                case WEAPON_TYPE.WEAPON_RIFLE:
                    GetComponentInParent<PlayerController>().PlaySound(10);
                    GetComponentInParent<PlayerController>().SlowWalkSpeed(true);
                    break;
                case WEAPON_TYPE.WEAPON_PISTOL:
                    GetComponentInParent<PlayerController>().PlaySound(9);
                    break;
                case WEAPON_TYPE.WEAPON_SHOTGUN:
                    GetComponentInParent<PlayerController>().PlaySound(8);
                    break;
                default:
                    break;
            }
            --m_fBulletsRemaining;
            m_reloadUI[0].fillAmount = ((float)m_fBulletsRemaining / (float)m_MagzineSize);

            //Now we do a hit scan to see if we hit anything in front of the gun 


            if (m_weaponScript.m_WeaponType != WEAPON_TYPE.WEAPON_RIFLE)
            {
                if (m_MuzzleFlash.isPlaying == false)
                    m_MuzzleFlash.Play();
                Instantiate(m_bulletParticle, m_MuzzleFlash.transform.position, GameObject.FindGameObjectWithTag("Player").transform.rotation);
            }
            else
            {
                Instantiate(m_bulletParticle, m_MuzzleFlashQuad.transform.position, GameObject.FindGameObjectWithTag("Player").transform.rotation);

                StartCoroutine(PlayMuzzleFlash(m_MuzzleFlashDuration));//Play muzzle flash
            }
            m_dSinceLastShot = 0;
            UpdateWeaponStats();

        }
        else if (m_MuzzleFlash && m_MuzzleFlash.isPlaying)
        {
            m_MuzzleFlash.Stop();
        }

        if (Input.GetAxisRaw("Fire1") == 0 || m_fBulletsRemaining <= 0)
            GetComponentInParent<PlayerController>().SlowWalkSpeed(false);
    }


    public void UpdateWeapon()
    {
        m_Damage = GetComponent<Weapon>().m_damage;
        m_RateOfFire = GetComponent<Weapon>().m_attackRate;
        m_reloadTime = GetComponent<Weapon>().m_reloadTime; 
        UpdateWeaponStats();
    }

    public void UpdateWeaponStats()
    {
        m_weaponScript.m_clipSize = m_MagzineSize;
        m_weaponScript.m_currentClip = m_fBulletsRemaining;
        m_weaponScript.UpdateUIStats();
    }

    IEnumerator PlayMuzzleFlash(float a_duration)
    {
        m_MuzzleFlashQuad.SetActive(true);
        yield return new WaitForSeconds(a_duration);
        m_MuzzleFlashQuad.SetActive(false);
    }

    //HHYYEEEEAAAAHHHHH
    void ReLoad()
    {
        if(!reloadStarted)
        {
            reloadStarted = true;
            GetComponentInParent<PlayerController>().PlaySound(6);
            GetComponentInParent<Animator>().SetBool("Reloading", true);
            if(m_weaponScript.m_WeaponType==WEAPON_TYPE.WEAPON_SHOTGUN)
            {
                GetComponentInParent<Animator>().SetBool("UsingShotgunReload", true);
                GetComponentInParent<Animator>().SetBool("UsingRifleReload", false);
                GetComponentInParent<Animator>().SetBool("UsingPistolReload", false);

            }
            else if (m_weaponScript.m_WeaponType == WEAPON_TYPE.WEAPON_PISTOL)
            {
                GetComponentInParent<Animator>().SetBool("UsingPistolReload", true);
                GetComponentInParent<Animator>().SetBool("UsingRifleReload", false);
                GetComponentInParent<Animator>().SetBool("UsingShotgunReload", false);

            }
            else {
                GetComponentInParent<Animator>().SetBool("UsingRifleReload", true);
                GetComponentInParent<Animator>().SetBool("UsingShotgunReload", false);
                GetComponentInParent<Animator>().SetBool("UsingPistolReload", false);
            }
        }
        m_reloadUI[0].fillAmount = (float)(m_timeSinceReloadStarted / m_reloadTime);
        m_reloadUI[1].enabled = true;
        m_fBulletsRemaining = 0;
        m_timeSinceReloadStarted += Time.deltaTime;
        if(m_timeSinceReloadStarted >= m_reloadTime)
        {
            m_fBulletsRemaining = m_MagzineSize;
            m_timeSinceReloadStarted = 0.0;
            m_reloadUI[1].enabled = false;
            m_reloadUI[0].fillAmount = 1;
            GetComponentInParent<PlayerController>().PlaySound(7);
            reloadStarted = false;

        }
        UpdateWeaponStats();
    }
}
