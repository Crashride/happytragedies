﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class SoundManager : MonoBehaviour
{

    public AudioClip[] m_playerSounds;
    public AudioClip[] m_enemySounds;
    public AudioClip[] m_enviromentSounds;
    public AudioClip[] m_menuSounds;

    string m_currScene;

    // Use this for initialization
    void Start()
    {
        DontDestroyOnLoad(gameObject);
        m_currScene = SceneManager.GetActiveScene().name;
        if (m_currScene == "MainMenu")
        {
            AudioSource tmep = gameObject.GetComponents<AudioSource>()[0].loop ? gameObject.GetComponents<AudioSource>()[0] : gameObject.GetComponents<AudioSource>()[1];
            tmep.clip = m_menuSounds[2];
            tmep.Play();
        }
        else if (m_currScene == "GameOver")
        {
            AudioSource tmep = gameObject.GetComponents<AudioSource>()[0].loop ? gameObject.GetComponents<AudioSource>()[0] : gameObject.GetComponents<AudioSource>()[1];
            tmep.clip = m_menuSounds[3];
            tmep.Play();
        }
        else
        {
            if (GameObject.FindGameObjectWithTag("GameManager"))
            {
                GameManager gm = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>();
                if (gm.m_currentLevel == gm.BossFloor)
                {
                    AudioSource tmep = gameObject.GetComponents<AudioSource>()[0].loop ? gameObject.GetComponents<AudioSource>()[0] : gameObject.GetComponents<AudioSource>()[1];
                    tmep.clip = m_enemySounds[6];
                    tmep.Play();
                }
            }
            else
            {
                AudioSource tmep = gameObject.GetComponents<AudioSource>()[0].loop ? gameObject.GetComponents<AudioSource>()[0] : gameObject.GetComponents<AudioSource>()[1];
                tmep.clip = m_enviromentSounds[3];
                tmep.Play();
            }
        }
    }



    // Update is called once per frame
    void Update()
    {
        if (SceneManager.GetActiveScene().name != m_currScene)
        {
            m_currScene = SceneManager.GetActiveScene().name;
            if (m_currScene == "MainMenu")
            {
                AudioSource tmep = gameObject.GetComponents<AudioSource>()[0].loop ? gameObject.GetComponents<AudioSource>()[0] : gameObject.GetComponents<AudioSource>()[1];
                tmep.clip = m_menuSounds[2];
                tmep.Play();
            }
            else if (m_currScene == "GameOver")
            {
                AudioSource tmep = gameObject.GetComponents<AudioSource>()[0].loop ? gameObject.GetComponents<AudioSource>()[0] : gameObject.GetComponents<AudioSource>()[1];
                tmep.clip = m_menuSounds[3];
                tmep.Play();
            }
            else
            {
                
                AudioSource tmep = gameObject.GetComponents<AudioSource>()[0].loop ? gameObject.GetComponents<AudioSource>()[0] : gameObject.GetComponents<AudioSource>()[1];
                if (tmep.clip != m_enviromentSounds[3])
                {
                    tmep.clip = m_enviromentSounds[3];
                    tmep.Play();
                }
            }
        }
    }

    public void MenuMove()
    {
        if (GameObject.FindGameObjectWithTag("SoundManager") != gameObject)
        {
            GameObject.FindGameObjectWithTag("SoundManager").GetComponent<SoundManager>().MenuMove();
        }
        else
        {
            AudioSource tmep = gameObject.GetComponents<AudioSource>()[0].loop ? gameObject.GetComponents<AudioSource>()[1] : gameObject.GetComponents<AudioSource>()[0];
            tmep.mute = false;
            tmep.clip = m_menuSounds[0];
            tmep.Play();
        }
    }

    public void MenuSelect()
    {
        if (GameObject.FindGameObjectWithTag("SoundManager") != gameObject)
        {
            GameObject.FindGameObjectWithTag("SoundManager").GetComponent<SoundManager>().MenuSelect();
        }
        else
        {
            AudioSource tmep = gameObject.GetComponents<AudioSource>()[0].loop ? gameObject.GetComponents<AudioSource>()[1] : gameObject.GetComponents<AudioSource>()[0];
            tmep.mute = false;
            tmep.clip = m_menuSounds[1];
            tmep.Play();
        }
    }

    public void PlaySound(GameObject a_object, int a_index)
    {
        if (a_object.tag == "Player" || a_object.tag == "Weapon" || a_object.tag == "Gun")
        {
            if (a_index == 3 || a_index == 13)
            {
                AudioSource[] sources = a_object.GetComponents<AudioSource>();
                sources[1].mute = false;
                sources[1].clip = m_playerSounds[a_index];
                sources[1].Play();
            }
            else if (a_index == 6 || a_index == 7 || a_index == 11)
            {
                AudioSource[] sources = a_object.GetComponents<AudioSource>();
                sources[1].mute = false;
                sources[1].clip = m_playerSounds[a_index];
                sources[1].Play();
            }
            else if (a_index <= 1)
            {
                AudioSource[] sources = a_object.GetComponents<AudioSource>();
                sources[1].mute = false;
                sources[1].clip = m_playerSounds[a_index];
                sources[1].Play();
            }
            else if (a_index == 12)
            {
                AudioSource tmep = gameObject.GetComponents<AudioSource>()[0].loop ? gameObject.GetComponents<AudioSource>()[1] : gameObject.GetComponents<AudioSource>()[0];
                tmep.mute = false;
                tmep.clip = m_playerSounds[a_index];
                tmep.Play();
            }
            else
            {
                if(a_object.GetComponent<AudioSource>().isPlaying)
                {
                    //a_object.GetComponent<AudioSource>().Stop();
                }
                a_object.GetComponent<AudioSource>().mute = false;
                a_object.GetComponent<AudioSource>().clip = m_playerSounds[a_index];
                a_object.GetComponent<AudioSource>().Play();
            }
        }
        else if (a_object.tag == "Enemy" || a_object.tag == "Boss")
        {
            a_object.GetComponent<AudioSource>().mute = false;
            a_object.GetComponent<AudioSource>().clip = m_enemySounds[a_index];
            a_object.GetComponent<AudioSource>().Play();
        }
        else if (a_object.tag == "Door")
        {
            a_object.GetComponent<AudioSource>().mute = false;
            a_object.GetComponent<AudioSource>().clip = m_enviromentSounds[a_index];
            a_object.GetComponent<AudioSource>().Play();
        }
        else if(a_object.tag == "Sealed")
        {
            return;
        }
        else
        {
            AudioSource tmep = gameObject.GetComponents<AudioSource>()[0].loop ? gameObject.GetComponents<AudioSource>()[1] : gameObject.GetComponents<AudioSource>()[0];
            tmep.mute = false;
            tmep.clip = m_enviromentSounds[a_index];
            tmep.Play();
        }
    }

    public void MuteSound(GameObject a_object)
    {
        if (a_object.tag == "Player")
        {
            AudioSource[] sources = a_object.GetComponentsInChildren<AudioSource>();
            foreach (AudioSource zounds in sources)
            {
                zounds.mute = true;
            }
        }
        else
            a_object.GetComponent<AudioSource>().mute = true;
    }
}
