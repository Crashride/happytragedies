﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using Random = UnityEngine.Random;
public class LevelSpawnerPrefab : MonoBehaviour
{
    //Has reference to the player
    //this allows it to set player active after level creation
    //should probably move this to a game manager, here mostly for convinience
    GameObject player;

    [Tooltip("Stores the Room Prefabs to instaniate at runtime")]
    public GameObject[] RoomPrefabs;

    [Tooltip("Stores the Entry Prefabs to instaniate at runtime")]
    public GameObject[] EntryPrefabs;

    [Tooltip("Stores the Exit Prefabs to instaniate at runtime")]
    public GameObject[] ExitPrefabs;

    //[Tooltip("Stores the WTR Prefabs to instaniate at runtime")]
    //public GameObject[] WTRPrefabs;

    [Tooltip("Stores the Room Layouts to instaniate at runtime")]
    public GameObject[] LevelLayout;
    //Finds the holder objects in the scene that rooms will be children of
    //once this takes in a level template it will be extraced from that prefab
    GameObject[] RoomsToBeGenerated;

    public GameObject m_ExitTrigger;

	public GameObject SpawnerManager;
	GameObject Spawner;

	bool entryDone;
    bool exitDone;
    bool wtrDone;
    //if templates have a uniform amount this will not be needed
    private int roomCount;


    private GameObject activeLayout;
    private Transform[] roomHolder;
    // Use this for initialization

    void OnEnable()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        //make sure the player is inactive until after generation
		//SetActive(false);
        entryDone = false;
        exitDone = false;
        activeLayout = LevelLayout[Random.Range(0, LevelLayout.Length)];
        Instantiate(activeLayout, Vector3.zero, Quaternion.identity);

        //find all rooms that are to be generated in the scene
        RoomsToBeGenerated = GameObject.FindGameObjectsWithTag("Room");
        roomCount = RoomsToBeGenerated.Length;

        for (int i = 0; i < roomCount; i++)
        {
            GenerateRoom(i, Random.Range(0, RoomPrefabs.Length));
        }
        player.SetActive(true);
		Spawner = GameObject.FindGameObjectWithTag("SpawnerManager");
		if(!Spawner)
		{
			Instantiate(SpawnerManager, Vector3.zero, Quaternion.identity);
		}

		SpawnerManager spManager = GameObject.FindGameObjectWithTag("SpawnerManager").GetComponent<SpawnerManager>();
		spManager.CreateSpawners(1, RoomsToBeGenerated); //currently hardcoding the level number
    }

    void GenerateRoom(int roomNumber, int roomPrefab)
    {

        var roomCons = RoomsToBeGenerated[roomNumber].GetComponent<Room>();
        if (roomCons.EntryOrExit && (!entryDone || !exitDone))
        {
            if (!entryDone)
            {
                GameObject instance = Instantiate(EntryPrefabs[Random.Range(0, EntryPrefabs.Length)], new Vector3(RoomsToBeGenerated[roomNumber].transform.position.x, 0, RoomsToBeGenerated[roomNumber].transform.position.z), Quaternion.identity) as GameObject;
                instance.transform.SetParent(RoomsToBeGenerated[roomNumber].transform);
                Vector3 Pos = instance.transform.position;

                //valid for 4 door rooms
                DoDoorStuff(roomCons, instance);

                Pos.y += 1;
                player.transform.position = Pos;
                player.GetComponent<FallSafety>().startPos = Pos;
                
                entryDone = true;
            }
            else if (!exitDone)
            {
                GameObject instance = Instantiate(ExitPrefabs[Random.Range(0, ExitPrefabs.Length)], new Vector3(RoomsToBeGenerated[roomNumber].transform.position.x, 0, RoomsToBeGenerated[roomNumber].transform.position.z), Quaternion.identity) as GameObject;
                instance.transform.SetParent(RoomsToBeGenerated[roomNumber].transform);
                GameObject exitInstance = Instantiate(m_ExitTrigger, RoomsToBeGenerated[roomNumber].transform.position, Quaternion.identity) as GameObject;
                DoDoorStuff(roomCons, instance);
                exitInstance.transform.SetParent(instance.transform);
                exitDone = true;
            }
        }
        else
        {
            GameObject instance = Instantiate(RoomPrefabs[roomPrefab], RoomsToBeGenerated[roomNumber].transform.position, Quaternion.identity) as GameObject;
            instance.transform.SetParent(RoomsToBeGenerated[roomNumber].transform);
            DoDoorStuff(roomCons, instance);
        }
	}

    void DoDoorStuff(Room roomCons, GameObject instance)
    {
        Debug.Log(string.Format("North: {0} South {1} East: {2} West {3}", roomCons.north, roomCons.south, roomCons.east, roomCons.west));
        GameObject[] Doors = new GameObject[4];
        int DoorNum = 0;
        foreach (Transform child in instance.GetComponentsInChildren<Transform>())
        {
            if (child.tag == ("Door"))
            {
                Doors[DoorNum] = child.gameObject;
                DoorNum++;
            }
        }

        if (!roomCons.east)
        {
            for (int i = 0; i < 4; i++)
            {
                if (Doors[i].transform.localPosition.x > 3)
				{
					//Doors[i].GetComponent<BoxCollider>().enabled = false;
					//Doors[i].SetActive(false)
					Doors[i].tag = "Door";
				}

            }
        }

        if (!roomCons.west)
        {
            for (int i = 0; i < 4; i++)
            {
                if (Doors[i].transform.localPosition.x < -3)
                {
                    //Doors[i].GetComponent<BoxCollider>().enabled = false;
                    //Doors[i].SetActive(false);
					Doors[i].tag = "Door";
				}

            }
        }

        if (!roomCons.north)
        {
            for (int i = 0; i < 4; i++)
            {
                if (Doors[i].transform.localPosition.z > 3)
                {
					//Doors[i].GetComponent<BoxCollider>().enabled = false;
					//Doors[i].SetActive(false);
					Doors[i].tag = "Door";
				}

            }
        }

        if (!roomCons.south)
        {
            for (int i = 0; i < 4; i++)
            {
                if (Doors[i].transform.localPosition.z < -3)
                {
					//Doors[i].GetComponent<BoxCollider>().enabled = false;
					//Doors[i].SetActive(false);
					Doors[i].tag = "Door";
				}

            }
        }
    }
}
