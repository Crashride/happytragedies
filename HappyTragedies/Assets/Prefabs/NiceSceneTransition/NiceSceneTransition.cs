﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.SceneManagement;
public class NiceSceneTransition : MonoBehaviour
{
    GameManager gm;
    Text Floor;
    Text Number;
    public static NiceSceneTransition instance = null;
    public float FadeInTime = 1.0f;
    public float FadeOutTime = 1.0f;
    public bool fadeIn;
    public bool fadeOut;
    public bool endDone = false;
    public Image fadeImg;
    float time = 0.0f;
    // Use this for initialization
    void Awake()
    {
        if (instance == null)
        {
            DontDestroyOnLoad(transform.gameObject);
            instance = this;
            if (fadeIn)
            {
                fadeImg.color = new Color(fadeImg.color.r, fadeImg.color.g, fadeImg.color.b, 1.0f);
            }
        }
        else
        {
            Destroy(transform.gameObject);
        }
        if(GameObject.FindGameObjectWithTag("GameManager"))
        {
            gm = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>();
            Floor = GameObject.FindGameObjectWithTag("LevelUI").GetComponent<LevelUIflash>().m_texts[1];
            Number = GameObject.FindGameObjectWithTag("LevelUI").GetComponent<LevelUIflash>().m_texts[0];
        }
        if(SceneManager.GetActiveScene().name == "SplashScreen")
        {
            NiceSceneTransition.instance.LoadScene("MainMenu");
        }
    }
    void OnEnable()
    {
        if (fadeIn)
        {
            StartCoroutine(StartScene());
        }
    }
    public void LoadScene(string level)
    {
        StartCoroutine(EndScene(level));
    }
    IEnumerator StartScene()
    {

        if (gm)
        {
            Floor.enabled = true;
            Number.enabled = true;
            Number.text = (gm.m_currentLevel + 26).ToString();
        }
        time = 1.0f;
        yield return null;
        while (time >= 0.0f)
        {
            fadeImg.color = new Color(fadeImg.color.r, fadeImg.color.g, fadeImg.color.b, time);
            time -= Time.unscaledDeltaTime * (1.0f / FadeInTime);
            yield return new WaitForEndOfFrame();
        }
        endDone = false;
        fadeImg.gameObject.SetActive(false);
        if (gm)
        {
            Floor.enabled = false;
            Number.enabled = false;
        }
    }

    // Destroy this fucjer between scenes;
    IEnumerator EndScene(string nextScene)
    {
        if (SceneManager.GetActiveScene().name == "SplashScreen")
            yield return new WaitForSecondsRealtime(5);
        if (gm)
        {
            Floor.enabled = true;
            Number.enabled = true;
            Number.text = (gm.m_currentLevel + 26).ToString();
        }
        fadeImg.gameObject.SetActive(true);
        time = 0.0f;
        yield return new WaitForEndOfFrame();
        while (time <= 1.0f)
        {
            fadeImg.color = new Color(fadeImg.color.r, fadeImg.color.g, fadeImg.color.b, time);
            time += Time.unscaledDeltaTime * (1.0f / FadeOutTime);
            yield return new WaitForEndOfFrame();
        }
        fadeImg.color = new Color(fadeImg.color.r, fadeImg.color.g, fadeImg.color.b, 1.0f);
        endDone = true;
        SceneManager.LoadScene(nextScene);
        if (SceneManager.GetActiveScene().name == "SplashScreen")
        {
            DestroyImmediate(gameObject);
            yield break;
        }
        StartCoroutine(StartScene());
        //yield break;
    }
}