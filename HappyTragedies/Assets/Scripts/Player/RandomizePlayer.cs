﻿using UnityEngine;
using System.Collections;

public class RandomizePlayer : MonoBehaviour {

	public Texture[] hairTextures;
    public Texture[] hairTextureLong;
    GameObject longHairMesh;
    GameObject shortHairMesh;
    private Material hairMat;
    bool longHair;
	// Use this for initialization
	void Awake () {
        //get the material and randomise it with random hair texture

        Transform[] childrenObj = GetComponentsInChildren<Transform>();
            foreach(Transform t in childrenObj)
        {
            if(t.tag=="Long Hair")
            {
                longHairMesh = t.gameObject;
            }
            if(t.tag == "Short Hair")
            {
                shortHairMesh = t.gameObject;
            }
        }
        int ChooseHairType = Random.Range(0, 2);
        if (ChooseHairType == 1)
        {
            longHair = true;
            shortHairMesh.SetActive(false);
        }
        else
        {
            longHair = false;
            longHairMesh.SetActive(false);
        }
            if (longHair)
		hairMat = longHairMesh.GetComponentInChildren<Renderer>().material;
        else
		hairMat = shortHairMesh.GetComponentInChildren<Renderer>().material;
        if (longHair)
            hairMat.mainTexture = hairTextureLong[Random.Range(0, hairTextures.Length)];
        else
            hairMat.mainTexture = hairTextures[Random.Range(0, hairTextures.Length)];
    }

    public void RandomiseHair()
	{
        //randomise the hair texture
        int ChooseHairType = Random.Range(0, 2);
        if (ChooseHairType == 1)
        {
            longHair = true;
            shortHairMesh.SetActive(false);
        }
        else
        {
            longHair = false;
            longHairMesh.SetActive(false);
        }
        if (longHair)
            hairMat = longHairMesh.GetComponent<Renderer>().material;
        else
            hairMat = shortHairMesh.GetComponent<Renderer>().material;
        if (longHair)
            hairMat.mainTexture = hairTextureLong[Random.Range(0, hairTextures.Length)];
        else
            hairMat.mainTexture = hairTextures[Random.Range(0, hairTextures.Length)];
    }
}
