﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(NavMeshAgent))]
[RequireComponent(typeof(Health))]

public class EnemyController : MonoBehaviour
{

    public float m_seekRadius;

    public float BulletDamage;
    public bool isDead = false;

    Health health;

    public bool m_wandering = false;

    private Animator anim;
	private ParticleSystem enimHit;
    // Use this for initialization
    void Start()
    {
        health = gameObject.GetComponent<Health>();
        anim = gameObject.GetComponent<Animator>();
        GetComponent<NavMeshAgent>().SetDestination(GameObject.FindGameObjectWithTag("Player").transform.position);
		enimHit = GetComponentInChildren<ParticleSystem>();
		enimHit.Clear();
		enimHit.Stop();
	}

    // Update is called once per frame
    void Update()
    {
        if (!isDead)
        {
            Ray tRay = new Ray(GetComponent<Transform>().position, (GameObject.FindGameObjectWithTag("Player").transform.position - GetComponent<Transform>().position));
            RaycastHit data;
            if (Physics.Raycast(tRay, out data))
            {
                if ((data.collider.tag == "Player" && data.distance <= m_seekRadius) || data.collider.tag == "Enemy")
                {
                    //anim.SetInteger("State", 1);
                    GetComponent<NavMeshAgent>().SetDestination(GameObject.FindGameObjectWithTag("Player").transform.position);
                }
                else if(m_wandering)
                {
                    //if(GetComponent<NavMeshAgent>().)
                    if(GetComponent<NavMeshAgent>().pathStatus == NavMeshPathStatus.PathComplete || GetComponent<NavMeshAgent>().velocity.magnitude <= 0.0f)
                    {
                        m_wandering = false;
                    }
                }
                else
                {
                    //Matt's super fucking shit house wander function
                    GetComponent<NavMeshAgent>().ResetPath();
                    m_wandering = true;
                    float temp = 22.5f - Random.Range(0, 45);
                    Quaternion ranAng = Quaternion.Euler(0, temp, 0);
                    Vector3 ranPoint = ranAng * gameObject.transform.forward;
                    GetComponent<NavMeshAgent>().SetDestination(gameObject.transform.position + new Vector3(ranPoint.x, 0, ranPoint.z));
                }
            }
            //set the navmashagent to seek the player
        }

        if(health.GetHealth() <= 0)
        {
            isDead = true;
            anim.SetBool("Dead", true);
        }
    }

    void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.tag == "Player" && !isDead)
        {
            anim.SetInteger("State" , 2);
            other.gameObject.GetComponentInParent<Health>().DoDamage(GetComponent<EnemyAttack>().m_AttackDamage);
            gameObject.GetComponent<Rigidbody>().AddExplosionForce(9, other.transform.position, 1, 0, ForceMode.Impulse);
        }
    }


    public void finsihedDying()
    {
        GameObject instance = FindObjectOfType<WeaponGenerator>().GenerateWeapon(100);
        instance.AddComponent<WeaponSwap>();

        instance.transform.position = gameObject.transform.position;
        if(instance.GetComponent<GunAttack>())
        {
            instance.GetComponent<GunAttack>().enabled = false;
        }
        else
        {
            instance.GetComponent<MeleeAttack>().enabled = false;
        }
        GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>().GainExp(3);
        Debug.Log("finishedDying is called");
        gameObject.SetActive(false);
    }

    //Sound Manager Getter

    public void PlaySound(int a_index)
    {
        SoundManager sm = GameObject.FindGameObjectWithTag("SoundManager").GetComponent<SoundManager>();
        sm.PlaySound(gameObject, a_index);
    }

    public void MuteSound()
    {
        SoundManager sm = GameObject.FindGameObjectWithTag("SoundManager").GetComponent<SoundManager>();
        sm.MuteSound(gameObject);
    }

	public void PlayOnHitParticles()
	{
		if (enimHit.isPlaying)
		{
			return;
		}
		else
		{
			enimHit.Clear();
			enimHit.Play();
		}
	}
}
