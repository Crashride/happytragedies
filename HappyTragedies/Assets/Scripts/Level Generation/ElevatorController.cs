﻿using UnityEngine;
using System.Collections;

public class ElevatorController : MonoBehaviour
{
    public Collider m_floorCollider;
    GameObject player;
    Transform startNode;
    bool paused;
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        startNode = GameObject.FindGameObjectWithTag("StartNode").transform;
        player.transform.position = startNode.position + new Vector3(0, 0.05f, 0);
        player.transform.SetParent(startNode);
    }
    public void Pause()
    {
        GameObject.FindGameObjectWithTag("Roof").GetComponent<Collider>().enabled = false;
        m_floorCollider.enabled = false;
        paused = true;
        player.GetComponent<Rigidbody>().Sleep();
        player.GetComponent<PlayerController>().PauseMovement(true);
        player.GetComponent<FallSafety>().enabled = false;
    }
    void Update()
    {
        if (paused)
        {
            player.GetComponent<Rigidbody>().Sleep();

            player.transform.position = startNode.position + new Vector3(0, 0.05f, 0);
        }
    }
    public void UnPause()
    {
        m_floorCollider.enabled = true;
        GameObject.FindGameObjectWithTag("Roof").GetComponent<Collider>().enabled = true;

        paused = false;
        player.transform.SetParent(null);
        player.GetComponent<Rigidbody>().useGravity = true;
        player.GetComponent<PlayerController>().PauseMovement(false);
        player.GetComponent<FallSafety>().enabled = true;
        DontDestroyOnLoad(player);
    }
}
