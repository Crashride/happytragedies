﻿using UnityEngine;
using System.Collections;

public class BossMeleeAttack1Prepare : AIBehaviour
{

    GameObject m_Player;
    NavMeshAgent m_Agent;
    BossAI m_AI;
    eResult m_CurrentState; 

    public override eResult Execute()
    {
        if (m_CurrentState == eResult.Fail)
        {
            m_Agent.SetDestination(m_Player.transform.position);
            m_AI.SetAnimationState(1); //walking
            m_CurrentState = eResult.InProgress; 
        }
        else if (m_CurrentState == eResult.InProgress)
        {
            m_Agent.SetDestination(m_Player.transform.position);
            if (m_AI.IsPlayerClose("Melee"))
                m_CurrentState = eResult.Success;  
        }

        return m_CurrentState;
    }

    public override eResult Evaluate()
    {
        if (m_Player == null)
            m_Player = GameObject.FindGameObjectWithTag("Player");
        if (m_Agent == null)
            m_Agent = GetComponent<NavMeshAgent>();
        if (m_AI == null)
            m_AI = GetComponent<BossAI>();
        m_CurrentState = eResult.Fail; 

        return eResult.Success;
    }
}
