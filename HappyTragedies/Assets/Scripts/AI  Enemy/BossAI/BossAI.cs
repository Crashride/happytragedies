﻿using UnityEngine;
using System.Collections;
using System.Reflection; 

[RequireComponent (typeof(Rigidbody))]
[RequireComponent (typeof(NavMeshAgent))]
[RequireComponent (typeof(Health))]
public class BossAI : MonoBehaviour {

    //=====================
    //Behaviours 
    [Tooltip("The behavi9our to be executed upon death")]
    public string   m_OnDeath           = "empty";
    [Tooltip("The zeroth attack behaviour")]
    public string   m_Attack0           = "empty"; //Attack 0
    [Tooltip("The behaviour that can be used to prepare for the zeroth attack")]
    public string   m_Attack0Prepare    = "empty"; 
    [Tooltip("The first attack behaviour")]
    public string   m_Attack1           = "empty"; //Attack 1
    [Tooltip("The behaviour that can be used to prepare for the first attack")]
    public string   m_Attack1Prepare    = "empty";
    [Tooltip("The second attack behaviour")]
    public string   m_Attack2           = "empty"; //Attack 2
    [Tooltip("The behaviour that can be used to prepare for the second attatk")]
    public string   m_Attack2Prepare    = "empty"; 
    [Tooltip("The behaviour that is executed when the AI reaches 75% health")]

    public string   m_On75Health = "empty";
    [Tooltip("The behaviour that is executed when the AI reaches 50% health")]
    public string   m_On50Health = "empty";
    [Tooltip("The behaviour that is executed when the AI reaches 25% health")]
    public string   m_On25Health = "empty"; 


    AIBehaviour     m_DeathBehaviour;
    AIBehaviour     m_Attack0Behaviour;
    AIBehaviour     m_Attack0PrepareBehaviour;
    AIBehaviour     m_Attack1Behaviour;
    AIBehaviour     m_Attack1PrepareBehaviour;
    AIBehaviour     m_Attack2Behaviour;
    AIBehaviour     m_Attack2PrepareBehaviour; 
    AIBehaviour     m_75HealthBehaviour;
    AIBehaviour     m_50HealthBehaviour;
    AIBehaviour     m_25HealthBehaviour;
    //=====================

    //=====================
    //Public Vars 
    [Tooltip ("The amount of damage this enemy will inflict on its heavy attacks first strike")]
    public float            m_HeavyDamage1;
    [Tooltip ("The amount of damage this enemy will inflict on its heavy attacks seconds strike")]
    public float            m_HeavyDamage2;
    [Tooltip ("The amount of damage this enemy will inflict while using its quick attack")]
    public float            m_QuickDamage; 
    [Tooltip("The amount of damage that the boss' stun attack will inflict on the player")]
    public float            m_StunDamage = 0;
    [Tooltip("How long in seconds the boss' stun attack will stun the player for")]
    public double           m_StunDuration; 
    [Tooltip("The amount of money the player will recieve for killing the boss")]
    public float            m_KillReward;
    [Tooltip("The amount of exp the player will recieve for killing the boss")]
    public int            m_ExpReward;
    [Tooltip ("How long the first attack will take to be ready again")]
    public float            m_Attack0Cooldown;  //Attack 0
    [Tooltip("How long the second attack will take to be ready again")]
    public float            m_Attack1Cooldown; //Attack 1
    [Tooltip("How long the third attack will take to be ready again")]
    public float            m_Attack2Cooldown; 
    [Tooltip("The amount of absolute cooldown between attacking and going into another action")]
    public float            m_AbsoluteCooldown; 
    [Tooltip("A reference to the particle effect to play when this AI gets hit.")]
    public ParticleSystem   m_BloodEffect;
    [Tooltip("A reference to the Gameobject used as a warning for the attack range of the Boss' stun attack")]
    public GameObject       m_WarningCircle;
    [Tooltip("A reference to the particle effect to play when the Boss' stun attack lands")]
    public GameObject       m_ShockWaveEffect; 
    [Tooltip("The amount of enemies to spawn when the boss reaches 75% health")]
    public int              m_SpawnCount75;
    [Tooltip("The amount of enemies to spawn when the boss reaches 50% health")]
    public int              m_SpawnCount50;
    [Tooltip("The amount the boss' speed will be boosted at 25%")]
    public float            m_BoostAmount;

    //Sorry Doug, this gone get messy
    [Tooltip("The duration of the shake that occurs on death xplosion")]
    public float m_deathXplodeShakeDuration;
    [Range(0, 1.0f)]
    [Tooltip("The intensity of the shake that occurs on death xplosion")]
    public float m_deathXplodeShakeIntensity;
    [Tooltip("The duration of the shake that occurs on stun attack")]
    public float m_stunShakeDuration;
    [Range(0, 1.0f)]
    [Tooltip("The intensity of the shake that occurs on stun attack")]
    public float m_stunShakeIntensity;


    //===================

    //===================
    //Private Vars 
    bool            m_Active; 
    bool            m_Alive;                    //Whether or not the AI is currently alive
    bool            m_HasDone75;                //Whether or not the AI has done 75% behaviour 
    bool            m_HasDone50;                //Whether or not the AI has done 50% behaviour
    bool            m_HasDone25;                //Whether or not the AI has done 75% behaviour
    Health          m_Health;                   //A reference to the AIs health component
    GameObject      m_Player;                   //A reference to the player's gameobject
    Animator        m_Animator;                 //A reference to the AIs animator component
    AIBehaviour     m_InProgressAction;         //A reference to the action currently being executed
    Rigidbody       m_RigidBody;                //A reference to the rigidbody attached to this gameobject 
    float           m_SinceLastAttack0 = 10.0f; //Used to time the cooldown between attacks of type 0
    float           m_SinceLastAttack1 = 10.0f; //Used to time the cooldown between attacks of type 1
    float           m_SinceLastAttack2 = 10.0f; // Used to time the cooldown between attacks of type 2
    bool            m_MeleeClose = false;       //Used to determine if it is possible to melee attack the player
    bool            m_RangedClose = false;      //used to determine if it is possible to ranged attack the player
    float           m_CooldownAbsoluteTimer;    //A timer used to count a cooldown of the AI idling between attacks 
    //===================

    // Use this for initialization
    void Start ()
    {
        //Get our references to components and the player
        m_Health    = GetComponent<Health>();
        m_Animator  = GetComponent<Animator>();
        m_Player    = GameObject.FindGameObjectWithTag("Player");
        m_RigidBody = GetComponent<Rigidbody>(); 

        //default our bools 
        m_Alive     = true;
        m_Active    = true;
        m_HasDone25 = false;
        m_HasDone50 = false;
        m_HasDone75 = false;

        //default our timers 
        m_SinceLastAttack0      = m_Attack0Cooldown;
        m_SinceLastAttack1      = m_Attack1Cooldown;
        m_CooldownAbsoluteTimer = m_AbsoluteCooldown; 
         
        //Initialise our behaviours 
        m_DeathBehaviour    = InitialiseBehaviour(m_OnDeath);
        m_Attack0Behaviour  = InitialiseBehaviour(m_Attack0);
        m_Attack1Behaviour  = InitialiseBehaviour(m_Attack1);
        m_Attack2Behaviour  = InitialiseBehaviour(m_Attack2);
        m_75HealthBehaviour = InitialiseBehaviour(m_On75Health);
        m_50HealthBehaviour = InitialiseBehaviour(m_On50Health);
        m_25HealthBehaviour = InitialiseBehaviour(m_On25Health);
        m_Attack0PrepareBehaviour = InitialiseBehaviour(m_Attack0Prepare);
        m_Attack1PrepareBehaviour = InitialiseBehaviour(m_Attack1Prepare);
        m_Attack2PrepareBehaviour = InitialiseBehaviour(m_Attack2Prepare); 

    }

    //Initialisation Helper function, determines if a behaviour should be empty or 
    //if it should add a component of a specific type
    AIBehaviour InitialiseBehaviour(string a_BehaviourName)
    {
        if (a_BehaviourName == "empty")
        {
            return gameObject.AddComponent<AIBehaviour>();
        }
        else
        {
            return CreateBehaviourFromName(a_BehaviourName) as AIBehaviour;
        }
    }

    //Generation function used to add components of a given type, prodivded by Matt
    private AIBehaviour CreateBehaviourFromName(string strBehaviourName)
    {
        System.Type t = Assembly.GetAssembly(this.GetType()).GetType(strBehaviourName);
        AIBehaviour newBehaviour = gameObject.AddComponent(t) as AIBehaviour;

        return newBehaviour;
    }

    // Update is called once per frame
    void Update ()
    {
	    if (m_Alive && m_Active)
        {
            //make sure we're looking at the player, and update our cooldowns; 
            if (m_RigidBody.constraints != RigidbodyConstraints.FreezeAll || m_RigidBody.constraints != RigidbodyConstraints.FreezeRotation)
            {
                Vector3 lookat = m_Player.transform.position;
                lookat.y = transform.position.y;
                transform.LookAt(lookat, Vector3.up);
            }

            Debug.Log("Doug Mode activated. Spawning more Dougs");


            m_SinceLastAttack0 += Time.deltaTime;
            m_SinceLastAttack1 += Time.deltaTime; 

            //check we are still alive
            if (m_Health.GetHealth() <= 0.0f)
            {
                m_Alive = false;
                m_DeathBehaviour.Execute();
                return;
            }
            //Check for an inprogress action
            if (m_InProgressAction != null)
            {
                AIBehaviour.eResult result = m_InProgressAction.Execute(); 
                if (result == AIBehaviour.eResult.Success || result == AIBehaviour.eResult.Fail)
                {
                    SetAnimationState(0); 
                    m_InProgressAction = null; 
                }
                return; 
            }
            //Check for 75% action
            if (m_HasDone75 == false && m_Health.GetHealth() <= m_Health.GetMaxHealth() * 0.75f)
            {
                if (m_75HealthBehaviour.Evaluate() == AIBehaviour.eResult.Success)
                {
                    if (m_75HealthBehaviour.Execute() == AIBehaviour.eResult.InProgress)
                    {
                        m_InProgressAction = m_75HealthBehaviour;
                        PlaySound(5);
                    }
                    return;
                }
            }
            //Check for 50% action
            if (m_HasDone50 == false && m_Health.GetHealth() <= m_Health.GetMaxHealth() * 0.5f)
            {
                if (m_50HealthBehaviour.Evaluate() == AIBehaviour.eResult.Success)
                {
                    if (m_50HealthBehaviour.Execute() == AIBehaviour.eResult.InProgress)
                    {
                        m_InProgressAction = m_50HealthBehaviour;
                        PlaySound(5);
                    }
                    return; 
                }
            }
            //Check for 25% action 
            if (m_HasDone25 == false && m_Health.GetHealth() <= m_Health.GetMaxHealth() * 0.25f)
            {
                if (m_25HealthBehaviour.Evaluate() == AIBehaviour.eResult.Success)
                {
                    if (m_25HealthBehaviour.Execute() == AIBehaviour.eResult.InProgress)
                    {
                        m_InProgressAction = m_25HealthBehaviour;
                        PlaySound(5);
                    }
                    return; 
                }
            }
            //if we get this far into the function, we can attack, we need to figure out which attacks we can do
            int num;
            if (m_HasDone50)
                num = Random.Range(0, 3);
            else if (m_HasDone75)
                num = Random.Range(0, 2);
            else
                num = 0;

            for (int i = 0; i < 3; i++)
            {
                switch ((num + i) % 3)
                {
                    case 0:
                        if (m_SinceLastAttack0 >= m_Attack0Cooldown)
                        {
                            if (m_Attack0Behaviour.Evaluate() == AIBehaviour.eResult.Success)
                            {
                                if (m_Attack0Behaviour.Execute() == AIBehaviour.eResult.InProgress)
                                {
                                    m_SinceLastAttack0 = 0.0f;
                                    m_InProgressAction = m_Attack0Behaviour;
                                    return;
                                }
                            }
                            else if (m_Attack0PrepareBehaviour.Evaluate() == AIBehaviour.eResult.Success)
                                if (m_Attack0PrepareBehaviour.Execute() == AIBehaviour.eResult.InProgress)
                                    m_InProgressAction = m_Attack0PrepareBehaviour; 
                        }
                        break;
                    case 1:
                        if (m_SinceLastAttack1 >= m_Attack1Cooldown && m_HasDone75)
                        {
                            if (m_Attack1Behaviour.Evaluate() == AIBehaviour.eResult.Success)
                            {
                                if (m_Attack1Behaviour.Execute() == AIBehaviour.eResult.InProgress)
                                {
                                    m_SinceLastAttack1 = 0.0f;
                                    m_InProgressAction = m_Attack1Behaviour;
                                    return;
                                }
                            }
                            else if (m_Attack1PrepareBehaviour.Evaluate() == AIBehaviour.eResult.Success)
                                if (m_Attack1PrepareBehaviour.Execute() == AIBehaviour.eResult.InProgress)
                                    m_InProgressAction = m_Attack1PrepareBehaviour;
                        }
                        break;
                    case 2:
                        if (m_SinceLastAttack2 >= m_Attack2Cooldown && m_HasDone50)
                        {
                            if (m_Attack2Behaviour.Evaluate() == AIBehaviour.eResult.Success)
                            {
                                if (m_Attack2Behaviour.Execute() == AIBehaviour.eResult.InProgress)
                                {
                                    m_SinceLastAttack2 = 0.0f;
                                    m_InProgressAction = m_Attack2Behaviour;
                                    return;
                                }
                            }
                            else if (m_Attack2PrepareBehaviour.Evaluate() == AIBehaviour.eResult.Success)
                                if (m_Attack2PrepareBehaviour.Execute() == AIBehaviour.eResult.InProgress)
                                    m_InProgressAction = m_Attack2PrepareBehaviour;
                        }
                        break;
                    default:
                        break;
                }
            }    
        }
	}

    
    public int GetBossFightSpawnRate(int a_turn)
    {
        switch (a_turn)
        {
            case 0:
                return m_SpawnCount75;
            case 1:
                return m_SpawnCount50;
            default:
                return 0; 
        }
    }

    //Called to inflict damage to the boss' health component, we go through this function so that we can play blood effects
    public void TakeDamage(float a_damage)
    {
        Debug.Log("Boss Damaged");
        m_Health.DoDamage(a_damage);
    }

    public void ResetAttackCounter(int a_attackNo) // 0 = attack0 | 1 = attack1
    {
        switch(a_attackNo)
        {
            case 0:
                m_SinceLastAttack0 = 0.0f;
                break;
            case 1:
                m_SinceLastAttack1 = 0.0f;
                break; 
                
        }   
    }
    //used to set the value of the player close variables from outside of this class 
    public void SetPlayerClose(string a_field, bool a_bool)
    {
        if (a_field == "Melee")
        {
            m_MeleeClose = a_bool; 
        }
        if (a_field == "Ranged")
        {
            m_RangedClose = a_bool; 
        }
    }

    public bool IsPlayerClose(string a_field)
    {
        if (a_field == "Melee")
        {
            return m_MeleeClose; 
        }
        if (a_field == "Ranged")
        {
            return m_RangedClose; 
        }
        return false; 
    }

    //used to find the damage dealt by the boss' heavy attacks 
    public float GetHeavyDamage(int a_strike)
    {
        if (a_strike == 1)
            return m_HeavyDamage1;
        else if (a_strike == 2)
            return m_HeavyDamage2;
        else return 0; 
    }

    //used to find the damage dealt by the boss' quick attack 
    public float GetQuickDamage()
    {
        return m_QuickDamage; 
    }

    public float GetKillReward()
    {
        return m_KillReward; 
    }

    public int GetExpReward()
    {
        return m_ExpReward; 
    }
    public void SetAnimationState(int a_StateInt)
    {
        m_Animator.SetInteger("State", a_StateInt);
    }

    public void SetAnimationBool(string a_boolName, bool a_bool)
    {
        m_Animator.SetBool(a_boolName, a_bool); 
    }

    public float GetStunDamage()
    {
        return m_StunDamage; 
    }

    public GameObject GetWarningCircle()
    {
        return m_WarningCircle; 
    }

    public GameObject GetShockWave()
    {
        return m_ShockWaveEffect; 
    }

    public double GetStunDuration()
    {
        return m_StunDuration; 
    }

    public void MakeActive()
    {
        m_Active = true; 
    }

    public float GetBoostAmount()
    {
        return m_BoostAmount; 
    }

    public void DeathShake()
    {
        if (!Camera.main.GetComponent<FollowPlayer>().Shaking())
            Camera.main.GetComponent<FollowPlayer>().Shake(m_deathXplodeShakeDuration, m_deathXplodeShakeIntensity);
    }

    public void SetHasDone(int a_case)
    {
        switch(a_case)
        {
            case 0:
                m_HasDone75 = true;
                break;
            case 1:
                m_HasDone50 = true;
                break;
            case 2:
                m_HasDone25 = true;
                break; 
            default:
                break; 
        }
    }

    //========================================
    //      AUDIO FUNCTIONS 

    //Finds the sound manager and uses it to play this gameobjects sound 
    public void PlaySound(int a_index)
    {
        SoundManager sm = GameObject.FindGameObjectWithTag("SoundManager").GetComponent<SoundManager>();
        if (sm != null)
            sm.PlaySound(gameObject, a_index);
    }

    //Find the sound manager and use it to mute this gameobjects sound
    public void MuteSound()
    {
        SoundManager sm = GameObject.FindGameObjectWithTag("SoundManager").GetComponent<SoundManager>();
        if (sm != null)
            sm.MuteSound(gameObject);
    }

}
