﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public static GameManager instance = null;
    Health m_playerHealth;
    public GameObject PlayerPrefab;
    GameObject instancePlayer;
    Vector3 PlayerOriginalStartPos;
    public int BossFloor;

    public bool m_Paused = false;
    [SerializeField]
    private bool m_EndGame = false;

    public GameObject m_togglableUIPrefab;
    GameObject m_togglableUI;

    public GameObject m_PausePrefab;
    GameObject m_PauseMenu;

    UIStats m_InGameUI;

	//stores the current level the player is on
    public int m_currentLevel = 0;

	//canvas to be displayed on death
    public GameObject m_deathScreen;
    
    //hold the soundmanager and a corresponding prefab
    private GameObject soundManager;
    public GameObject soundManagerPrefab;

    public GameObject m_PickupPopup;

    // Use this for initialization
    void Awake()
    {
        m_togglableUI = GameObject.FindGameObjectWithTag("Stats");
        if (!m_togglableUI)
        {
            GameObject temp = Instantiate(m_togglableUIPrefab);
            m_togglableUI = temp;
            m_togglableUI.GetComponentInChildren<Canvas>().worldCamera = Camera.main;
            m_InGameUI = m_togglableUI.GetComponentInChildren<UIStats>(true);
            m_InGameUI.UpdateFloorNumber(m_currentLevel);
            DontDestroyOnLoad(m_togglableUI);
            //DontDestroyOnLoad(m_InGameUI);
        }
        else
        {
            m_togglableUI.GetComponentInChildren<Canvas>().worldCamera = Camera.main;
            m_InGameUI = m_togglableUI.GetComponentInChildren<UIStats>();
            m_InGameUI.UpdateFloorNumber(m_currentLevel);
            DontDestroyOnLoad(m_togglableUI);
            DontDestroyOnLoad(m_InGameUI);
        }

        m_PauseMenu = GameObject.FindGameObjectWithTag("Pause");
        if(!m_PauseMenu)
        {
            m_PauseMenu = Instantiate(m_PausePrefab);
        }
        m_PauseMenu.SetActive(false);
        DontDestroyOnLoad(m_PauseMenu);
        // if no game manager exists create one
        if (instance == null)
        {
            instance = this;
        }
		//if a game manager already exists destroy it        
        else if(instance != this)
        {
            Destroy(this);
        }

		//Dont destroy this gamemanger in between scenes
		DontDestroyOnLoad(gameObject);

		//check to see if there is already a player and if there isnt create one
		GameObject Player = GameObject.FindGameObjectWithTag("Player");
        if (!Player)
        {
            instancePlayer = Instantiate(PlayerPrefab, new Vector3(-0.84f, 1f, 0.01f), Quaternion.identity) as GameObject;
            m_playerHealth = instancePlayer.GetComponent<Health>();
            PlayerOriginalStartPos = instancePlayer.transform.position;
        }
        else
        {
            instancePlayer = Player;
            m_playerHealth = instancePlayer.GetComponent<Health>();
            PlayerOriginalStartPos = instancePlayer.transform.position;

        }

		// once we are no longer on the tutorial level, initialise the level spawner and activate it
		if (SceneManager.GetActiveScene().name != "Tzar" && SceneManager.GetActiveScene().name != "John's Scene" && SceneManager.GetActiveScene().name != "StartLevel" && SceneManager.GetActiveScene().name != "MainMenu" && SceneManager.GetActiveScene().name != "GameOver")
        {
            LevelGeneration spawnerScript = GetComponent<LevelGeneration>();
            if (m_currentLevel % 3 != 0)
            {
                spawnerScript.GenerateLevel(false);//dont need wts
            }
            else
            {
                spawnerScript.GenerateLevel(true);//need wts
            }
        }
        soundManager = GameObject.FindGameObjectWithTag("SoundManager");
		//if no sound mananger currently exists create one
        if (!soundManager)
        {
            GameObject Instance = Instantiate(soundManagerPrefab);
            soundManager = Instance;
        }

    }

    void OnLevelWasLoaded(int level)
    {
        if(m_currentLevel == BossFloor && SceneManager.GetActiveScene().name != "GameOver")
        {
            LevelGeneration spawnerScript = GetComponent<LevelGeneration>();
            spawnerScript.GenerateBossRoom();
        }
        else if (level != 0 && SceneManager.GetActiveScene().name != "Tzar"&& SceneManager.GetActiveScene().name != "StartLevel" && SceneManager.GetActiveScene().name != "GameOver")
        {
            LevelGeneration spawnerScript = GetComponent<LevelGeneration>();
            if (m_currentLevel % 3 != 0)
            {
                spawnerScript.GenerateLevel(false);//dont need wts
            }
            else
            {
                spawnerScript.GenerateLevel(true);//need wts
            }
        }
        m_togglableUI.GetComponentInChildren<Canvas>().worldCamera = Camera.main;

    }

    void Start()
    {
        DontDestroyOnLoad(m_deathScreen);
        //Reset Timescale
        Time.timeScale = 1.0f;

        m_Paused = false;
        m_EndGame = false;
        m_playerHealth = instancePlayer.GetComponent<Health>();
    }

    // Update is called once per frame
    void Update()
    {
        if (m_playerHealth.GetHealth() > 0)
            m_EndGame = false;

        if (m_playerHealth.GetHealth() <= 0 && !m_EndGame)
        {
            m_EndGame = true;
            //Time.timeScale = 0;
            //GameObject UI = Instantiate(m_deathScreen);
            //UI.SetActive(true);
            Destroy(instancePlayer);
            m_togglableUI.SetActive(false);
            SceneManager.LoadScene("GameOver");
        }
        if (m_Paused)
        {
            Time.timeScale = 0.0f;
        }
        else
            Time.timeScale = 1.0f;

        if (Input.GetButtonDown("Pause"))
        {
            m_Paused = !m_Paused;
            m_PauseMenu.SetActive(m_Paused);
        }

    }

    public void NextLevel()
    {
        ++m_currentLevel;
        //m_togglableUI.GetComponentInChildren<WTSLockUI>(true).gameObject.SetActive(true);
        //m_togglableUI.GetComponentInChildren<WTSLockUI>(true).UnlockSystem();
        m_InGameUI.UpdateFloorNumber(m_currentLevel);
        //GameObject.FindGameObjectWithTag("LevelUI").GetComponent<LevelUIflash>().NextLevel();
    }

    public void PlaySuccSound()
    {
        soundManager.GetComponent<SoundManager>().PlaySound(gameObject, 5);
    }

    public void Restart()
    {
        if (gameObject != GameObject.FindGameObjectWithTag("GameManager"))
        {
            if (!GameObject.FindGameObjectWithTag("GameManager"))
                return;
            GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>().Restart();
        }
        else
        {
            if (instancePlayer == null)
            {
                instancePlayer = Instantiate(PlayerPrefab, new Vector3(-0.84f, 1f, 0.01f), Quaternion.identity) as GameObject;
                m_playerHealth = instancePlayer.GetComponent<Health>();
                PlayerOriginalStartPos = instancePlayer.transform.position;
            }
            Time.timeScale = 1.0f;
            //m_deathScreen.SetActive(false);
            m_playerHealth.SetHealth(m_playerHealth.m_MaxHealth);
            m_currentLevel = -25;
            m_InGameUI.UpdateFloorNumber(m_currentLevel);
            m_EndGame = false;
            m_Paused = false;
            //m_PauseMenu.SetActive(false);
            instancePlayer.transform.position = PlayerOriginalStartPos;
            instancePlayer.GetComponent<PlayerController>().Randomize();
            m_togglableUI.SetActive(true);
			//m_togglableUI.GetComponentInChildren<WTSLockUI>(true).gameObject.SetActive(true);
			GameObject[] allObj = FindObjectsOfType<GameObject>();
			SceneManager.LoadScene("MainMenu");
			for (int i = 0; i < allObj.Length; i++)
			{
				if (allObj[i] && allObj[i].tag != "MainCamera")
				{
					Destroy(allObj[i]);
				}
			}
			SceneManager.LoadScene("StartLevel");
            //Reloads current level
            //hard coded start pos, this will need to be updated;
        }
    }

    public void LoadStartLevel()
    {
        SceneManager.LoadScene("StartLevel");
        Restart();

    }

    public void LoadMenu()
    {
		GameObject[] allObj = FindObjectsOfType<GameObject>();
        SceneManager.LoadScene("MainMenu");
		for(int i = 0; i < allObj.Length; i++)
		{
			if(allObj[i])
			{
				Destroy(allObj[i]);
			}
		}
    }

    public void Quit()
	{
		Application.Quit();
	}

    public UIStats GetUIStats()
    {
        return m_InGameUI;
    }
}
