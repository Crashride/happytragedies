﻿using UnityEngine;
using System.Collections;
/// <summary>
/// AUTHOR: DOUGLAS LANDERS 
/// DATE: 8/16/16
/// PURPOSE: 
/// The seek class is a simple AI behaviour that sets the destination of the 
/// </summary>
public class SeekBehaviour : AIBehaviour
{

    //===============
    //Private Vars 
    GameObject m_Player;    //A reference to the players gameobject
    NavMeshAgent m_Agent;   //A reference to the AI's navmeshagent
    EnemyAI m_AI;           //A reference to the AI's EnemyAI component
    //===============


    //All we need to do is set the AI's navMeshAgent to the player's position anf return success
    public override eResult Execute()
    { 

        //if we have valid references execute our stuff
        if (m_Player && m_Agent && m_AI)
        {
            m_Agent.SetDestination(m_Player.transform.position);
            m_AI.SetActionState(1);
            m_AI.SetAnimationState(1); //used to set the animation for the AI to walking
            return eResult.Success;
        }
        else
            return eResult.Fail; 
    }

    public override eResult Evaluate()
    {
        if (m_AI == null)
            m_AI = GetComponent<EnemyAI>();
        //first see if we need to get references 
        if (m_Player == null)
            m_Player = GameObject.FindGameObjectWithTag("Player");
        if (m_Agent == null)
            m_Agent = GetComponent<NavMeshAgent>();

        //only return success if the AI caqn see the player 
        if (m_AI.CanSeePlayer())
            return eResult.Success;
        else return eResult.Fail;
    }

}
