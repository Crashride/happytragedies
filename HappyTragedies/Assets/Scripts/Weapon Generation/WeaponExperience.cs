﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class WeaponExperience : MonoBehaviour {

    public GameObject EffectPrefab;
    public int currentExp;
    public int totalExpForLevelBase;
    private int totalExpForLevelCurrent;
    private int maxLevel;
    public float weaponAttackPerLevel;
    bool isMax;
    private Weapon weaponAttached;
    public float ScaleChangePerLevel;

    //UIStats Handle
    private Image m_UIStats;

    // Use this for initialization

    void Start() {
        isMax = false;
        weaponAttached = GetComponent<Weapon>();
        weaponAttached.UpdateUIStats();
        maxLevel = weaponAttached.m_levelCap;
        if (maxLevel == weaponAttached.m_weaponLevel)
            isMax = true;

        totalExpForLevelCurrent = totalExpForLevelBase;
        for (int i = 1; i < weaponAttached.m_weaponLevel; ++i)
        {
            totalExpForLevelCurrent = (int)(totalExpForLevelCurrent * ScaleChangePerLevel);
        }
        //Debug.Log("Start ?UI");
        StartCoroutine(GetUIElement());
        //Debug.Log("WHAT ?UI");
        //m_UIStats = GameObject.FindGameObjectWithTag("XPBar").GetComponent<Image>();
        //UpdateUI();
    }

    void Update()
    {

    }

    public void UpdateUI()
    {
        //Debug.Log(currentExp + " , " + totalExpForLevelCurrent + " , " + isMax);
        m_UIStats.fillAmount = (float)currentExp / (float)totalExpForLevelCurrent;
        if (isMax)
            m_UIStats.fillAmount = 1.0f;
        GameObject.FindGameObjectWithTag("Stats").GetComponentInChildren<UIStats>().UpdateXP(currentExp , totalExpForLevelCurrent, isMax);
    }

    public void GainEXP(int exp)
    {
        Debug.Log("called");
        if (isMax)
        {
            return;
        }
        currentExp += exp;
        //Debug.Log("called Exp");
        //Debug.Log(currentExp);
        //Debug.Log(exp);
        //Debug.Log(totalExpForLevelCurrent);
        if (currentExp > totalExpForLevelCurrent)
        {
            //Debug.Log("called inside");
            LevelUp();
            exp = currentExp - totalExpForLevelCurrent;
            currentExp = 0;
            float temp = (totalExpForLevelCurrent * ScaleChangePerLevel);
            totalExpForLevelCurrent = (int)temp;
            GainEXP(exp);
        }
        UpdateUI();
    }

    void LevelUp()
    {
        if (!isMax)
        {
            weaponAttached.m_weaponLevel++;
            weaponAttached.m_damage += weaponAttackPerLevel;
            weaponAttached.GetComponentInParent<PlayerController>().LevelUpParticlesPlay();
            weaponAttached.UpdateUIStats();
            if(weaponAttached.m_WeaponType == WEAPON_TYPE.WEAPON_SHOTGUN)
                StartCoroutine(GameObject.FindGameObjectWithTag("Stats").GetComponentInChildren<UIStats>().LevelUpUI(weaponAttackPerLevel * 4));
            else
                StartCoroutine(GameObject.FindGameObjectWithTag("Stats").GetComponentInChildren<UIStats>().LevelUpUI(weaponAttackPerLevel));
            //Debug.Log(weaponAttached.m_damage);
            //Debug.Log("LEVEL UP");
            GameObject.FindGameObjectWithTag("SoundManager").GetComponent<SoundManager>().PlaySound(gameObject, 12);
            // GameObject instance = Instantiate(EffectPrefab);
            // instance.transform.SetParent(this.transform);
            // currentExp = 0;
            if (GetComponent<MeleeAttack>())
            {
                GetComponent<MeleeAttack>().UpdateWeapon();
            }
            else if (GetComponent<GunAttack>())
            {
                GetComponent<GunAttack>().UpdateWeapon();
            }

            if (weaponAttached.m_weaponLevel == weaponAttached.m_levelCap)
            {
                //todo: draw max level UI
                isMax = true;
            }
        }
    }
    IEnumerator GetUIElement()
    {
        //Debug.Log("Entered CO");
        while (GameObject.FindGameObjectWithTag("XPBar") == null)
        {
            //Debug.Log("YIELD");
            yield return new WaitForEndOfFrame();
        }
        m_UIStats = GameObject.FindGameObjectWithTag("XPBar").GetComponent<Image>();
        UpdateUI();
        //Debug.Log("RAN");
    }
}
