﻿using UnityEngine;
using System.Collections;

public class HealPlayer : MonoBehaviour {
    bool hasBeenUsed;
    private GameObject guiHolder;
    [Tooltip("Percentage of players max health that the player is healed by (should be 0 - 1)")]
    public float healingAmount;
	// Use this for initialization
	void Start () {
        Canvas temp = GetComponentInChildren<Canvas>();
        guiHolder = temp.gameObject;
        guiHolder.SetActive(false);
        hasBeenUsed = false;
    }

    // Update is called once per frame
    void OnTriggerStay(Collider other)
    {
        if (!hasBeenUsed && other.tag == "Player")
        {
            guiHolder.SetActive(true);
            if (Input.GetButtonDown("Swap"))
            {
                other.GetComponent<Health>().DoHealing(other.GetComponent<Health>().GetMaxHealth() * (healingAmount + other.GetComponent<PlayerStats>().GetRegenBuff()));
                other.GetComponent<PlayerController>().PlayHealthParticles();
                hasBeenUsed = true;
                guiHolder.SetActive(false);
            }
        }
    }
    
    void OnTriggerExit(Collider other)
    {
        if(other.tag == "Player")
        {
            guiHolder.SetActive(false);
        }
    }
}
