﻿using UnityEngine;
using System.Collections;

[RequireComponent (typeof (BoxCollider))]
public class MeleeAttack : MonoBehaviour {

    //======================
    //Public Vars 
    [Tooltip ("The amount of knockback applied to hit targets")]
    public float m_AttackForce;
    [Tooltip("How long the rest between attacks is")]
    public float m_AttackRate;
    [Tooltip("The maximum number of effects that can be placed on this weapon")]
    public int m_MaxWeaponEffects;
    [Tooltip("A list of the weapon effects on this weapon")]
    public WeaponEffect[] m_WeaponEffects;
    [Tooltip("The gameobject tag that this weapon will consider a target")]
    public string m_TargetTag; 
    //======================

    //======================
    //Private Vars 
    //How much time has passed since this weapon last attack. 
    double m_SinceLastAttack;

    [SerializeField]
    private float m_damage;
    private Weapon weapon;
    //[HideInInspector]
    public float m_attackSpeed;
    //A reference to the players animator
    Animator m_playerAnimator;
    PlayerController m_Player; //A reference to the playercontroller
    bool m_isAttacking = false;

    //======================

    // Use this for initialization
    void Start ()
    {
        m_Player = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>(); 
        //Debug.Log(m_damage);
        weapon = GetComponent<Weapon>();
        m_damage = weapon.m_damage;
        m_playerAnimator = GameObject.FindGameObjectWithTag("Player").GetComponent<Animator>();
        m_SinceLastAttack = 10; //Give this a high default so attacks can be done right away.
        //Make sure the box collider is a trigger
        m_attackSpeed = weapon.m_attackRate;
        GetComponent<BoxCollider>().isTrigger = true;
        m_AttackRate = 2 / m_attackSpeed;
	}

    // Update is called once per frame
    void Update()
    {
        //if the player is stunned do not update the baton
        if (m_Player.GetIsStunned())
            return; 
        //update the sinceLastAttack counter
        if (m_isAttacking)
        {
            m_SinceLastAttack += Time.deltaTime;
            if(m_SinceLastAttack >= m_AttackRate)
            {
                m_SinceLastAttack = 0;
                m_isAttacking = false;
            }
        }

        //check to see if the player wants to / can attack 
        if (Input.GetAxis("Fire1") > 0 && m_playerAnimator.GetBool("DoMelee") == false && !m_isAttacking)
        {
            Attack();
        }
        //else if(Input.GetAxisRaw("Fire1") <= 0)
        //m_playerAnimator.speed = 1;
    }

    void Attack()
    {
        if(weapon.m_WeaponType == WEAPON_TYPE.WEAPON_MELEE)
            gameObject.GetComponentInParent<PlayerController>().PlaySound(2);
        else
            gameObject.GetComponentInParent<PlayerController>().PlaySound(14);
        m_isAttacking = true;
        m_SinceLastAttack = 0.0f; //reset the attack cooldown
        m_playerAnimator.SetBool("DoMelee", true); //this will player the attack animation, which will swing the weapon and hit enemies
        //m_playerAnimator.speed = m_attackSpeed;
    }

    void OnTriggerEnter(Collider col)
    {
        //find out if the collision was with our target and then do damage and a knockback (if enemy)
        if (gameObject.GetComponentInParent<PlayerController>())
        {
            if (m_TargetTag == "Player")
            {
                if (col.gameObject.tag == "Player")
                {
                    col.gameObject.GetComponent<Health>().DoDamage(m_damage);
                }
            }
            else if (m_TargetTag == "Enemy")
            {
                if (col.gameObject.tag == "Enemy")
                {

                    EnemyAI ai = col.gameObject.GetComponent<EnemyAI>();
                    if (ai)
                        ai.TakeDamage(m_damage);
                    else
                        col.gameObject.GetComponent<Health>().DoDamage(m_damage);
                    Vector3 dist = gameObject.transform.position - col.gameObject.transform.position;
                    Vector3 forceVector = dist.normalized * m_AttackForce;
                    forceVector.y = 0; 
                    Rigidbody rb = col.gameObject.GetComponent<Rigidbody>();
                    rb.isKinematic = false; 
                    rb.AddForce(-forceVector, ForceMode.Impulse);
                    col.gameObject.GetComponentInChildren<Canvas>(true).enabled = true;
                    if(weapon.m_WeaponType == WEAPON_TYPE.WEAPON_MELEE)
                        gameObject.GetComponentInParent<PlayerController>().PlaySound(3);
                    else
                        gameObject.GetComponentInParent<PlayerController>().PlaySound(13);
                    
                }
                else if (col.gameObject.tag == "Boss")
                {
                    BossAI ai = col.gameObject.GetComponent<BossAI>();
                    if (ai)
                        ai.TakeDamage(m_damage);
                    //col.gameObject.GetComponentInChildren<Canvas>(true).enabled = true; 
                }
            }
            
        }
                
    }

    public void UpdateWeapon()
    {
        m_damage = weapon.m_damage;
    }

    public void UpdateWeaponStats(Weapon a_gen)
    {

    }
}

