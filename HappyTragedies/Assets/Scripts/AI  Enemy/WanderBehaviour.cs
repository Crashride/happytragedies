﻿using UnityEngine;
using System.Collections;

/// <summary>
/// AUTHOR: DOUGLAS LANDERS AND MATT CALLAHAN 
/// DATE: 8/21/16
/// PURPOSE: 
/// The Wander behaviour is an idle AI behaviour to make the AI's wander around the room 
/// in a nartual seeming way, rather than standing entirely still. 
/// </summary>
public class WanderBehaviour : AIBehaviour {

    //===================
    //Private Vars 
    NavMeshAgent m_Agent; 
    //===================
    
    public override eResult Execute()
    {
        if (m_Agent == null)
            m_Agent = GetComponent<NavMeshAgent>(); 

        if (m_Agent)
        {
            if (m_Agent.pathStatus == NavMeshPathStatus.PathComplete)
            {
                //Debug.Log("Setting wander path");
                float WanderAngle = 45.0f - Random.Range(0, 90);
                Quaternion WanderQuat = Quaternion.Euler(0, WanderAngle, 0);
                Vector3 WanderTo = WanderQuat * transform.forward;
                m_Agent.SetDestination(transform.position + new Vector3(WanderTo.x, 0, WanderTo.z));
                GetComponent<EnemyAI>().SetAnimationState(1); //used to set the animation for the AI to walking
                return eResult.Success;
            }
            else if (m_Agent.pathStatus == NavMeshPathStatus.PathInvalid)
            {
                m_Agent.SetDestination(transform.position); //cancel the destination
            }
            else if (m_Agent.pathStatus == NavMeshPathStatus.PathPartial)
                return eResult.Success; 
        }

        return eResult.Fail;
    }

    public override eResult Evaluate()
    {
        return eResult.Success;
    }
}
