﻿using UnityEngine;
using System.Collections;

public class BossQuickAttack : AIBehaviour
{

    //Vars
    BossAI              m_AI;
    NavMeshAgent        m_Agent;
    Rigidbody           m_RigidBody; 
    PlayerController    m_Player;
    eResult             m_CurrentState = eResult.Fail; 

    public override eResult Evaluate()
    {
        //default currentstate to fail and get references 
        m_CurrentState = eResult.Fail;
        if (m_AI == null)
            m_AI = GetComponent<BossAI>();
        if (m_Agent == null)
            m_Agent = GetComponent<NavMeshAgent>();
        if (m_RigidBody == null)
            m_RigidBody = GetComponent<Rigidbody>(); 
        if (m_Player == null)
            m_Player = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>(); 

        //check if player is close and return success or fail
        if (m_AI.IsPlayerClose("Melee"))
            return eResult.Success;
        else return eResult.Fail; 
    }

    public override eResult Execute()
    {
        if (m_CurrentState == eResult.Fail)
        {
            m_Agent.enabled = false; //turn off the agent so it can't move
            int i = Random.Range(0, 2);
            m_RigidBody.constraints = RigidbodyConstraints.FreezeAll; 
            if (i == 0)
                m_AI.SetAnimationState(6); 
            else
                m_AI.SetAnimationState(5);
            m_CurrentState = eResult.InProgress; 
        }
        return m_CurrentState; 
    }

    public void DoDamage()
    {
        if (m_AI.IsPlayerClose("Melee"))
            m_Player.DoDamage(m_AI.GetQuickDamage());
    }

    public void FinishedAttack()
    {
        m_CurrentState = eResult.Success;
        m_Agent.enabled = true;
        m_AI.SetAnimationState(0);
        m_RigidBody.constraints = RigidbodyConstraints.None; 
    }

}
