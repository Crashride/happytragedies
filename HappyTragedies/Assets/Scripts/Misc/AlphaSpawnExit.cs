﻿using UnityEngine;
using System.Collections;

public class AlphaSpawnExit : MonoBehaviour
{

    public GameObject m_ExitPrefab;
    public GameObject m_TeleportPrefab; 

    public void CreateExit()
    {
        Debug.Log("Called exitcreate"); 
        GameObject prefab = GameObject.Instantiate(m_ExitPrefab, transform.parent.position, Quaternion.identity) as GameObject;
        GameObject telefab = GameObject.Instantiate(m_TeleportPrefab, transform.parent.position, Quaternion.identity) as GameObject; 
    }
}
