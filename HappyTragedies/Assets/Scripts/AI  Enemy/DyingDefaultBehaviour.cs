﻿using UnityEngine;
using System.Collections;

public class DyingDefaultBehaviour : AIBehaviour {

    //===================
    //Private Vars
    Health m_Health;
    EnemyAI m_AI;

    float   m_KillReward;
    int     m_ExpReward; 
    //===================

    public override eResult Execute()
    {
        //Start playing our death animation 
        m_AI.SetAnimationBool("Dead", true);
        m_AI.SetActionState(3);
        m_AI.m_deathXplode.Play();
        m_AI.GetComponentInChildren<SkinnedMeshRenderer>().enabled = false;
        if(!Camera.main.GetComponent<FollowPlayer>().Shaking())
            Camera.main.GetComponent<FollowPlayer>().Shake(m_AI.m_XplodeShakeDuration, m_AI.m_XplodeShakeIntensity);
        //De-activate any components that will interfere with the enemy being 'dead' 
        GetComponent<NavMeshAgent>().enabled = false;
        GetComponent<CapsuleCollider>().enabled = false;
        GetComponent<Rigidbody>().freezeRotation = true;
        GetComponent<Rigidbody>().useGravity = false;
        GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeAll;

        //Give the player some exp and money. 
        PlayerController pc = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>();
        pc.GainExp(m_ExpReward);
        pc.AddBank(m_KillReward);

        //Drop a weapon if one hasn't been dropped before 
        if (m_AI.GetRoom() && !m_AI.GetRoom().hasSpawnedWep)
        {
            WeaponGenerator wpGen = FindObjectOfType<WeaponGenerator>(); 
            GameObject instance = wpGen.GenerateWeapon(100);   //Generate the weapon
            instance.GetComponent<WeaponSwap>().enabled = true;
            
            m_AI.GetRoom().hasSpawnedWep = true;
            instance.transform.position = gameObject.transform.position;
            //Instantiate a particle system to indicate a dropped weapon. 
            //GameObject PSInstance = GameObject.Instantiate(wpGen.GetWeaponDropParticles(), instance.transform) as GameObject;
            // PSInstance.transform.localPosition = Vector3.zero; 
            //Play the particles
            //PSInstance.GetComponent<ParticleSystem>().Play();
            if (instance.GetComponent<GunAttack>())
            {
                instance.GetComponent<GunAttack>().enabled = false;
            }
            else
            {
                instance.GetComponent<MeleeAttack>().enabled = false;
            }
        }
        return eResult.Success; 
    }

    public override eResult Evaluate()
    {
        if (m_Health == null)
            m_Health = GetComponent<Health>();
        if (m_AI == null)
            m_AI = GetComponent<EnemyAI>();
        if (m_KillReward <= 0.0f)
            m_KillReward = m_AI.GetKillReward();
        if (m_ExpReward <= 0.0f)
            m_ExpReward = m_AI.GetExpReward(); 


        return eResult.Success; 
    }

    //This function is called by the animator at the end of the death animation
    public void FinishedDying()
    {
        gameObject.SetActive(false);
    }
}
