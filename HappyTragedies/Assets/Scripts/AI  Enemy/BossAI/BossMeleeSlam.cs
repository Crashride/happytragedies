﻿using UnityEngine;
using System.Collections;

public class BossMeleeSlam : AIBehaviour {

    //Vars
    BossAI m_AI;
    NavMeshAgent m_Agent;
    Rigidbody m_RigidBody;
    PlayerController m_Player;
    eResult m_CurrentState;

    //Used to get references and
    public override eResult Evaluate()
    {
        m_CurrentState = eResult.Fail;
        if (m_Agent == null)
            m_Agent = GetComponent<NavMeshAgent>();
        if (m_AI == null)
            m_AI = GetComponent<BossAI>();
        if (m_RigidBody == null)
            m_RigidBody = GetComponent<Rigidbody>();
        if (m_Player == null)
            m_Player = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>();

        if (m_AI.IsPlayerClose("Ranged"))
        {
            return eResult.Success;
        }
        else return eResult.Fail; 
    }

    //called to begin the execution of the behaviour 
    public override eResult Execute()
    {
        if (m_CurrentState == eResult.Fail)
        {
            m_Agent.enabled = false;
            m_AI.SetAnimationState(2); //animation for slaming 
            m_AI.GetWarningCircle().SetActive(true); 
            m_RigidBody.constraints = RigidbodyConstraints.FreezeAll;
            m_CurrentState = eResult.InProgress; 
        }
        return m_CurrentState; 
    }

    public void SlamHit()
    {
        if (!Camera.main.GetComponent<FollowPlayer>().Shaking())
            Camera.main.GetComponent<FollowPlayer>().Shake(m_AI.m_stunShakeDuration, m_AI.m_stunShakeIntensity);
        m_AI.GetShockWave().GetComponent<ParticleSystem>().Play();
        if (m_AI.IsPlayerClose("Ranged")){ m_Player.GetComponent<Health>().DoDamage(m_AI.GetStunDamage()); }
    }

    public void FinishedSlamAttack()
    {
        m_AI.GetWarningCircle().SetActive(false); 
        m_CurrentState = eResult.Success;
        m_Agent.enabled = true;
        m_RigidBody.constraints = RigidbodyConstraints.None;
        m_AI.SetAnimationState(0);
        m_CurrentState = eResult.Success; 
            
    }

}
