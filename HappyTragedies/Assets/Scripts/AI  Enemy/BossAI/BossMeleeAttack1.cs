﻿using UnityEngine;
using System.Collections;

public class BossMeleeAttack1 : AIBehaviour {

    //Private Vars 
    BossAI      m_AI;
    NavMeshAgent m_Agent;
    Rigidbody m_Rigidbody; 
    PlayerController      m_Player; 
    eResult     m_CurrentState; 

    public override eResult Execute()
    {
        if (m_CurrentState == eResult.Fail)
        {
            m_Agent.enabled = false;
            m_AI.SetAnimationState(3); //Animation state for slashing
            m_Rigidbody.constraints = RigidbodyConstraints.FreezeRotation; 
            m_CurrentState = eResult.InProgress;
        }
        return m_CurrentState; 
    }

    public override eResult Evaluate()
    {
        //Get references
        if (m_AI == null)
            m_AI = GetComponent<BossAI>();
        if (m_Agent == null)
            m_Agent = GetComponent<NavMeshAgent>();
        if (m_Rigidbody == null)
            m_Rigidbody = GetComponent<Rigidbody>();
        if (m_Player == null)
            m_Player = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>(); 

        //default our current state 
        m_CurrentState = eResult.Fail;


        if (m_AI.IsPlayerClose("Melee"))
        {
            return eResult.Success; 
        }
            
        else return eResult.Fail; 
    }

    //First deal damage function,  both  of these could be the same but we might have them doing seperate things later
    public void DealDamage1()
    {
        if (m_AI.IsPlayerClose("Melee") && m_Player != null)
        {
            m_Player.DoDamage(m_AI.GetHeavyDamage(1));
        }
    }

    //Second deal damage function
    public void DealDamage2()
    {
        if (m_AI.IsPlayerClose("Melee") && m_Player != null)
        {
            m_Player.DoDamage(m_AI.GetHeavyDamage(2));
        }
    }

    //Called when the attack is finished to end the behaviour
    public void FinishedAttackSlash()
    {
        m_Agent.enabled = true;
        m_AI.SetAnimationState(0); //0 = idle
        m_Rigidbody.constraints = RigidbodyConstraints.None;
        m_CurrentState = eResult.Success; 
    }
}
