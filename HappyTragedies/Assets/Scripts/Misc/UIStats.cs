﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UIStats : MonoBehaviour
{
    [Tooltip(" 0 = FloorNum\n 1 = WeaponLevel\n 2 = WeaponDmg\n 3 = WeaponAtkSpd\n 4 = ReloadTime\n 5 = BaseDmg\n 6 = WeaponLvlUP\n 7 = WeaponLvlUpDMG\n 8 = [Redacted]\n 9 = MaxLevelToggle\n 10 = GunClipFraction")]
    public Text[] m_statTextFields;
    public SpriteRenderer m_blueStars;
    public SpriteRenderer[] m_GoldStars;
    public SpriteRenderer[] m_MaxHPUpgradeBubbles;
    public SpriteRenderer[] m_StaminaUpgradeBubbles;
    public SpriteRenderer[] m_HpRegenUpgradeBubbles;
    public SpriteRenderer[] m_XPBonusUpgradeBubbles;
    // Use this for initialization
    void Awake()
    {
        //m_statTextFields = GameObject.FindGameObjectWithTag("Stats").GetComponentsInChildren<Text>();
                
        //UpdateFloorNumber(GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>().m_currentLevel);

        for(int i = 0; i < 5; ++i)
        {
            m_MaxHPUpgradeBubbles[i].enabled = false;
            m_StaminaUpgradeBubbles[i].enabled = false;
            m_HpRegenUpgradeBubbles[i].enabled = false;
            m_XPBonusUpgradeBubbles[i].enabled = false;
            m_GoldStars[i].enabled = false;
        }
        m_blueStars.enabled = false;

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void UpdateWeaponRating(int a_wepScore)
    {
        //Calculate the star rating
        for (int i = 0; i < a_wepScore; ++i)
        {
            if (i == 5)
            {
                foreach (SpriteRenderer sp in m_GoldStars)
                {
                    sp.enabled = false;
                }
                m_blueStars.enabled = true;
            }
            else
            {
                m_GoldStars[i].enabled = true;
                m_blueStars.enabled = false;
            }
        }
    }

    public void UpdateFloorNumber(int a_floorNum)
    {
        m_statTextFields[0].text = "Floor " + (a_floorNum + 26).ToString();
    }

    public void UpdateWeaponLevel(int a_weaponLvl, int a_maxLevel)
    {
        m_statTextFields[1].text = a_weaponLvl.ToString() + "/" + a_maxLevel.ToString();
    }

    public void UpdateDmg(float a_dmg)
    {
        m_statTextFields[2].text = a_dmg.ToString("F1");
    }

    public void UpdateAtkSpd(float a_atkspd)
    {
        if (a_atkspd == 0)
        {
            m_statTextFields[3].text = "N/A";
        }
        else
            m_statTextFields[3].text = a_atkspd.ToString("F1") + "/s";
    }

    public void UpdateReloadSpeed(double a_reloadTime)
    {
        if (a_reloadTime == 0)
        {
            m_statTextFields[4].text = "N/A";
        }
        else
            m_statTextFields[4].text = a_reloadTime.ToString("F1") + " s";
    }

    public void UpdateBaseDmg(float a_baseDmg)
    {
        m_statTextFields[5].text = a_baseDmg.ToString("F1");
    }

    public void UpdateMaxHealth(int a_maxHealthlvl)
    {
        if (a_maxHealthlvl > 5)
            return;
        for (int i = 0; i < a_maxHealthlvl; ++i)
        {
            m_MaxHPUpgradeBubbles[i].enabled = true;
        }
                
    }

    public void UpdateHPRegen(int a_hpRegenlvl)
    {
        if (a_hpRegenlvl > 5)
            return;
        for (int i = 0; i < a_hpRegenlvl; ++i)
        {
            m_HpRegenUpgradeBubbles[i].enabled = true;
        }
    }

    public void UpdateXPBonus(int a_xpBonuslvl)
    {
        if (a_xpBonuslvl > 5)
            return;
        for (int i = 0; i < a_xpBonuslvl; ++i)
        {
            m_XPBonusUpgradeBubbles[i].enabled = true;
        }
    }

    public void UpdateMvSpd(int a_mvSpdlvl)
    {
        if (a_mvSpdlvl > 5)
            return;
        for (int i = 0; i < a_mvSpdlvl; ++i)
        {
            m_StaminaUpgradeBubbles[i].enabled = true;
        }
    }

    public void UpdateXP(int a_curr, int a_max, bool a_isMaxLvl)
    {
        if(a_isMaxLvl)
            m_statTextFields[9].text = "MAX";
        else
            m_statTextFields[9].text = a_curr.ToString() + "/" + a_max.ToString();
    }

    public void UpdateClipSize(int a_clipSize, int a_currentClip)
    {
        if (a_clipSize == 0)
        {
            m_statTextFields[10].text = "N/A";
        }
        else if(a_clipSize != 0 && a_currentClip == 0)
        {
            m_statTextFields[10].text = "";
        }
        else
            m_statTextFields[10].text = a_currentClip.ToString() + "/" + a_clipSize.ToString();
    }

    public void UpdatePlayerHealth(float a_currHealth, float a_maxHealth)
    {
        m_statTextFields[11].text = a_currHealth.ToString("F0") + "/" + a_maxHealth.ToString();
    }

    public IEnumerator LevelUpUI(float a_dmgIncrease)
    {
        m_statTextFields[6].enabled = true;
        m_statTextFields[7].enabled = true;
        m_statTextFields[7].text = "\nDMG +" + a_dmgIncrease.ToString("F0");
        yield return new WaitForSecondsRealtime(2.0f);
        m_statTextFields[6].enabled = false;
        m_statTextFields[7].enabled = false;

    }
}
