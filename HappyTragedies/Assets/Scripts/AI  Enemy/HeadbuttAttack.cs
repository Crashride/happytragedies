﻿using UnityEngine;
using System.Collections;

public class HeadbuttAttack : AIBehaviour
{
    //==================
    //Private Vars 
    eResult m_State = eResult.Fail; //The current stage of the behaviour | Fail = not started | In Progress = In Progress | Success = Finished 
    EnemyAI m_AI; 
    //==================
    public override eResult Execute()
    {
        if (m_State == eResult.Fail)
        {
            m_AI.SetAnimationState(2); //Sets the animation state to play the attack animation
            m_AI.ResetAttackCounter(); 
            GetComponent<NavMeshAgent>().enabled = false;
            GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeRotation; 
            m_State = eResult.InProgress;
            return m_State;
        }
        else return m_State; 
    }

    //Checks to see if the conditions are met for this behaviours action
    public override eResult Evaluate()
    {
        if (m_AI == null)
            m_AI = GetComponent<EnemyAI>(); //Get our AI component
        m_State = eResult.Fail;         //Set the State to its default

        if (m_AI.IsPlayerClose())
            return eResult.Success;
        else
            return eResult.Fail; 
    }

    //Called by the animator when it is ready to inflict damage
    public void InflictDamage()
    {
        Debug.Log("doing damage");
        if (m_AI.IsPlayerClose())
            GameObject.FindGameObjectWithTag("Player").GetComponent<Health>().DoDamage(m_AI.m_BaseDamage); 
    }

    //Called by the animator when it finishes the attack animation
    public void FinishHeadbuttAttack()
    {
        m_AI.SetAnimationState(0); //Set the ai back to idling animation.
        m_AI.SetActionState(1);
        GetComponent<NavMeshAgent>().enabled = true; //turn the nav mesh agent back on 
        GetComponent<Rigidbody>().constraints = RigidbodyConstraints.None; 
        m_State = eResult.Success; 
         
    }

}
