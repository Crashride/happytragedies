﻿using UnityEngine;
using System.Collections;

public class BossMeleeTrigger : MonoBehaviour
{
    void OnTriggerEnter(Collider col)
    {
        if (col.tag == "Player")
        {
            BossAI ai = GetComponentInParent<BossAI>();
            ai.SetPlayerClose("Melee", true); 
        }
    }

    void OnTriggerExit(Collider col)
    {
        if (col.tag == "Player")
        {
            BossAI ai = GetComponentInParent<BossAI>();
            ai.SetPlayerClose("Melee", false); 
        }
    }
}
