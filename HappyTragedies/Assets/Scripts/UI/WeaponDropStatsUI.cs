﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class WeaponDropStatsUI : MonoBehaviour
{
    public SpriteRenderer m_blueStars;
    public Text[] m_statFields;
    public SpriteRenderer[] m_GoldStars;
    public GameObject[] m_ArrowsNStuff;
    Weapon playerWep;
    Weapon droppedWep;
    Color baseColor;


    // Use this for initialization
    void Start()
    {
        SetStats();
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void SetStats()
    {
        playerWep = GameObject.FindGameObjectWithTag("Player").GetComponentInChildren<Weapon>();
        droppedWep = gameObject.GetComponentInParent<Weapon>();

        //set text fields to corresponding stat numbers
        if (droppedWep.m_WeaponType == WEAPON_TYPE.WEAPON_SHOTGUN)
            m_statFields[0].text = (droppedWep.m_lvlOneDmg * 4).ToString("F1");
        else
            m_statFields[0].text = droppedWep.m_lvlOneDmg.ToString("F1");
        m_statFields[1].text = droppedWep.m_levelCap.ToString();
        m_statFields[2].text = droppedWep.m_WeaponType == WEAPON_TYPE.WEAPON_PISTOL ? "N/A" : droppedWep.m_attackRate.ToString("F1");
        m_statFields[3].text = (droppedWep.m_WeaponType == WEAPON_TYPE.WEAPON_MELEE || droppedWep.m_WeaponType == WEAPON_TYPE.WEAPON_SWORD) ? "N/A" : droppedWep.m_reloadTime.ToString("F1");
        m_statFields[4].text = (droppedWep.m_WeaponType == WEAPON_TYPE.WEAPON_MELEE || droppedWep.m_WeaponType == WEAPON_TYPE.WEAPON_SWORD) ? "N/A" : droppedWep.m_clipSize.ToString("F1");


        baseColor = new Color32(255, 175, 22, 255);

        //Calculate the star rating
        for (int i = 0; i < droppedWep.m_score; ++i)
        {
            if (i == 5)
            {
                foreach (SpriteRenderer sp in m_GoldStars)
                {
                    sp.enabled = false;
                }
                m_blueStars.enabled = true;
            }
            else
            {
                m_GoldStars[i].enabled = true;
                m_blueStars.enabled = false;
            }
        }
        m_statFields[0].color = droppedWep.m_lvlOneDmg > playerWep.m_lvlOneDmg ? Color.green : droppedWep.m_lvlOneDmg == playerWep.m_lvlOneDmg ? baseColor : Color.red;
        m_statFields[1].color = droppedWep.m_levelCap > playerWep.m_levelCap ? Color.green : droppedWep.m_levelCap == playerWep.m_levelCap ? baseColor : Color.red;

        if (playerWep.m_WeaponType == WEAPON_TYPE.WEAPON_PISTOL || droppedWep.m_WeaponType == WEAPON_TYPE.WEAPON_PISTOL)
            m_statFields[2].color = baseColor;
        else
            m_statFields[2].color = droppedWep.m_attackRate > playerWep.m_attackRate ? Color.green : droppedWep.m_attackRate == playerWep.m_attackRate ? baseColor : Color.red;

        if (playerWep.m_WeaponType == WEAPON_TYPE.WEAPON_MELEE || playerWep.m_WeaponType == WEAPON_TYPE.WEAPON_SWORD || droppedWep.m_WeaponType == WEAPON_TYPE.WEAPON_MELEE || playerWep.m_WeaponType == WEAPON_TYPE.WEAPON_SWORD)
            m_statFields[3].color = baseColor;
        else
            m_statFields[3].color = droppedWep.m_reloadTime < playerWep.m_reloadTime ? Color.green : droppedWep.m_reloadTime == playerWep.m_reloadTime ? baseColor : Color.red;

        if (playerWep.m_WeaponType == WEAPON_TYPE.WEAPON_MELEE || playerWep.m_WeaponType == WEAPON_TYPE.WEAPON_SWORD || droppedWep.m_WeaponType == WEAPON_TYPE.WEAPON_MELEE || playerWep.m_WeaponType == WEAPON_TYPE.WEAPON_SWORD)
            m_statFields[4].color = baseColor;
        else
            m_statFields[4].color = droppedWep.m_clipSize > playerWep.m_clipSize ? Color.green : droppedWep.m_clipSize == playerWep.m_clipSize ? baseColor : Color.red;

        for(int i = 0; i < 5; ++i)
        {
            if(m_statFields[i].color == baseColor)
            {
                foreach(SpriteRenderer sp in m_ArrowsNStuff[i].GetComponentsInChildren<SpriteRenderer>())
                {
                    if (sp.sortingOrder == 1)
                        sp.enabled = true;
                }
            }
            else if (m_statFields[i].color == Color.green)
            {
                foreach (SpriteRenderer sp in m_ArrowsNStuff[i].GetComponentsInChildren<SpriteRenderer>())
                {
                    if (sp.sortingOrder == 2)
                        sp.enabled = true;
                }
            }
            else if (m_statFields[i].color == Color.red)
            {
                foreach (SpriteRenderer sp in m_ArrowsNStuff[i].GetComponentsInChildren<SpriteRenderer>())
                {
                    if (sp.sortingOrder == 0)
                        sp.enabled = true;
                }
            }
        }
    }
}
