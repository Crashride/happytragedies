﻿using UnityEngine;
using System.Collections;
/// <summary>
/// AUTHOR: DOUGLAS LANDERS 
/// DATE: 19th July 2016 (created & last editted)
/// PURPOSE: 
/// The Spawner manager is responsible for placing an enemySpawner in each room of the dungeon that needs to spawn enemies.
/// It is also responsible for deciding what enemies each spawner is allowed to spawn and how many it should spawn. 
/// </summary>

public class SpawnerManager : MonoBehaviour
{


    //=====================
    //Public Vars 
    [Tooltip("A prefab for an enemy spawner, do not give an enemySpawner script")]
    public GameObject m_EmptyPrefab;
    [Tooltip("Range Within enemies can spawn (0,0 is centre of room")]
    public float RangeOfSpawn = 4;
    //=====================

    //=====================
    //Private Vars 
    //A list of the spawners this object has created.
    EnemySpawner[] m_Spawners;
    //A list of all of the sections of the dungeon.
    DungeonSection[] m_DungeonSections;
    //A reference to the section of the dungeon we are currently in 
    DungeonSection m_CurrentSection; 
    //=======================

    void Awake()
    {
        //Use this initialisation function to get our list of dungeon sections 
        m_DungeonSections = GetComponents<DungeonSection>();
        m_CurrentSection = null; 
    }

    //This function will place a spawner
    public void CreateSpawners(int a_level, GameObject[] a_rooms)
    {
        int roomCount = a_rooms.Length;
        for (int i = 0; i < roomCount; i++)
        {
            if (a_rooms[i] == null)
            {
                roomCount = i;
            }
        }
        m_Spawners = new EnemySpawner[roomCount];

        //First we need to find out what dungeon section we are working from. 
        for (int i = 0; i < m_DungeonSections.Length; i++)
        {
            if (m_DungeonSections[i].StartLevel <= (a_level) && m_DungeonSections[i].EndLevel >= (a_level))
            {
                //we are working in this section
                m_CurrentSection = m_DungeonSections[i];
                break;
            }
        }


        if (m_CurrentSection != null)
        {
            for (int i = 0; i < roomCount; i++)
            {
                GameObject emptyGO = Instantiate(m_EmptyPrefab, a_rooms[i].transform.position, Quaternion.identity) as GameObject;
                emptyGO.AddComponent<EnemySpawner>();
                EnemySpawner spawner = emptyGO.GetComponent<EnemySpawner>();
                //Determine how many enemies we want this spawner to have 

                float enemyCount = m_CurrentSection.EnemiesPerRoom; //set enemy count to our default section spawn count 
                enemyCount *= (a_level - m_CurrentSection.StartLevel) * m_CurrentSection.EnemySpawnIncrease;

                //At this point we should double check to make sure enemy count wasn't set to 0 accidentally, if it is just reset it to default EnemiesPerRoom
                if (enemyCount == 0)
                    enemyCount = m_CurrentSection.EnemiesPerRoom;

                float enemyVariance = enemyCount * (m_CurrentSection.EnemyCountVariance / 2 / 100); //find the variance in enemies for this section. 
                enemyCount = Random.Range(enemyCount - enemyVariance, enemyCount + enemyVariance);  //Find a random number inside the variance range. 
                spawner.SetMaxEnemies((int)enemyCount);

                spawner.SetEnemyTypes(m_CurrentSection.EnemyTypes);
                spawner.SetSpawnChance(m_CurrentSection.SpawnChances);
                spawner.SetRoom(a_rooms[i].GetComponent<Room>());
                spawner.SetDamageFactor(m_CurrentSection.EnemyDamageFactor);
                spawner.SetHealthFactor(m_CurrentSection.EnemyHealthFactor);
                spawner.SetRewardFactor(m_CurrentSection.EnemyRewardFactor); 

                a_rooms[i].GetComponent<Room>().SetSpawner(spawner);
                m_Spawners[i] = spawner;
				emptyGO.transform.SetParent(a_rooms[i].gameObject.transform);
            }
        }

        SpawnEnemies(); //until we have a game manager we have to spawn enemies here
    }

    //Use this function to delete all the spawners. 
    public void PurgeSpawners()
    {
        //     foreach (EnemySpawner spawner in m_Spawners)
        //     {
        //         Destroy(spawner.gameObject); 
        //     }
    }

    //This function will tell all spawners to spawn their enemies and deactivate them
    public void SpawnEnemies()
    {
        foreach (EnemySpawner spawner in m_Spawners)
        {
            spawner.SpawnEnemies();
        }
    }

}
