﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class IntroManager : MonoBehaviour {

	public float introRunTime;
	private float introStartTime;
	public float dragTime;

	public GameObject harkinson;

	public GameObject escapeDeckCamera;
	public GameObject creditsCamera;

	public float[] anaDialogueStarts;
	private int anaStartCounter;
	public float[] anaDialogueEnds;
	private int anaEndCounter;

	public float[] akiraDialogueStarts;
	private int akiraStartCounter;
	public float[] akiraDialogueEnds;
	private int akiraEndCounter;

	public float[] escapeDeckToCredits;
	private int escToCredCounter;
	public float[] creditsToEscapeDeck;
	private int credToEscCounter;

	public float[] switchBG;
	public int[] whichBG;
	private int BGCounter;

	public float[] moveBG;
	public int[] whereAndWhom;
	private int whomCounter;

	public GameObject anaTextImport;
	public GameObject akiraTextImport;

	public Text anaTextBox;
	public Text akiraTextBox;

	private string[] anaLines;
	private string[] akiraLines;

	public AudioSource dialogueAudio;

	public GameObject normalBG;
	public GameObject bWBG;
	public GameObject negBG;
	public GameObject orangeBG;
	public GameObject purpleBG;
	public GameObject yellowBG;

	public GameObject bGGroup;
	public GameObject transformDesigner;
	public GameObject transformProgrammer;
	public GameObject transformArtist;
	public GameObject transformFinal;
	public GameObject designerGroup;
	public GameObject programmerGroup;
	public GameObject artistGroup;
	public GameObject stencil;

	public float timeToDisableCameraAnimation;
	public float timeToStartFinalDrag;

	// Use this for initialization
	void Start () 
	{
		introStartTime = Time.time;
		anaStartCounter = 0;
		anaEndCounter = 0;
		akiraStartCounter = 0;
		akiraEndCounter = 0;
		escToCredCounter = 0;
		credToEscCounter = 0;
		BGCounter = 0;
		whomCounter = 0;

		//Start with escape deck scene
		escapeDeckCamera.SetActive (true);
		creditsCamera.SetActive (false);

		//get imported text of dialogue
		anaLines = anaTextImport.GetComponent<ImportText>().textLines;
		akiraLines = akiraTextImport.GetComponent<ImportText>().textLines;

		//make sure dialogue starts off empty
		anaTextBox.text = "";
		akiraTextBox.text = "";

		if (dialogueAudio != null) {
			dialogueAudio.Play ();
		}
		//Start action
	}
	
	// Update is called once per frame
	void Update () 
	{
		//Find out how long intro has been running.
		introRunTime = Time.time - introStartTime - dragTime;
		CheckIfStartingAna ();
		CheckIfEndingAna ();
		CheckIfStartingAkira ();
		CheckIfEndingAkira ();
		CheckIfSwitchEndToCred ();
		CheckIfSwitchCredToEnd ();
		CheckIfCameraStop ();
		CheckIfFinalDragStart ();
		if (BGCounter <= whichBG.Length - 1 && BGCounter <= switchBG.Length - 1)
		{
			CheckIfChangeBG (whichBG [BGCounter]);
		}
		if (whomCounter <= whereAndWhom.Length - 1 && whomCounter <= moveBG.Length - 1)
		{
			CheckIfMoveBG (whereAndWhom [whomCounter]);
		}
	}

	public void CheckIfStartingAna()
	{
		if (anaStartCounter <= anaDialogueStarts.Length - 1)
		{
			if (introRunTime >= anaDialogueStarts [anaStartCounter]) 
			{
				//StartDialogueScroll
				anaTextBox.text = anaLines[anaStartCounter];
				anaStartCounter += 1;
			}
		}
	}

	public void CheckIfEndingAna()
	{
		if (anaEndCounter <= anaDialogueEnds.Length - 1)
		{
			if (introRunTime >= anaDialogueEnds [anaEndCounter]) 
			{
				//RemoveDialogue
				anaTextBox.text = "";
				anaEndCounter += 1;
			}
		}
	}

	public void CheckIfStartingAkira()
	{
		if (akiraStartCounter <= akiraDialogueStarts.Length - 1) 
		{
			if (introRunTime >= akiraDialogueStarts [akiraStartCounter]) 
			{
				//StartDialogueScroll
				akiraTextBox.text = akiraLines[akiraStartCounter];
				akiraStartCounter += 1;
			}
		}
	}

	public void CheckIfEndingAkira()
	{
		if (akiraEndCounter <= akiraDialogueEnds.Length - 1) 
		{
			if (introRunTime >= akiraDialogueEnds [akiraEndCounter]) 
			{
				//RemoveDialogue
				akiraTextBox.text = "";
				akiraEndCounter += 1;
			}
		}
	}

	public void CheckIfSwitchEndToCred()
	{
		if (escToCredCounter <= escapeDeckToCredits.Length - 1) 
		{
			if (introRunTime >= escapeDeckToCredits [escToCredCounter]) 
			{
				creditsCamera.SetActive (true);
				escapeDeckCamera.SetActive (false);
				escToCredCounter += 1;
			}
		}
	}

	public void CheckIfSwitchCredToEnd()
	{
		if (credToEscCounter <= creditsToEscapeDeck.Length - 1) 
		{
			if (introRunTime >= creditsToEscapeDeck [credToEscCounter]) 
			{
				escapeDeckCamera.SetActive (true);
				creditsCamera.SetActive (false);
				credToEscCounter += 1;
			}
		}
	}

	public void CheckIfCameraStop()
	{
		if (introRunTime >= timeToDisableCameraAnimation) 
		{
			escapeDeckCamera.GetComponent<Animator>().enabled = false;
		}
	}

	public void CheckIfFinalDragStart()
	{
		if (introRunTime >= timeToStartFinalDrag) 
		{
			harkinson.GetComponent<Animator> ().SetInteger ("DragState", 1);
		}
	}

	public void CheckIfChangeBG(int xxx)
	{
		if (introRunTime >= switchBG [BGCounter]) 
		{
			//none = 0
			//normal = 1
			//bw = 2
			//neg = 3
			//orange = 4
			//purple = 5
			//yellow = 6

			normalBG.GetComponent<Image> ().enabled = false;
			bWBG.GetComponent<Image> ().enabled = false;
			negBG.GetComponent<Image> ().enabled = false;
			orangeBG.GetComponent<Image> ().enabled = false;
			purpleBG.GetComponent<Image> ().enabled = false;
			yellowBG.GetComponent<Image> ().enabled = false;

			if (xxx == 0) {
				//none
			} else if (xxx == 2) {
				bWBG.GetComponent<Image> ().enabled = true;
			} else if (xxx == 3) {
				negBG.GetComponent<Image> ().enabled = true;
			} else if (xxx == 4) {
				orangeBG.GetComponent<Image> ().enabled = true;
			} else if (xxx == 5) {
				purpleBG.GetComponent<Image> ().enabled = true;
			} else if (xxx == 6) {
				yellowBG.GetComponent<Image> ().enabled = true;
			} else {
				normalBG.GetComponent<Image> ().enabled = true;
			}
				
			BGCounter += 1;
		}
	}

	public void CheckIfMoveBG(int yyy)
	{
		if (introRunTime >= moveBG [whomCounter]) 
		{
			//designer = 1
			//programmer = 2
			//artist = 3
			//final = 4


			designerGroup.SetActive (false);
			programmerGroup.SetActive (false);
			artistGroup.SetActive (false);
			stencil.SetActive (false);


			if (yyy == 2) {
				Vector3 pos = transformProgrammer.transform.position;
				Quaternion rot = transformProgrammer.transform.rotation;
				programmerGroup.SetActive (true);
				bGGroup.transform.position = pos;
				bGGroup.transform.rotation = rot;
			} else if (yyy == 3) {
				Vector3 pos = transformArtist.transform.position;
				Quaternion rot = transformArtist.transform.rotation;
				artistGroup.SetActive (true);
				bGGroup.transform.position = pos;
				bGGroup.transform.rotation = rot;
			} else if (yyy == 4) {
				Vector3 pos = transformFinal.transform.position;
				Quaternion rot = transformFinal.transform.rotation;
				stencil.SetActive (true);
				bGGroup.GetComponent<SpinCycle> ().canSpin = true;
				bGGroup.transform.position = pos;
				bGGroup.transform.rotation = rot;
			} else {
				Vector3 pos = transformDesigner.transform.position;
				Quaternion rot = transformDesigner.transform.rotation;
				designerGroup.SetActive (true);
				bGGroup.transform.position = pos;
				bGGroup.transform.rotation = rot;
			}
				
			whomCounter += 1;
		}
	}
}
