﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(AudioSource))]
public class Door : MonoBehaviour
{

    public Material openMat;
    public Material lockedMat;
    private Material objectsMat;
    public bool m_roomIsCleared;
    Animator m_doorAnim;

    BoxCollider[] m_doorColliders;
    bool m_locked = false;

    // Use this for initialization
    void Awake()
    {

        m_doorAnim = GetComponentInChildren<Animator>();
        if (m_doorAnim)
        {
        }
        BoxCollider[] temp = GetComponentsInChildren<BoxCollider>();
        m_doorColliders = new BoxCollider[1];
        int i = 0;
        foreach (BoxCollider col in temp)
        {
            if (!col.isTrigger)
            {
                m_doorColliders[i] = col;
                ++i;
            }
        }
        m_roomIsCleared = false;
        objectsMat = gameObject.GetComponentInChildren<Renderer>().material;
    }

    void Start()
    {
        if(gameObject.tag == "Sealed")
        {
            if (objectsMat)
            {
                GetComponentInChildren<Renderer>().material = lockedMat;
            }
        }
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void Unlock()
    {
        if(tag == "Sealed")
        {
            return;
        }
        m_locked = false;

        m_roomIsCleared = true;
        tag = ("Door");
        if (objectsMat)
        {
            GetComponentInChildren<Renderer>().material = openMat;
        }
        foreach (BoxCollider col in m_doorColliders)
        {
            if(col)
                col.enabled = false;
        }
    }

    public void Lock()
    {
        m_roomIsCleared = false;
        m_locked = true;

        foreach (BoxCollider col in m_doorColliders)
        {
            if (col)
                col.enabled = true;
            if(col.gameObject.tag == "Sealed")
            {
                break;
            }
            m_doorAnim = GetComponentInChildren<Animator>();
            if (objectsMat)
            {
                GetComponentInChildren<Renderer>().material = lockedMat;
            }
            m_doorAnim.SetBool("openDoor", false);
            tag = ("Locked");
        }


    }

    void OnTriggerEnter(Collider other)
    {
        if (tag != "Sealed" && tag != "Locked")
        {
            if (other.tag == "Player")
            {
                m_doorAnim = GetComponentInChildren<Animator>();
                m_doorAnim.SetBool("openDoor", true);
                if(!m_locked) PlaySound(1);
                foreach (BoxCollider col in m_doorColliders)
                {
                    if(col)
                        col.enabled = false;
                }
            }
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (tag!="Locked" && tag!= "Sealed")
        {
            if (other.tag == "Player")
            {
                m_doorAnim = GetComponentInChildren<Animator>();
                if (!m_locked) PlaySound(2);
                m_doorAnim.SetBool("openDoor", false);
            }
            foreach (BoxCollider col in m_doorColliders)
            {
                if (col)
                    col.enabled = true;
            }
        }
    }
    public void OpenDoor()
    {
        m_locked = false;
        m_doorAnim = GetComponentInChildren<Animator>();
        m_doorAnim.SetBool("openDoor", true);
        tag = ("Door");
        if (objectsMat)
        {
            GetComponentInChildren<Renderer>().material = openMat;
        }
        BoxCollider[] temp = GetComponents<BoxCollider>();
        for(int i = 0; i < temp.Length; i++)
        {
            if(!temp[i].isTrigger)
            {
                temp[i].enabled = false;
            }
        }

    }

    public void PlaySound(int a_index)
    {
        SoundManager sm = GameObject.FindGameObjectWithTag("SoundManager").GetComponent<SoundManager>();
        sm.PlaySound(gameObject, a_index);
    }
    public void SealDoor()
    {
        tag = "Sealed";
    }
}
