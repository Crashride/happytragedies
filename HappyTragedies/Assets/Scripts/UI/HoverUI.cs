﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;



public class HoverUI: MonoBehaviour {
    Vector3 SelectorPos;
	public int MoveAmount;
    public GameObject selector;
    // Use this for initialization
    void Start () {
        SelectorPos = GetComponent<RectTransform>().localPosition;
		SelectorPos.y -= MoveAmount;
    }
	
	// Update is called once per frame
    public void Hover()
    {
        selector.GetComponent<RectTransform>().localPosition = SelectorPos;
    }
}
